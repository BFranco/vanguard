module.exports = function(grunt) {

    // Project configuration.
    grunt.initConfig({
        watch: {
            front: {
                files: 'application/assets/default/sass/**/*.scss',
                tasks: ['compass:front']
            }
        },
        compass: {
            front: {
                options: {
                    specify: [
                        'application/assets/default/sass/main.scss',
                        'application/assets/default/sass/lte-IE8.scss'
                    ],

                    basePath: 'application/assets/default/',
                    httpPath: '/assets/default/',

                    sassDir: 'sass',
                    cssDir: 'styles',
                    imagesDir: 'images',
                    javascriptsDir: 'scripts',
                    fontsDir: 'fonts',

                    require: ['susy']
                }
            }
        },
        favicons: {
            options: {
                appleTouchBackgroundColor: "#ffffff",
                tileColor: "auto"
            },
            icons: {
                src: 'application/assets/default/images/icons/source.152x152.png',
                dest: 'application/assets/default/images/icons'
            }
        },
        svgmin: {
            options: {
                plugins: [{
                    removeViewBox: true,
                    removeUselessStrokeAndFill: true,
                    removeEmptyAttrs: true
                }]
            },
            dist: {
                files: [{
                    expand: true,
                    cwd: 'application/assets/default/images/src',
                    src: ['**/*.svg'],
                    dest: 'application/assets/default/images/',
                    ext: '.svg'
                }]
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-contrib-compass');
    grunt.loadNpmTasks('grunt-favicons');

    // Default task(s).
    grunt.registerTask('default', ['watch:webadmin']);
    grunt.registerTask('webadmin', ['watch:webadmin']);
    grunt.registerTask('front', ['watch:front']);

    grunt.registerTask('build', ['compass:webadmin'])

};