<?php

/* components/form/text.twig */
class __TwigTemplate_7ac038a1011c0a066ecf74dd998d5b753eccae0a3cf7f3f71047a3c3b69af960 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"form-group";
        if ((isset($context["invalid"]) ? $context["invalid"] : null)) {
            echo " has-error";
        }
        echo "\">
    <label class=\"control-label\">";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["label"]) ? $context["label"] : null), "html", null, true);
        echo "</label>
    <input type=\"text\" class=\"form-control\" name=\"";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : null), "html", null, true);
        echo "\" value=\"";
        echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : null), "html", null, true);
        echo "\">
    ";
        // line 4
        if ((isset($context["help"]) ? $context["help"] : null)) {
            echo " }}<p class=\"help-block\">Example block-level help text here.</p>";
        }
        // line 5
        echo "    ";
        if ((isset($context["errors"]) ? $context["errors"] : null)) {
            // line 6
            echo "        ";
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["errors"]) ? $context["errors"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["error"]) {
                // line 7
                echo "            <p class=\"text-danger\">";
                echo twig_escape_filter($this->env, (isset($context["error"]) ? $context["error"] : null), "html", null, true);
                echo "</p>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['error'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 9
            echo "    ";
        }
        // line 10
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "components/form/text.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 10,  57 => 9,  48 => 7,  43 => 6,  40 => 5,  36 => 4,  30 => 3,  26 => 2,  19 => 1,);
    }
}
