<?php

/* front.tpl */
class __TwigTemplate_072b188e7c36cb49242fbc785c307063d4c7877eaabf64773b94007022c4a3f4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>

<!--[if IE 7 ]>     <html lang=\"\" class=\"ie7\"> <![endif]-->
<!--[if lte IE 8 ]> <html lang=\"\" class=\"lte-ie8\"> <![endif]-->
<!--[if (gt IE 7)|!(IE)]><!--> <html lang=\"en\"> <!--<![endif]-->

<head prefix=\"og: http://ogp.me/ns# fb: http://ogp.me/ns#\">
    <title>";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
        echo "</title>
    <meta charset=\"utf-8\">

    <meta name=\"description\" content=\"\" />
    <meta name=\"keywords\"    content=\"\" />

    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">

    <!-- Use png favicon -->
    <link rel=\"icon\" href=\"/favicon.png\">
    <!--[if IE]><link rel=\"shortcut icon\" href=\"/favicon.ico\"><![endif]-->

    <!-- Icon for (older) Android devices -->
    <link rel=\"apple-touch-icon-precomposed\" href=\"/assets/default/images/icons/apple-touch-icon-precomposed.png\" />

    <!-- Windows tile settings -->
    <meta name=\"application-name\" content=\"";
        // line 24
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["config"]) ? $context["config"] : null), "siteName"));
        echo "\"/>
    <meta name=\"msapplication-TileColor\" content=\"#ffffff\"/>
    <meta name=\"msapplication-TileImage\" content=\"/assets/default/images/icons/windows-tile-144x144.png\"/>

    <!-- CSS Styles -->
    <!--[if (gte IE 6)&(lte IE 8)]>
        <link rel=\"stylesheet\" type=\"text/css\" media=\"all\" href=\"/assets/default/styles/lte-IE8.css?v=0\" />
    <![endif]-->
    <!--[if (gt IE 8)|!(IE)]><!-->
        <link rel=\"stylesheet\" type=\"text/css\" media=\"all\" href=\"/assets/default/styles/main.css?v=0\" />
    <!--<![endif]-->


    <!-- Modernizr -->
    <script src=\"/assets/default/scripts/components/modernizr-custom.js\"></script>

    <!-- jQuery + Selectivizr + Respond -->
    <!--[if (gte IE 6)&(lte IE 8)]>
    <script src=\"//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js\"></script>
    <script>window.jQuery || document.write('<script src=\"/assets/lib/vendor/jquery/jquery.js\">\\x3C/script>')</script>
    <script type=\"text/javascript\" src=\"/assets/lib/vendor/selectivizr/selectivizr.js\"></script>
    <script src=\"/assets/lib/vendor/respond/respond.min.js\"></script>
    <![endif]-->

</head>
<body>
    <header>

    </header>
    ";
        // line 53
        $this->displayBlock('content', $context, $blocks);
        // line 54
        echo "
    <!-- jQuery -->
    <!--[if (gt IE 8)|!(IE)]><!-->
    <script src=\"/assets/lib/vendor/jquery/jquery.js\"></script>
    <!--<![endif]-->

    <!-- Our own Scripts -->


</body>
</html>";
    }

    // line 53
    public function block_content($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "front.tpl";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  96 => 53,  82 => 54,  80 => 53,  48 => 24,  29 => 8,  20 => 1,);
    }
}
