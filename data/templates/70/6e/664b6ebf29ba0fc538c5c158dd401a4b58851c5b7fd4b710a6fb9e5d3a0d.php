<?php

/* test.twig */
class __TwigTemplate_706e664b6ebf29ba0fc538c5c158dd401a4b58851c5b7fd4b710a6fb9e5d3a0d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("front.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "front.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "    <div id=\"resume\">
        <div class=\"container\">
            <h1><span class=\"itemprop=\"name\">Brecht Franco</span><br/><span class=\"black\">Webdeveloper</span></h1>
            <hr/>

            <div class=\"row\">
                <h2>Ervaring</h2>
                <p>Ik heb in mijn carriëre van de volgende interessante <br/>ervaringen mogen genieten:</p>
                <div class=\"attribute\">
                    <div class=\"name\">6 mei 2013 - 1 december 2013</div>
                    <div class=\"value\">
                        <p>Inventis</p>
                        <p>Webdeveloper</p>
                        <p>Front-end integratie + ontwikkelen modules (front- en back-end) voor custom framework</p>
                    </div>
                </div>
                <div class=\"attribute\">
                    <div class=\"name\">1 februari 2013 - 22 maart 2013</div>
                    <div class=\"value\">
                        <p>anaXis - Stage</p>
                        <p>Drupal Developer</p>
                        <p>Realiseren van websites met Drupal. Back- en front-end: module development, theming, etc.</p>
                    </div>
                </div>
                <h2>Vaardigheden</h2>
                <p>Door mijn vorige tewerkstelling, stage en vrijetijdsbesteding<br/> ben ik met de volgende zaken in aanraking gekomen:</p>
                <div class=\"attribute\">
                    <div class=\"name\">Software</div>
                    <div class=\"value\">
                        <ul>
                            <li>PhpStorm</li>
                            <li>Netbeans</li>
                            <li>Eclipse</li>
                            <li>Aptana</li>
                            <li>Photoshop</li>
                            <li>CSS preprocessors: SASS (Compass, Susy, ...), LESS</li>
                            <li>Besturingssystemen: Windows, Mac(OSX), Linux(Ubuntu)</li>
                        </ul>
                    </div>
                </div>
                <div class=\"attribute\">
                    <div class=\"name\">Technologieën</div>
                    <div class=\"value\">
                        <ul>
                            <li>PHP (OOP)</li>
                            <li>CSS3</li>
                            <li>XHTML</li>
                            <li>Javascript</li>
                            <li>jQuery</li>
                            <li>Ext JS</li>
                            <li>Template engines: Smarty en Twig</li>
                            <li>Gebruik van Composer en Bower</li>
                            <li>Git</li>
                            <li>Adaptive en responsive design</li>
                        </ul>
                    </div>
                </div>
                <div class=\"attribute\">
                    <div class=\"name\">Frameworks/CMS</div>
                    <div class=\"value\">
                        <p>Drupal 7: Theming en module development</p>
                        <p>Custom frameworks met Zend/Doctrine/Symfony components</p>
                    </div>
                </div>
                <div class=\"attribute\">
                    <div class=\"name\">Behaalde certificaten</div>
                    <div class=\"value\">
                        <ul>
                            <li>(X)HTML</li>
                            <li>PHP + Databases</li>
                            <li>Drupal</li>
                            <li>Interfacetechnieken (CSS)</li>
                            <li>Webanimatie (CSS3, jQuery + AJAX)</li>
                            <li>Photoshop</li>
                        </ul>
                    </div>
                </div>
            </div>
            </div>
    </div>
    <div id=\"contact\">
        <div class=\"divider\"></div>

        <div class=\"container\">
            <div class=\"row\">
                <h2>Contacteer me</h2>

                <div class=\"col-4 data\">
                    <h3>Mijn gegevens</h3>
                    <div class=\"dark-box\">
                        <h4>E-mail</h4>
                        <a class=\"info\" href=\"&#109;&#97;&#105;&#108;&#116;&#111;&#58;&#102;&#114;&#97;&#110;&#99;&#111;&#46;&#98;&#114;&#101;&#99;&#104;&#116;&#64;&#103;&#109;&#97;&#105;&#108;&#46;&#99;&#111;&#109;\"><meta itemprop=\"email\" content=\"franco.brecht@gmail.com\">&#102;&#114;&#97;&#110;&#99;&#111;&#46;&#98;&#114;&#101;&#99;&#104;&#116;&#64;&#103;&#109;&#97;&#105;&#108;&#46;&#99;&#111;&#109;</a>
                        <h4>Gsm</h4>
                        <a href='tel:0494684694' class=\"info phone\" itemprop=\"telephone\">0494/68 46 94</a>
                        <h4>Adres</h4>
                        <span itemprop=\"address\" itemscope itemtype=\"http://schema.org/PostalAddress\">
                            <span class=\"info no-margin\" itemprop=\"streetAddress\">
                                Boomgaardstraat 10<br/>
                                3550 Zolder
                            </span>
                        </span>
                    </div>
                </div>
                <div id=\"contact_form\" class=\"col-5 form align-left\">
                    <h3>Of via dit formulier</h3>
                    ";
        // line 108
        if ((isset($context["success"]) ? $context["success"] : null)) {
            // line 109
            echo "                        <div class=\"success\">Bedankt voor je berichtje! <br/>Ik neem zo snel mogelijk contact op!</div>
                    ";
        }
        // line 111
        echo "                    <form action=\"#contact_form\" method=\"post\">
                        <div class=\"entry";
        // line 112
        if ($this->getAttribute((isset($context["errors"]) ? $context["errors"] : null), "name")) {
            echo " error ";
        }
        echo "\">
                            <div class=\"input-wrapper\">
                                <label for=\"name\"><span class=\"name-icon\"></span></label><input type=\"text\" name=\"name\" id=\"name\" placeholder=\"Naam\" value=\"";
        // line 114
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["twig"]) ? $context["twig"] : null), "post"), "name"));
        echo "\"/>
                            </div>
                            ";
        // line 116
        if ($this->getAttribute((isset($context["errors"]) ? $context["errors"] : null), "name")) {
            echo "<span class=\"error\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["errors"]) ? $context["errors"] : null), "name"));
            echo "</span>";
        }
        // line 117
        echo "                        </div>
                        <div class=\"entry";
        // line 118
        if ($this->getAttribute((isset($context["errors"]) ? $context["errors"] : null), "email")) {
            echo " error ";
        }
        echo "\">
                            <div class=\"input-wrapper\">
                                <label for=\"email\"><span class=\"email-icon\"></span></label><input type=\"text\" name=\"email\" id=\"email\" placeholder=\"E-mail adres\" value=\"";
        // line 120
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["twig"]) ? $context["twig"] : null), "post"), "email"));
        echo "\"/>
                            </div>
                            ";
        // line 122
        if ($this->getAttribute((isset($context["errors"]) ? $context["errors"] : null), "email")) {
            echo "<span class=\"error\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["errors"]) ? $context["errors"] : null), "email"));
            echo "</span>";
        }
        // line 123
        echo "                        </div>
                        <div class=\"entry";
        // line 124
        if ($this->getAttribute((isset($context["errors"]) ? $context["errors"] : null), "message")) {
            echo " error ";
        }
        echo "\">
                            <div class=\"input-wrapper\">
                                <label for=\"message\"><span class=\"message-icon\"></span></label><textarea name=\"message\" id=\"message\" placeholder=\"Bericht\">";
        // line 126
        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["twig"]) ? $context["twig"] : null), "post"), "message"));
        echo "</textarea>
                            </div>
                            ";
        // line 128
        if ($this->getAttribute((isset($context["errors"]) ? $context["errors"] : null), "message")) {
            echo "<span class=\"error\">";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["errors"]) ? $context["errors"] : null), "message"));
            echo "</span>";
        }
        // line 129
        echo "                        </div>
                        <input type=\"submit\" class=\"button\" value=\"Verzenden\"/>
                    </form>
                </div>
                <div class=\"col-3 social-media omega\">
                    <h3>Sociale media</h3>
                    <div class=\"row\"><a class=\"button red\" href=\"http://www.facebook.com/brecht.franco\" target=\"_blank\">Facebook</a></div>
                    <div class=\"row\"><a class=\"button red\" href=\"http://www.linkedin.com/profile/view?id=177830222\" target=\"_blank\">LinkedIn</a></div>
                </div>
            </div>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "test.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  207 => 129,  201 => 128,  196 => 126,  189 => 124,  186 => 123,  180 => 122,  175 => 120,  168 => 118,  165 => 117,  159 => 116,  154 => 114,  147 => 112,  144 => 111,  140 => 109,  138 => 108,  31 => 3,  28 => 2,);
    }
}
