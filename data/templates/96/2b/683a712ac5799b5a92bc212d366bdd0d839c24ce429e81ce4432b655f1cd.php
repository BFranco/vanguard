<?php

/* admin/detail.twig */
class __TwigTemplate_962b683a712ac5799b5a92bc212d366bdd0d839c24ce429e81ce4432b655f1cd extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("admin.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "admin.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "<div class=\"row\">
    <div class=\"col-lg-12\">
        <div class=\"form-group\"><a class=\"btn btn-default\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, (isset($context["overviewUrl"]) ? $context["overviewUrl"] : null), "html", null, true);
        echo "\"><i class=\"fa fa-arrow-left\"></i> Naar overzicht</a></div>
    </div>
</div>
<div class=\"row\">
    <div class=\"col-lg-12\">";
        // line 9
        echo (isset($context["content"]) ? $context["content"] : null);
        echo "</div>
</div>
";
    }

    public function getTemplateName()
    {
        return "admin/detail.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 9,  35 => 5,  31 => 3,  28 => 2,);
    }
}
