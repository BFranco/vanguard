<?php

/* admin.twig */
class __TwigTemplate_64e646471638f4bda906fdd153ce84d4ae8432486c9d8e6242ca662048d1a626 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>

<!--[if IE 7 ]><html lang=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["config"]) ? $context["config"] : null), "language"), "html", null, true);
        echo "\" class=\"ie7\"> <![endif]-->
<!--[if lte IE 8 ]> <html lang=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["config"]) ? $context["config"] : null), "language"), "html", null, true);
        echo "\" class=\"lte-ie8\"> <![endif]-->
<!--[if (gt IE 7)|!(IE)]><!--> <html lang=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["config"]) ? $context["config"] : null), "language"), "html", null, true);
        echo "\"> <!--<![endif]-->

<head>
    <title>";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
        if ((isset($context["title"]) ? $context["title"] : null)) {
            echo " | ";
        }
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["config"]) ? $context["config"] : null), "siteName"), "html", null, true);
        echo " - Web developer</title>
    <meta charset=\"utf-8\">

    <meta name=\"description\" content=\"";
        // line 11
        echo twig_escape_filter($this->env, (isset($context["description"]) ? $context["description"] : null));
        echo "\" />
    <meta name=\"keywords\"    content=\"Web development, web, webdesign, Brecht, Franco, Brecht Franco, Limburg, Zolder, Hasselt, freelance, vanguard\" />
    <meta name=\"author\"      content=\"Brecht Franco\">
    <meta name=\"location\"    content=\"Heusden-Zolder\">
    <meta name=\"role\"        content=\"Web developer\">

    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">

    <!-- Use png favicon -->
    <link rel=\"icon\" href=\"/assets/default/images/favicons/favicon.png\">
    <!--[if IE]><link rel=\"shortcut icon\" href=\"/assets/default/images/favicons/favicon.ico\"><![endif]-->

    <!-- Icon for (older) Android devices -->
    <link rel=\"apple-touch-icon-precomposed\" href=\"/assets/default/images/favicons/apple-touch-icon-precomposed.png\" />

    <!-- Windows tile settings -->
    <meta name=\"application-name\" content=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["config"]) ? $context["config"] : null), "siteName"));
        echo "\"/>
    <meta name=\"msapplication-TileColor\" content=\"#272b2e\"/>
    <meta name=\"msapplication-TileImage\" content=\"/assets/default/images/favicons/windows-tile-144x144.png\"/>

    <!-- CSS Styles -->
    <link rel=\"stylesheet\" type=\"text/css\" media=\"all\" href=\"/assets/admin/css/bootstrap.min.css?v=0\" />
    <link href=\"/assets/admin/font-awesome/css/font-awesome.css\" rel=\"stylesheet\">
    <link rel=\"stylesheet\" type=\"text/css\" media=\"all\" href=\"/assets/admin/css/sb-admin.css?v=0\" />


</head>
<body>
    <div id=\"wrapper\">

        <nav class=\"navbar navbar-default navbar-static-top\" role=\"navigation\" style=\"margin-bottom: 0\">
        <div class=\"navbar-header\">
            <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".sidebar-collapse\">
                <span class=\"sr-only\">Toggle navigation</span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
                <span class=\"icon-bar\"></span>
            </button>
            <a class=\"navbar-brand\" href=\"index.html\">";
        // line 49
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["config"]) ? $context["config"] : null), "siteName"));
        echo " - Admin Panel</a>
        </div>
        <!-- /.navbar-header -->

        <ul class=\"nav navbar-top-links navbar-right\">
        <li>
            <a href=\"";
        // line 55
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["config"]) ? $context["config"] : null), "siteUrl"), "html", null, true);
        echo "\"><i class=\"fa fa-home fa-fw\"></i></a>
        </li>
        <li class=\"dropdown\">
            <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">
                <i class=\"fa fa-user fa-fw\"></i>  <i class=\"fa fa-caret-down\"></i>
            </a>
            <ul class=\"dropdown-menu dropdown-user\">
                <li><a href=\"";
        // line 62
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["config"]) ? $context["config"] : null), "adminUrl"), "html", null, true);
        echo "logout/\"><i class=\"fa fa-sign-out fa-fw\"></i> Logout</a>
                </li>
            </ul>
            <!-- /.dropdown-user -->
        </li>
        <!-- /.dropdown -->
        </ul>
        <!-- /.navbar-top-links -->

        </nav>
        <!-- /.navbar-static-top -->

        <nav class=\"navbar-default navbar-static-side\" role=\"navigation\">
            <div class=\"sidebar-collapse\">
                <ul class=\"nav\" id=\"side-menu\">
                    <li>
                        <a href=\"";
        // line 78
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["config"]) ? $context["config"] : null), "adminUrl"), "html", null, true);
        echo "\"><i class=\"fa fa-dashboard fa-fw\"></i> Dashboard</a>
                    </li>
                    <li>
                        <a href=\"";
        // line 81
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["config"]) ? $context["config"] : null), "adminUrl"), "html", null, true);
        echo "news/\"><i class=\"fa fa-dashboard fa-fw\"></i> News</a>
                    </li>

                </ul>
                <!-- /#side-menu -->
            </div>
            <!-- /.sidebar-collapse -->
        </nav>
        <!-- /.navbar-static-side -->

        <div id=\"page-wrapper\">
            <div class=\"row\">
                <div class=\"col-lg-12\">
                    <h1 class=\"page-header\">";
        // line 94
        echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
        echo "</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            ";
        // line 98
        $this->displayBlock('content', $context, $blocks);
        // line 99
        echo "            <!-- /.row -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    <!-- /#wrapper -->

    <!-- Core Scripts - Include with every page -->
    <script src=\"/assets/admin/js/jquery-1.10.2.js\"></script>
    <script src=\"/assets/admin/js/bootstrap.min.js\"></script>
    <script src=\"/assets/admin/js/plugins/metisMenu/jquery.metisMenu.js\"></script>
    <script src=\"/assets/admin/ckeditor/ckeditor.js\"></script>

    <script>
        ";
        // line 113
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["js"]) ? $context["js"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["line"]) {
            // line 114
            echo "           ";
            echo (isset($context["line"]) ? $context["line"] : null);
            echo "
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['line'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 116
        echo "    </script>

    <!-- SB Admin Scripts - Include with every page -->
    <script src=\"/assets/admin/js/sb-admin.js\"></script>

    <script src=\"/assets/admin/js/main.js\"></script>

    <!-- Page-Level Demo Scripts - Blank - Use for reference -->

</body>
</html>";
    }

    // line 98
    public function block_content($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "admin.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  204 => 98,  190 => 116,  181 => 114,  177 => 113,  161 => 99,  159 => 98,  152 => 94,  136 => 81,  130 => 78,  111 => 62,  101 => 55,  92 => 49,  67 => 27,  48 => 11,  38 => 8,  32 => 5,  24 => 3,  20 => 1,  39 => 9,  31 => 3,  28 => 4,);
    }
}
