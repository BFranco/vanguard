<?php

/* admin/login.twig */
class __TwigTemplate_ff37011b288bedb9a2b5a924c9129b4bb562e6d17a55eca846a8df67ee653504 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("admin.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "admin.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "    <div class=\"col-lg-3\">
        <form role=\"form\">
            <div class=\"form-group input-group\">
                <span class=\"input-group-addon\"><i class=\"fa fa-user\"></i></span>
                <input type=\"text\" class=\"form-control\" placeholder=\"Gebruikersnaam\" name=\"user\">
            </div>

            <div class=\"form-group input-group\">
                <span class=\"input-group-addon\"><i class=\"fa fa-lock\"></i></span>
                <input type=\"password\" class=\"form-control\" placeholder=\"Wachtwoord\" name=\"password\">
            </div>
            <button type=\"submit\" class=\"btn btn-default\">Log in</button>
        </form>
    </div>
";
    }

    public function getTemplateName()
    {
        return "admin/login.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  31 => 3,  28 => 2,);
    }
}
