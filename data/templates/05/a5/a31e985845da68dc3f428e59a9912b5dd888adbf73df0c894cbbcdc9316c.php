<?php

/* 404.twig */
class __TwigTemplate_05a5a31e985845da68dc3f428e59a9912b5dd888adbf73df0c894cbbcdc9316c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("front.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "front.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "    <div id=\"error\">
        <div class=\"container\">
            <h1>Error 404</h1>
            <p>Deze pagina bestaat niet!</p>
            <a class=\"button\" href=\"";
        // line 7
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["config"]) ? $context["config"] : null), "siteUrl"), "html", null, true);
        echo "\">Terug naar homepagina</a>
        </div>
    </div>
";
    }

    public function getTemplateName()
    {
        return "404.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  37 => 7,  31 => 3,  28 => 2,);
    }
}
