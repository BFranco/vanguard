<?php

/* components/layout/overview.twig */
class __TwigTemplate_9a6e832879f8e10de175b9b93f4bff1a43bea6b13fd48aabc7c65fbef7e7f295 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<table class=\"table table-striped table-bordered table-hover dataTable no-footer\" id=\"dataTables-example\" aria-describedby=\"dataTables-example_info\">
    <thead>
        <tr role=\"row\">
        ";
        // line 4
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["columns"]) ? $context["columns"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["column"]) {
            // line 5
            echo "            <th>";
            echo twig_escape_filter($this->env, $this->getAttribute((isset($context["column"]) ? $context["column"] : null), "title"), "html", null, true);
            echo " </th>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['column'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 7
        echo "        </tr>
    </thead>
    <tbody>
        ";
        // line 10
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["data"]) ? $context["data"] : null));
        foreach ($context['_seq'] as $context["_key"] => $context["item"]) {
            // line 11
            echo "            <tr>
                ";
            // line 12
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["columns"]) ? $context["columns"] : null));
            foreach ($context['_seq'] as $context["_key"] => $context["column"]) {
                // line 13
                echo "                    <td>
                        ";
                // line 14
                if (($this->getAttribute((isset($context["column"]) ? $context["column"] : null), "data") == "actions")) {
                    // line 15
                    echo "                            ";
                    $context['_parent'] = (array) $context;
                    $context['_seq'] = twig_ensure_traversable($this->getAttribute((isset($context["item"]) ? $context["item"] : null), "actions", array(), "array"));
                    foreach ($context['_seq'] as $context["_key"] => $context["action"]) {
                        // line 16
                        echo "                                <a class=\"btn btn-sm ";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["action"]) ? $context["action"] : null), "button"), "class"), "html", null, true);
                        echo "\" href=\"";
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["config"]) ? $context["config"] : null), "adminUrl"), "html", null, true);
                        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["action"]) ? $context["action"] : null), "action"), "html", null, true);
                        echo "\"><i class=\"fa ";
                        echo twig_escape_filter($this->env, $this->getAttribute($this->getAttribute((isset($context["action"]) ? $context["action"] : null), "button"), "icon"), "html", null, true);
                        echo "\"></i></a>
                            ";
                    }
                    $_parent = $context['_parent'];
                    unset($context['_seq'], $context['_iterated'], $context['_key'], $context['action'], $context['_parent'], $context['loop']);
                    $context = array_intersect_key($context, $_parent) + $_parent;
                    // line 18
                    echo "                        ";
                } else {
                    // line 19
                    echo "                            ";
                    echo twig_escape_filter($this->env, $this->getAttribute((isset($context["item"]) ? $context["item"] : null), $this->getAttribute((isset($context["column"]) ? $context["column"] : null), "data"), array(), "array"), "html", null, true);
                    echo "

                        ";
                }
                // line 22
                echo "                    </td>
                ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['column'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 24
            echo "            </tr>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['item'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 26
        echo "    </tbody>
</table>
";
        // line 28
        echo (isset($context["pagination"]) ? $context["pagination"] : null);
    }

    public function getTemplateName()
    {
        return "components/layout/overview.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  105 => 28,  101 => 26,  94 => 24,  87 => 22,  80 => 19,  77 => 18,  63 => 16,  58 => 15,  56 => 14,  53 => 13,  49 => 12,  46 => 11,  42 => 10,  37 => 7,  28 => 5,  24 => 4,  19 => 1,);
    }
}
