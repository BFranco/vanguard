<?php

/* front.twig */
class __TwigTemplate_9ec7855a7a6cf08fb410242369acf7a90964b7de3b87a28eb9601bddac523671 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>

<!--[if IE 7 ]><html lang=\"";
        // line 3
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["config"]) ? $context["config"] : null), "language"), "html", null, true);
        echo "\" class=\"ie7\"> <![endif]-->
<!--[if lte IE 8 ]> <html lang=\"";
        // line 4
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["config"]) ? $context["config"] : null), "language"), "html", null, true);
        echo "\" class=\"lte-ie8\"> <![endif]-->
<!--[if (gt IE 7)|!(IE)]><!--> <html lang=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["config"]) ? $context["config"] : null), "language"), "html", null, true);
        echo "\"> <!--<![endif]-->

<head>
    <title>";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["title"]) ? $context["title"] : null), "html", null, true);
        if ((isset($context["title"]) ? $context["title"] : null)) {
            echo " | ";
        }
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["config"]) ? $context["config"] : null), "siteName"), "html", null, true);
        echo " - Web developer</title>
    <meta charset=\"utf-8\">

    <meta name=\"description\" content=\"";
        // line 11
        echo twig_escape_filter($this->env, (isset($context["description"]) ? $context["description"] : null));
        echo "\" />
    <meta name=\"keywords\"    content=\"Web development, web, webdesign, Brecht, Franco, Brecht Franco, Limburg, Zolder, Hasselt, freelance, vanguard\" />
    <meta name=\"author\"      content=\"Brecht Franco\">
    <meta name=\"location\"    content=\"Heusden-Zolder\">
    <meta name=\"role\"        content=\"Web developer\">

    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">

    <!-- Use png favicon -->
    <link rel=\"icon\" href=\"/assets/default/images/favicons/favicon.png\">
    <!--[if IE]><link rel=\"shortcut icon\" href=\"/assets/default/images/favicons/favicon.ico\"><![endif]-->

    <!-- Icon for (older) Android devices -->
    <link rel=\"apple-touch-icon-precomposed\" href=\"/assets/default/images/favicons/apple-touch-icon-precomposed.png\" />

    <!-- Windows tile settings -->
    <meta name=\"application-name\" content=\"";
        // line 27
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["config"]) ? $context["config"] : null), "siteName"));
        echo "\"/>
    <meta name=\"msapplication-TileColor\" content=\"#272b2e\"/>
    <meta name=\"msapplication-TileImage\" content=\"/assets/default/images/favicons/windows-tile-144x144.png\"/>

    <!-- CSS Styles -->
    <link rel=\"stylesheet\" type=\"text/css\" media=\"all\" href=\"/assets/default/styles/main.css?v=0\" />
    <!--[if (gte IE 6)&(lte IE 8)]>
        <link rel=\"stylesheet\" type=\"text/css\" media=\"all\" href=\"/assets/default/styles/lte-IE8.css?v=0\" />
    <![endif]-->


    <!-- Modernizr -->
    <script src=\"/assets/default/scripts/components/modernizr-custom.js\"></script>

    <!-- jQuery + Selectivizr + Respond -->
    <!--[if (gte IE 6)&(lte IE 8)]>
    <script src=\"//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js\"></script>
    <script>window.jQuery || document.write('<script src=\"a/assets/lib/vendor/jquery/jquery.js\">\\x3C/script>')</script>
    <script type=\"text/javascript\" src=\"/assets/lib/ven ador/selectivizr/selectivizr.js\"></script>
    <![endif]-->
    <script src=\"/assets/lib/vendor/respond/respond.min.js\"></script>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-46492484-1', 'brecht-franco.be');
        ga('send', 'pageview');

    </script>

</head>
<body>
    <div id=\"wrapper\">
        <header>
            <div class=\"picture\">Brecht Franco</div>
        </header>
        ";
        // line 66
        $this->displayBlock('content', $context, $blocks);
        // line 67
        echo "    </div>
    <div id=\"footer\">
        <a class=\"to-top\" href=\"#\"><span class=\"hover\">To top</span></a>
        <div class=\"container\">
            <h3>Site gemaakt op basis van</h3>
            <img class=\"vanguard\" width=\"200\" height=\"55\" src=\"/assets/default/images/vanguard.png\" alt=\"Vanguard Framework\"/>
            <p>&raquo; Eigen framework, in ontwikkeling &laquo;</p>
        </div>
    </div>

    <!-- jQuery -->
    <!--[if (gt IE 8)|!(IE)]><!-->
    <script src=\"/assets/lib/vendor/jquery/jquery.js\"></script>
    <!--<![endif]-->
    <script src=\"/assets/lib/vendor/jquery-placeholder/jquery.placeholder.min.js\"></script>


    <!-- Our own Scripts -->
    <script src=\"/assets/default/scripts/main.js\"></script>

</body>
</html>";
    }

    // line 66
    public function block_content($context, array $blocks = array())
    {
    }

    public function getTemplateName()
    {
        return "front.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  136 => 66,  111 => 67,  109 => 66,  67 => 27,  48 => 11,  38 => 8,  32 => 5,  24 => 3,  20 => 1,  207 => 129,  201 => 128,  196 => 126,  189 => 124,  186 => 123,  180 => 122,  175 => 120,  168 => 118,  165 => 117,  159 => 116,  154 => 114,  147 => 112,  144 => 111,  140 => 109,  138 => 108,  31 => 3,  28 => 4,);
    }
}
