<?php

/* admin/overview.twig */
class __TwigTemplate_946e826d9ed0f1d7aa1ba464ddbc949cdda34c97fcbc64acdefc1ea3f52740b6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = $this->env->loadTemplate("admin.twig");

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "admin.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $this->parent->display($context, array_merge($this->blocks, $blocks));
    }

    // line 2
    public function block_content($context, array $blocks = array())
    {
        // line 3
        echo "<div class=\"row\">
    <div class=\"col-lg-12\">
        <div class=\"form-group\"><a class=\"btn btn-success btn-lg\" href=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["config"]) ? $context["config"] : null), "adminUrl"), "html", null, true);
        echo "news/new/\">Nieuw Bericht</a></div>
    </div>
</div>
<div class=\"row\">
    <div class=\"col-lg-12\">";
        // line 9
        echo (isset($context["content"]) ? $context["content"] : null);
        echo "</div>
</div>
";
    }

    public function getTemplateName()
    {
        return "admin/overview.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 9,  35 => 5,  31 => 3,  28 => 2,);
    }
}
