<?php

/* components/form/textarea.twig */
class __TwigTemplate_e00e44611b172ae3750400640eae96a508b909692c9d4640683413ca0f745257 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<div class=\"form-group";
        if ((isset($context["invalid"]) ? $context["invalid"] : null)) {
            echo " has-error";
        }
        echo "\">
    <label>";
        // line 2
        echo twig_escape_filter($this->env, (isset($context["label"]) ? $context["label"] : null), "html", null, true);
        echo "</label>
    <textarea class=\"form-control\" rows=\"3\" id = \"";
        // line 3
        echo twig_escape_filter($this->env, (isset($context["id"]) ? $context["id"] : null), "html", null, true);
        echo "\" name=\"";
        echo twig_escape_filter($this->env, (isset($context["name"]) ? $context["name"] : null), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["value"]) ? $context["value"] : null), "html", null, true);
        echo "</textarea>
    ";
        // line 4
        if ((isset($context["help"]) ? $context["help"] : null)) {
            echo " }}<p class=\"help-block\">Example block-level help text here.</p>";
        }
        // line 5
        echo "</div>";
    }

    public function getTemplateName()
    {
        return "components/form/textarea.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  42 => 5,  38 => 4,  60 => 10,  57 => 9,  48 => 7,  43 => 6,  40 => 5,  36 => 4,  30 => 3,  26 => 2,  19 => 1,);
    }
}
