<?php

/* front.tpl */
class __TwigTemplate_c574209eed04cf4a0e0b54a575f83b00e20d1e31322ca7ec155da5868026cbed extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'content' => array($this, 'block_content'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<!doctype html>

<!--[if IE 7 ]>     <html lang=\"{\$lang}\" class=\"ie7\"> <![endif]-->
<!--[if lte IE 8 ]> <html lang=\"{\$lang}\" class=\"lte-ie8\"> <![endif]-->
<!--[if (gt IE 7)|!(IE)]><!--> <html lang=\"en\"> <!--<![endif]-->

<head prefix=\"og: http://ogp.me/ns# fb: http://ogp.me/ns#\">
    <meta charset=\"utf-8\">

    <meta name=\"description\" content=\"{\$metaDesc|default:\$config->metaDescription|strip_tags|replace:\"\\n\":' '|strip|trim|truncate:155|escape}\" />
    <meta name=\"keywords\"    content=\"{\$metaKeywords|default:\$config->metaKeywords|escape}\" />

    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">

    <!-- Use png favicon -->
    <link rel=\"icon\" href=\"/favicon.png\">
    <!--[if IE]><link rel=\"shortcut icon\" href=\"/favicon.ico\"><![endif]-->

    <!-- Icon for (older) Android devices -->
    <link rel=\"apple-touch-icon-precomposed\" href=\"/assets/default/images/icons/apple-touch-icon-precomposed.png\" />

    <!-- Windows tile settings -->
    <meta name=\"application-name\" content=\"{\$config->siteName|escape}\"/>
    <meta name=\"msapplication-TileColor\" content=\"#ffffff\"/>
    <meta name=\"msapplication-TileImage\" content=\"/assets/default/images/icons/windows-tile-144x144.png\"/>

    <!-- CSS Styles -->
    <!--[if (gte IE 6)&(lte IE 8)]>
    <link rel=\"stylesheet\" type=\"text/css\" media=\"all\" href=\"/assets/default/styles/lte-IE8.css?v=0\" />
    <![endif]-->
    <!--[if (gt IE 8)|!(IE)]><!-->
    <link rel=\"stylesheet\" type=\"text/css\" media=\"all\" href=\"/assets/default/styles/main.css?v=0\" />
    <!--<![endif]-->


    <!-- Modernizr -->
    <script src=\"/assets/default/scripts/components/modernizr-custom.js\"></script>

    <!-- jQuery + Selectivizr + Respond -->
    <!--[if (gte IE 6)&(lte IE 8)]>
    <script src=\"//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js\"></script>
    <script>window.jQuery || document.write('<script src=\"/assets/lib/vendor/jquery/jquery.js\">\\x3C/script>')</script>
    <script type=\"text/javascript\" src=\"/assets/lib/vendor/selectivizr/selectivizr.js\"></script>
    <script src=\"/assets/lib/vendor/respond/respond.min.js\"></script>
    <![endif]-->

</head>
<body>

";
        // line 50
        $this->displayBlock('content', $context, $blocks);
        echo "DEEERP

<!-- jQuery -->
<!--[if (gt IE 8)|!(IE)]><!-->
<script src=\"/assets/lib/vendor/jquery/jquery.js\"></script>
<!--<![endif]-->

<!-- Our own Scripts -->


</body>
</html>";
    }

    public function block_content($context, array $blocks = array())
    {
        echo "DEEEEERP";
    }

    public function getTemplateName()
    {
        return "front.tpl";
    }

    public function getDebugInfo()
    {
        return array (  71 => 50,  20 => 1,);
    }
}
