<?php

/* components/layout/detail.twig */
class __TwigTemplate_211ef7dd9559151a51ccc43e1916b7759cead6f3a9fb666e7dbbb62cf668fd22 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        echo "<form role=\"form\" method=\"post\">
    ";
        // line 2
        echo (isset($context["content"]) ? $context["content"] : null);
        echo "
</form>";
    }

    public function getTemplateName()
    {
        return "components/layout/detail.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  22 => 2,  19 => 1,);
    }
}
