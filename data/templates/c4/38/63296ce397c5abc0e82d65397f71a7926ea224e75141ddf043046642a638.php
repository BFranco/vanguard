<?php

/* components/layout/languagetab.twig */
class __TwigTemplate_c43863296ce397c5abc0e82d65397f71a7926ea224e75141ddf043046642a638 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        // line 1
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["config"]) ? $context["config"] : null), "enabledLanguages")) > 1)) {
            echo "<div class=\"panel-body\">";
        }
        // line 2
        echo "    <!-- Nav tabs -->
    ";
        // line 3
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["config"]) ? $context["config"] : null), "enabledLanguages")) > 1)) {
            // line 4
            echo "    <ul class=\"nav nav-tabs\">
        ";
            // line 5
            $context['_parent'] = (array) $context;
            $context['_seq'] = twig_ensure_traversable((isset($context["content"]) ? $context["content"] : null));
            foreach ($context['_seq'] as $context["lang"] => $context["data"]) {
                // line 6
                echo "        <li class=\"";
                if ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "active")) {
                    echo "active";
                }
                if ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "invalid")) {
                    echo " text-danger";
                }
                echo "\">
            <a href=\"#";
                // line 7
                echo twig_escape_filter($this->env, (isset($context["lang"]) ? $context["lang"] : null), "html", null, true);
                echo "\" data-toggle=\"tab\" class=\"";
                if ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "invalid")) {
                    echo " text-danger";
                }
                echo "\">";
                if ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "invalid")) {
                    echo "<i class=\"fa fa-times\"></i> ";
                }
                echo twig_escape_filter($this->env, (isset($context["lang"]) ? $context["lang"] : null), "html", null, true);
                echo "</a>
        </li>
        ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['lang'], $context['data'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 10
            echo "    </ul>
    ";
        }
        // line 12
        echo "
    <!-- Tab panes -->
    ";
        // line 14
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["config"]) ? $context["config"] : null), "enabledLanguages")) > 1)) {
            echo "<div class=\"tab-content\">";
        }
        // line 15
        echo "        ";
        $context['_parent'] = (array) $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["content"]) ? $context["content"] : null));
        foreach ($context['_seq'] as $context["lang"] => $context["data"]) {
            // line 16
            echo "            <div class=\"tab-pane fade ";
            if ($this->getAttribute((isset($context["data"]) ? $context["data"] : null), "active")) {
                echo "active";
            }
            echo " in\" id=\"";
            echo twig_escape_filter($this->env, (isset($context["lang"]) ? $context["lang"] : null), "html", null, true);
            echo "\">
                ";
            // line 17
            echo $this->getAttribute((isset($context["data"]) ? $context["data"] : null), "content");
            echo "
            </div>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['lang'], $context['data'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 20
        echo "        ";
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["config"]) ? $context["config"] : null), "enabledLanguages")) > 1)) {
            echo "</div>";
        }
        // line 21
        if ((twig_length_filter($this->env, $this->getAttribute((isset($context["config"]) ? $context["config"] : null), "enabledLanguages")) > 1)) {
            echo "</div>";
        }
    }

    public function getTemplateName()
    {
        return "components/layout/languagetab.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  103 => 21,  98 => 20,  89 => 17,  80 => 16,  75 => 15,  71 => 14,  67 => 12,  63 => 10,  45 => 7,  35 => 6,  31 => 5,  28 => 4,  26 => 3,  23 => 2,  19 => 1,);
    }
}
