<?php

/**
 * Redirect to the specified page, setting the header and the status code in the process.
 *
 * @param string $destination   The page to redirect to.
 * @param int    $statusCode    An optional HTTP status code.
 * @param array  $customHeaders Optional HTTP headers.
 */

if (!function_exists('redirect')) {

    function redirect($destination, $statusCode = 302, $customHeaders = null)
    {
        $statusCodeMap =
            [
                301 => "Moved Permanently",
                302 => "Found",
                403 => "Forbidden",
                404 => "Not Found",
            ];

        if (array_key_exists($statusCode, $statusCodeMap)) {
            header('HTTP/1.0 ' . $statusCode . ' ' . $statusCodeMap[$statusCode]);
        }

        if (is_array($customHeaders)) {
            foreach ($customHeaders as $customHeader) {
                header($customHeader);
            }
        }

        header("Location: {$destination}");
        exit;
    }
}
