<?php
    if (!function_exists('vardump')) {
        function vardump() {
            $dumper = \getLadybug();
            echo $dumper->dump(func_get_args());
        }
    }