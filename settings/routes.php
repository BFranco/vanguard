<?php
    use \Brecht\Route;
return array(
    'front' => array(
        '/' => new Route('Home', 'home'),
        '/news/' => new Route('News', 'overview')
    ),
    'admin' => array(
        '/' => new Route('Home', 'home'),
        '/logout/' => new Route('Home', 'logout'),
        '/login/' => new Route('Home', 'login'),
        '/news/' => new Route('News', 'overview'),
        '/news/edit/:number/' => new Route('News', 'detail', array(2)),
        '/news/remove/:number/' => new Route('News', 'remove', array(2)),
        '/news/new/' => new Route('News', 'detail')
    )
);