<?php

    return array(
        // General
        'siteName'      => 'Brecht Franco',
        'siteUrl'       => 'http://www.vanguard.bf/',

        // Language
        'defaultLanguage'      => 'nl',
        'multilingual'         => true,
        'enabledLanguages'     => array('nl', 'en'),

        // Admin
        'adminSegment'      => 'admin',
        'adminLanguage'     => 'nl',

        // Google Analytics
        'trackingCode' => 'UA-46492484-1',

        // Database
        'dbName'        => 'myapp',
        'dbHost'        => 'localhost',
        'dbUser'        => 'root',
        'dbPassword'    => 'root',

        'defaultHelpers' => array(
            'vardump',
            'redirect'
        )
    );