<?php

    define('ROOT', realpath(dirname(__FILE__)) . '/');
        define('APP', ROOT.'application/');
            define('MODULES', APP.'Modules/');
        define('SETTINGS', ROOT.'settings/');
        define('DATA', ROOT.'data/');
            define('CACHE', DATA.'cache/');
        define('LIBRARIES', ROOT.'libraries/vendor/');
            define('LIBRARIES_BRECHT', LIBRARIES.'Brecht/');
        define('HELPERS', ROOT . 'helpers/');

    set_include_path(LIBRARIES.PATH_SEPARATOR.LIBRARIES_BRECHT.PATH_SEPARATOR.get_include_path());

    require_once LIBRARIES . 'autoload.php';

    // Do after autoload because of object storage in session
    session_start();

    $env = 'DEVELOPMENT';

    if (getenv('APPLICATION_ENV')) {
        $env = getenv('APPLICATION_ENV');
    }

    $config = Brecht\Config\ConfigFactory::create($env);

    $router = new Brecht\Router();
    $router->route();