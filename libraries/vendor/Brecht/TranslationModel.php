<?php

namespace Brecht;

use Brecht\Database\DatabaseFactory;

class TranslationModel extends Model{

    protected $relatedCol;

    public function save ($translations)
    {
        foreach ($translations as $lang => $data) {
            $data['language'] = $lang;
            parent::save($data);
        }
    }

    public function getDb()
    {
        return $this->db;
    }
}