<?php
    namespace Brecht\Config;


    class Config {

        public $vars;

        public function __construct($path)
        {
            $this->vars = include($path);

            foreach ($this->vars as $key => $value) {
                $this->$key = $value;
            }

            $this->adminUrl = $this->siteUrl . $this->adminSegment . '/';

            $this->loadDefaultHelpers();
        }

        // TODO: move helper loading
        public function loadDefaultHelpers()
        {
            if (isset($this->vars['defaultHelpers'])) {
                \Brecht\Helpers::load($this->vars['defaultHelpers']);
            }
        }

        public function __call($name, $args = array())
        {
            vardump($name);die;
        }
    }