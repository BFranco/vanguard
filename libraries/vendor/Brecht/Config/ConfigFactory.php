<?php
namespace Brecht\Config;

    class ConfigFactory {

        private static $config = null;
        public static function create($env = 'DEVELOPMENT')
        {
            if (is_null(self::$config)) {
                self::createConfig($env);
            }

            return self::$config;
        }

        private static function createConfig($env)
        {
            $fileName = 'Config-' . $env . '.php';
            if (file_exists(SETTINGS . $fileName)) {
                self::$config = new \Brecht\Config\Config(SETTINGS . $fileName);
            } else {
                throw new \Exception($fileName . ' not found in path: ' . SETTINGS);
            }
        }
    }