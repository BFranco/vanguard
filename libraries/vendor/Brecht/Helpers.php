<?php

    namespace Brecht;

    class Helpers {
        public static function load($helpers) {
            if (!is_array($helpers)) {
                $helpers = array($helpers);
            }
            foreach ($helpers as $helper) {
                include_once(HELPERS . $helper . '.php');
            }

        }
    }