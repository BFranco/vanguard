<?php

    namespace Brecht;

    use Respect\Validation\Validator as RespectValidator;

    class Validator  {
        protected $rules = array();
        protected $name;
        protected $errors;
        protected static $errorMessages = array(
            'notEmpty' => "{{name}} mag niet leeg zijn.",
            'noWhiteSpace' => "{{name}} mag niet leeg zijn."
        );

        protected $validator;

        public function __construct()
        {
            $this->validator = RespectValidator::create();
        }

        public function addRule($rule, $params)
        {
            $this->rules[] = $rule;
            $this->validator = $this->validator->addRule($rule, $params);
        }

        public function validate($value)
        {
            $this->fillInName();
            try {
                $result = $this->validator->assert($value);
            } catch (\InvalidArgumentException $e) {
                $this->errors = $e->findMessages(self::$errorMessages);
                return false;
            }

            return $result;
        }


        public function setName($name)
        {
            $this->name = $name;
        }

        private function fillInName()
        {
            $rules = $this->validator->getRules();
            foreach ($rules as $rule) {
                $rule->setName($this->name);
            }
        }

        public function getErrors()
        {
            return isset($this->errors) ? $this->errors : null;
        }
    }