<?php

    namespace Brecht;

    use Brecht\Database\Database;
    use Brecht\Database\DatabaseFactory;

    class Model {
        protected $db;
        protected $config;

        protected $key = array('id');
        protected $columns;

        public function __construct()
        {
            $this->config = Config\ConfigFactory::create();
            $this->db = DatabaseFactory::create();
        }

        public function save ($record)
        {
            $columns = array();
            $values = array();
            $update = array();
            foreach ($record as $column => $value) {
                $columns[] = $column;
                $values[] = ":{$column}_val";
                $update[] = "{$column} = :{$column}_val";
                $params[":{$column}_val"] = [$value, is_integer($value) ? \PDO::PARAM_INT : \PDO::PARAM_STR];
            }

            $query = "
                  INSERT INTO {$this->table} (" . implode(",", $columns) . ")
                  VALUES (" . implode(",", $values) . ")
                  ON DUPLICATE KEY UPDATE " . implode(', ', $update);


            return $this->getDb()->insertOrUpdate($query, $params);

        }

        public function getDb()
        {
            return $this->db;
        }

        public function keyFilledIn($key, $record)
        {
            if (is_array($key)) {
                foreach ($key as $keyItem) {
                    if (!$this->keyFilledIn($keyItem, $record)) {
                        return false;
                    }
                }
                return true;
            }

            if (array_key_exists($key, $record) && !empty($record[$key])) {
                return true;
            }

            return false;
        }

        public function delete($id)
        {
            $query = "DELETE FROM {$this->table} WHERE id = :id";
            $params = array(
                ':id' => array($id, \PDO::PARAM_INT)
            );

            $this->getDb()->execute($query, $params);
        }
    }