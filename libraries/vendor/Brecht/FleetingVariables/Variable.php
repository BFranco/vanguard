<?php

    namespace Brecht\FleetingVariables;

    class Variable {
        private $value;
        private $lifeTime = 1;

        public function __construct($value = null)
        {
            $this->value = $value;
        }

        public function getValue()
        {
            return $this->value;
        }

        public function getLifeTime()
        {
            return $this->lifeTime;
        }
        public function incrementLife()
        {
            $this->lifeTime++ ;
        }

        public function decrementLife()
        {
            $this->lifeTime--;
        }

        public function isDead()
        {
            return $this->lifeTime < 1;
        }
    }