<?php

    namespace Brecht;

    use Brecht\FleetingVariables;

    class Controller{

        protected $view;
        protected $moduleName;
        protected $params = array();

        public function __construct()
        {
            $this->config = Config\ConfigFactory::create();
        }
        public function display($template, $params = array())
        {
            $view = new View();

            // Merge params
            $params = array_merge($this->params, $params);

            // Add config
            $params['config'] = $this->config;

            $view->setModule($this->moduleName);
            $view->display($template, $this->moduleName, $params);
        }

        protected function assign($name, $value)
        {
            $this->params[$name] = $value;
        }

        protected function setTitle($title)
        {
            $tempTitle = null;
            if (!is_array($title)) {
                $tempTitle = $title;
            } else {
                foreach ($title as $titleItem) {
                    if ($titleItem && !is_null($title) && $titleItem != '') {
                        $tempTitle;
                        break;
                    }
                }
            }
            $this->params['title'] = $tempTitle;
        }

        protected function setDescription($desc)
        {
            $tempDesc = null;
            if (!is_array($desc)) {
                $tempDesc = $desc;
            } else {
                foreach ($desc as $descItem) {
                    if ($descItem && !is_null($desc) && $descItem != '') {
                        $tempDesc;
                        break;
                    }
                }
            }
            $this->params['description'] = $tempDesc;
        }

        protected function addJs($js)
        {
            $this->params['js'][] = $js;
        }
    }