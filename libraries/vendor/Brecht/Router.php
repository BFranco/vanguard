<?php

namespace Brecht;

use Brecht\Exceptions\RouteException;
use Brecht\Route;
use Brecht\Cache\CacheFactory;
use Brecht\Exceptions\SegmentException;
use Brecht\Uri;

    class Router{

        private $segments;

        private $uri;

        private $routes = array();

        public function __construct()
        {
            $uriHandler = new Uri($_SERVER['REQUEST_URI']);
            $this->isAdmin = $uriHandler->isAdmin();

            $this->uri = $uriHandler->getUri();
            $this->segments = $uriHandler->getSegments();

            $this->getRoutes();
        }

        /**
         * Do the actual routing
         */
        public function route()
        {
            foreach ($this->routes as $key => $route) {
                // No match, skip!
                if (!preg_match($key, $this->uri)) continue;
                // Match! Try to load frontcontroller
                $controller = $this->getController($route->getModule());
                call_user_func_array(array($controller, $route->getFunction()), $this->buildParams($route));
            }

            $errorController = new \Modules\Error\Controllers\FrontController();
            call_user_func_array(array($errorController, 'error404'), array());
        }
        /**
         * Get all defined routes
         */
        public function getRoutes()
        {
            $this->routes = include(SETTINGS . 'routes.php');
            if ($this->isAdmin) {
                $this->routes = $this->routes['admin'];
            } else {
                $this->routes = $this->routes['front'];
            }
            $this->routes = $this->parseRoutes();
        }

        /**
         * Parse the set routes
         */
        private function parseRoutes()
        {
            $postfix = ($this->isAdmin ? 'admin' : 'front');

            $cache = CacheFactory::create();
            $parsed = $cache->get('routes_' . $postfix);

            if ($parsed->getData() && $parsed->getTime() < filemtime(SETTINGS . 'routes.php')) {
                return $parsed->getData();
            }

            $parsed = array();
            foreach ($this->routes as $key => $route) {
                $key = preg_replace('/:string/', '[a-zA-Z0-9_]*', $key);
                $key = preg_replace('/:number/', '[0-9]*', $key);
                $key = str_replace('/', '\/', $key);
                $parsed['/^' . $key . '$/'] = $route;
            }

            $cache->put('routes_' . $postfix, $parsed, 300);

            return $parsed;

        }

        /**
         * Build the parameters given in the Route object
         */
        private function buildParams($route)
        {
            $params = array();
            foreach ($route->getParams() as $param) {
                $params[] = $this->getSegment($param);
            }

            return $params;
        }

        /**
         * Retrieve a segment if it exists
         */
        private function getSegment($index)
        {
            try {
                if (array_key_exists($index, $this->segments)) {
                    return $this->segments[$index];
                }
                throw new SegmentException($index, $this->segments);
            } catch (SegmentException $e) {
                echo $e;
                exit;
            }
        }

        /**
         * Retrieves the controller of a certain module
         */
        private function getController($module)
        {
            try {
                $controller = 'FrontController';

                if ($this->isAdmin) {
                    $controller = 'AdminController';
                }

                $className = "\\Modules\\{$module}\\Controllers\\{$controller}";

                if (!class_exists($className)) {
                    throw new RouteException($controller, $module);
                }
                return new $className();
            } catch (RouteException $e) {
                $e->printMessage();
            }

            exit;
        }
    }