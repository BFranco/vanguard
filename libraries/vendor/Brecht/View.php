<?php
    namespace Brecht;

    use Brecht\Config\ConfigFactory;

    class View {
        private $twig;
        private $loader;
        public function __construct()
        {
            \Twig_Autoloader::register();
            // Look at module specific templates first, then look at global templates.
            $this->loader = new \Twig_Loader_Filesystem(
                array(
                    APP . 'layouts/'
                )
            );

            $this->twig = new \Twig_Environment($this->loader, array(
                'cache' => DATA . 'templates',
                'auto_reload' => true,
                'strict_variables' => false,
                'debug' => true
            ));
            $this->twig->addExtension(new \Twig_Extension_Debug());
        }

        public function setModule($moduleName)
        {
            $this->loader->addPath(MODULES . $moduleName . '/templates/');
        }

        public function display($template, $moduleName, $vars = array())
        {
            try{
                // Add globals
                $twig = array(
                    'session' => $_SESSION,
                    'post' => $_POST,
                    'get' => $_GET,
                    'server' => $_SERVER,
                );
                $this->twig->addGlobal("twig", $twig);
                $this->twig->addGlobal("app", APP);

                // And display template
                $this->twig->display($template, $vars);
            } catch(\Exception $e){
                echo $e->getMessage();
            };
            exit;
        }

        public function render($template, $vars)
        {
            // Always pass config
            $vars = array_merge($vars, array(
                'session' => $_SESSION,
                'post' => $_POST,
                'get' => $_GET,
                'server' => $_SERVER,
                'config' => ConfigFactory::create()
            ));

            return $this->twig->render($template, $vars);
        }
    }