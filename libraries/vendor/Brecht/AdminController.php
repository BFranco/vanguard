<?php

    namespace Brecht;

    class AdminController extends Controller {

        public function __construct()
        {
            parent::__construct();

            if (!$this->checkLogin()) {
                //$this->showLogin();
            }
            FleetingVariables::init();

            $this->lang = $this->config->adminLanguage;
        }

        private function checkLogin()
        {
            if (isset($_SESSION['isAdmin']) && $_SESSION['isAdmin'] === true) {
                return true;
            }
            return false;
        }

        private function showLogin()
        {
            redirect($this->adminUrl . 'login/');
        }



    }