<?php

    namespace Brecht;

    use Brecht\FleetingVariables\Variable;

    class FleetingVariables {
        private static $variables = array();
        private static $run = false;

        public static function init()
        {

            if (isset($_SESSION['fleetingVariables'])) {
                self::$variables = $_SESSION['fleetingVariables'];
            }
            if (self::$run) {
                return false;
            }
            self::removeDead();
            self::ageAll();

            self::$run = true;

        }

        public static function setVariable($namespace, $key, $value)
        {
            $_SESSION['fleetingVariables'][$namespace][$key] = new Variable($value);
        }

        public static function getVariable($namespace, $key)
        {
            return (isset(self::$variables[$namespace][$key])) ? self::$variables[$namespace][$key]->getValue() : null;
        }

        public static function keepAlive($namespace, $key)
        {
            if (isset($_SESSION['fleetingVariables'][$namespace][$key])) {
                $_SESSION['fleetingVariables'][$namespace][$key]->incrementLife();
            }
        }

        public static function removeDead()
        {
            foreach ($_SESSION['fleetingVariables'] as $namespace => $variables) {
                foreach ($variables as $key => $variable) {
                    if ($variable->isDead()) {
                        unset($_SESSION['fleetingVariables'][$namespace][$key]);
                    }
                }
            }
        }

        public static function ageAll()
        {
            foreach ($_SESSION['fleetingVariables'] as $namespace => $variables) {
                foreach ($variables as $variable) {
                    $variable->decrementLife();
                }
            }
        }

    }