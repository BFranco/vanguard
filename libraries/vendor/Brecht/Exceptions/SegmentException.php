<?php
    namespace Brecht\Exceptions;

    class SegmentException extends \Exception {

        protected $index;
        protected $segments;

        public function __construct($index, array $segments)
        {
            $this->index = $index;
            $this->segments = $segments;
        }

        public function __toString()
        {
            $message = '<h2>Segment not found</h2>';
            $message .= 'Segment with index <strong>' . $this->index . '</strong> was not found in these segments:';
            $message .= '<pre>';
            foreach ($this->segments as $key => $value) {
                $message .= '[' . $key . '] => ' . $value . '<br/>';
            }
            $message .= '</pre>';

            $message .= "<h3>File:</h3> {$this->file} <h3>Line:</h3>{$this->line}";

            return $message;
        }
    }