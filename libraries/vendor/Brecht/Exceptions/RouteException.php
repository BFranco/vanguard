<?php
namespace Brecht\Exceptions;

class RouteException extends \Exception {

    protected $class;
    protected $module;

    public function __construct($class, $module)
    {
        $this->class = $class;
        $this->module = $module;
    }

    public function printMessage()
    {
        echo '<h2>Class not found: ' . $this->class . '</h2>';
        echo 'Looking for class <strong>' . $this->class . '</strong> in module: <strong>' . $this->module . '</strong>';
    }
}