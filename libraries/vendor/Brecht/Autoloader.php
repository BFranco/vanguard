<?php
    namespace Brecht;

    class Autoloader{

        public static function register()
        {
            spl_autoload_register(array(__CLASS__, '_autoloader'));
        }

        public static function _autoloader($class)
        {
            //Seeing that we use the same folder structure as namespaces, convert backslashes to regular slashes
            $filename = str_replace('\'', '/', $class) . ".php";

            if (file_exists(LIBRARIES . $filename)) {
                include(LIBRARIES . $filename);

                // Check if class exists
                if (class_exists($class)) {
                    return true;
                }
            }

            return false;
        }
    }