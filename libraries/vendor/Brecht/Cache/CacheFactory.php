<?php
    namespace Brecht\Cache;

    use \Doctrine\Common\Cache\FilesystemCache as FilesystemCache;

    class CacheFactory extends FilesystemCache{
        private static $cache = null;
        public static function create()
        {
            if (is_null(self::$cache)) {
                self::createCache();
            }

            return self::$cache;
        }

        private static function createCache()
        {
            self::$cache = new \Brecht\Cache\Cache(CACHE);
        }
    }