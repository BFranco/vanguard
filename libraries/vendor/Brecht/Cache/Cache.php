<?php
namespace Brecht\Cache;

use \Doctrine\Common\Cache\FilesystemCache as FilesystemCache;

class Cache extends FilesystemCache{
    public function get($index)
    {
        $data = $this->doFetch($index);
        $time = $this->getTime($this->getFilename($index));
        $result = new CacheResult($data, $time);

        return $result;
    }

    public function put($index, $data, $time = 0)
    {
        return $this->doSave($index, $data, $time);
    }

    private function getTime($filename)
    {
        return filemtime($filename);
    }
}