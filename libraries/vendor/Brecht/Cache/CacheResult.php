<?php

    namespace Brecht\Cache;

    class CacheResult {
        private $time = null;
        private $data = null;

        public function __construct($data, $time)
        {
            $this->data = $data;
            $this->time = $time;
        }

        public function getData()
        {
            return $this->data;
        }

        public function getTime()
        {
            return $this->time;
        }
    }