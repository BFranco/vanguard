<?php
    namespace Brecht\Components;
    use Brecht\Input;
    trait InputComponentTrait {
        public function getFromSubmit()
        {
            return array($this->baseName => Input::postDefault($this->name));
        }
    }