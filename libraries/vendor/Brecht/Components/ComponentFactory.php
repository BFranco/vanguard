<?php

    namespace Brecht\Components;

    class ComponentFactory {
        static $components = array();
        public static function create ($component, $params, $environment = 'Front', &$parent = null) {

            if (class_exists("Brecht\\Components\\{$environment}\\Form\\{$component}")) {
                $componentClass = "Brecht\\Components\\{$environment}\\Form\\{$component}";
            } else {
                $componentClass = "Brecht\\Components\\{$environment}\\Layout\\{$component}";
            }

            $component = new $componentClass($params);

            $component->setParent($parent);

            self::registerComponent($component);

            return $component;
        }

        // Remember references to all made components
        public static function registerComponent($component)
        {
            self::$components[$component->getId()] = &$component;
        }

        public static function getComponentList()
        {
            return self::$components;
        }
    }