<?php

    namespace Brecht\Components;

    use Brecht\Input;
    use Brecht\Validator;
    use Respect\Validation\Exceptions\AllOfException;

    class BaseComponent {
        protected $template = null;
        protected $defaultTemplate = null;
        protected $vars = array();
        protected $name;
        protected $baseName;
        protected $parent = NULL;
        protected $value;
        protected $rules = array();
        protected $invalid = false;
        protected $errors;

        public function __construct($params)
        {
            // Give unique id if none is set
            $params['id'] = (isset($params['id'])) ?: uniqid('component_');

            foreach ($params as $key => $value) {
                $this->$key = $value;
            }

            $this->vars = $params;

            $this->baseName = $this->name;

            if (isset($this->validation)) {
                $this->registerRules($this->validation);
            }
        }

        public function setTemplate($template)
        {
            $this->template = $template;
        }

        public function render($view)
        {
            // Dont bother if there's nothing to output
            if (!$this->template && !$this->defaultTemplate) {
                throw new \Exception('No template or default template set.');
            }

            $template = $this->template ? $this->template : $this->defaultTemplate;
            return $view->render($template, $this->vars);
        }

        public function setParent($parentRef)
        {
            $this->parent = $parentRef;
        }

        public function getId()
        {
            return (isset($this->id)) ? $this->id : false;
        }

        public function needsWysiwyg()
        {
            return (isset($this->wysiwyg)) ? $this->wysiwyg : false;
        }

        public function setName($name)
        {
            $this->name = $name;
            $this->vars['name'] = $name;
        }
        // Get value from submit, else from given item
        // TODO: Split this up
        public function retrieveValue ($item = null)
        {
            $postValue = Input::postDefault($this->name, false);
            if ($postValue !== false) {
                $this->vars['value'] = $postValue;
            } else if ($item && array_key_exists($this->baseName, $item['record'])) {
                $this->vars['value'] = $item['record'][$this->baseName];
            }
        }
        // Implement otherwise if not
        public function getFromSubmit()
        {
            return array();
        }

        // Parse and register rules
        private function registerRules($rules)
        {
            $parsed = array();
            foreach ($rules as $rule) {
                if (is_array($rule)) {
                    $parsed[] = ['rule' => array_shift($rule), 'params' => $rule];
                } else {
                    $parsed[] = ['rule' => $rule, 'params' => []];
                }
            }
            $this->rules = $parsed;
        }

        // Validate the component by the rules that were set
        public function validate()
        {
            if (!isset($this->rules)|| count($this->rules) == 0) {
                return true;
            }
            // Get validator
            $validator = new Validator();

            // Register rules bound to this component
            foreach ($this->rules as $rule) {
                $validator->addRule($rule['rule'], $rule['params']);
            }

            // Set name for error reporting
            if (isset($this->label)) {
                $validator->setName($this->label);
            }

            // Get value from submit
            $value = $this->getFromSubmit();

            // Fix array
            if (is_array($value)) {
                $value = array_shift($value);
            }

            // If not validated, set invalid and return false
            if(!$validator->validate($value)) {
                $this->setInvalid($validator->getErrors());
                return false;
            }

            return true;
        }

        public function isInvalid()
        {
            return $this->invalid;
        }

        public function setInvalid($errors)
        {
            $this->invalid = true;
            $this->vars['invalid'] = true;
            $this->vars['errors'] = $errors;
        }

    }