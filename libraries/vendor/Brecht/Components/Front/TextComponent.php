<?php
    namespace Brecht\Components\Front;

    class TextComponent extends BaseComponent implements ComponentInterface {
        public function __construct($name)
        {
            parent::__construct($name);
            $this->defaultTemplate = __DIR__ . '/templates/text.twig';
        }
        public function validate()
        {

        }
    }