<?php
    namespace Brecht\Components\Front;

    class BaseComponent {
        protected $template = null;
        protected $defaultTemplate = null;
        protected $vars = array();
        protected $name;

        public function __construct($name)
        {
            $this->name = $name;
            $this->vars['name'] = $name;
        }

        public function setTemplate($template)
        {
            $this->template = $template;
        }

        public function output()
        {
            // Dont bother if there's nothing to output
            if (!$this->template && !$this->defaultTemplate) {
                throw new \Exception('No template or default template set.');
            }

            $template = $this->template ? $this->template : $this->defaultTemplate;

            $view = new \Brecht\View();

            return $view->render($template, $this->vars);
        }
    }