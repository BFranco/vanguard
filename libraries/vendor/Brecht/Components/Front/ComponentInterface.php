<?php
    namespace Brecht\Components\Front;

    interface ComponentInterface {
        public function output();
        public function validate();
    }