<?php
namespace Brecht\Components\Admin\Layout;

use Brecht\Components\ComponentFactory;
use Brecht\Input;

class FormComponent extends LayoutComponent {
    protected $model;
    protected $item;
    protected $invalid = false;
    protected $template = 'components/layout/detail.twig';

    public function __construct($init = array())
    {
        parent::__construct($init);

        if (isset($this->model)){
            $modelName =  'Modules\\' . $this->model;
            $this->model = new $modelName;
        }

        $this->handleSubmit();

        $this->item = $this->id ? $this->fetchItem() : array();
        $this->handleCkeditor();
    }

    public function fetchItem()
    {
        $params['id'] = $this->id;

        return call_user_func_array(array($this->model, 'getForAdminDetail'), $params);
    }

    public function output($view)
    {
        $content = $this->render($view);

        $params = array(
            'content' => $content
        );
        return $view->render($this->template, $params);
    }

    public function render($view)
    {
        $rendered = array();
        foreach ($this->components as $component) {
            $component->retrieveValue($this->item);
            $rendered[] = $component->render($view);
        }
        return implode($rendered);
    }

    public function handleCkeditor()
    {
        $components = ComponentFactory::getComponentList();
        $editorIds = array();
        foreach ($components as $id => $component) {
            if ($component->needsWysiwyg()) {
                $editorIds[] = $id;
            }
        }

        $this->js[] = 'var editors = ' . json_encode($editorIds) . ';';
    }

    public function getTitle($lang)
    {
        if (isset($this->item['translations'][$lang][$this->titleIndex])) {
            return $this->item['translations'][$lang][$this->titleIndex];
        }
        return null;
    }

    public function handleSubmit()
    {
        if ($this->isSubmitted()) {
            $data = $this->getFromSubmit();
            if (!$this->invalid && isset($this->callback)) {
                call_user_func_array($this->callback, array('data' => $data));
            }
        }
    }

    public function getFromSubmit()
    {
        $components = $this->getComponents();
        $data = array();
        if ($id = $this->getId()) {
            $data['id'] = $id;
        }
        foreach ($components as $component) {
            $data = array_merge($data, $component->getFromSubmit());
            $component->validate();

            if ($component->isInvalid()) {
                $this->invalid = true;
            }
        }

        return $data;
    }

    private function isSubmitted()
    {
        return Input::postDefault('submit', false);
    }
}