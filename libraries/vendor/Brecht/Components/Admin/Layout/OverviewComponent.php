<?php
    namespace Brecht\Components\Admin\Layout;

    class OverviewComponent extends LayoutComponent {
        protected $columns;
        protected $items;
        protected $model;
        protected $defaultAmount = 20;
        protected $template = 'components/layout/overview.twig';

        public function output($view)
        {
            $params = array(
                'data' => $this->buildRows(),
                'columns' => $this->columns,
                'currentPage'=> $this->getPage(),
                'amountOfPages' => $this->getAmountOfPages(),
                'pagination' => $this->generatePagination($this->getPage(), $this->getAmountOfPages())
            );

            return $view->render($this->template, $params);
        }

        public function buildRows()
        {
            $items = $this->fetchItems();
            $data = array();
            foreach ($items as $item) {
                $temp = array();
                foreach ($this->columns as $column) {
                    $temp[$column['data']] = array_key_exists($column['data'], $item) ? $item[$column['data']] : null;
                }
                if (isset($this->actions)) {
                    $temp['actions'] = $this->getActions($this->actions, $item);
                }
                $data[] = $temp;
            }
            if (isset($this->actions)) {
                $this->columns[] = array('title' => 'Acties', 'data' => 'actions');
            }

            return $data;
        }

        public function getActions($actions, $item)
        {
            $parsed = array();

            foreach ($actions as $action) {
                $params = array();
                foreach ($action['vars'] as $index) {
                    if (array_key_exists($index, $item)) {
                        $params[] = $item[$index];
                    }
                }
                $parsed[] = array(
                    'action' => $this->formatString($action['action'], $params),
                    'button' => $this->getIconFromAction($action)
                );
            }

            return $parsed;
        }

        public function fetchItems()
        {
            $params = $this->buildParameters();

            $modelName =  'Modules\\' . $this->model;
            $this->model = new $modelName;

            return call_user_func_array(array($this->model, 'getForAdminOverview'), $params);
        }

        public function buildParameters()
        {
            $params = array(
                (($this->perPage) ? $this->perPage : $this->defaultAmount), // Amount per page
                $this->getPage() // Page we're on
            );

            return $params;
        }

        private function getPage()
        {
            $page = (isset($_GET['p']) && is_numeric($_GET['p']) && $_GET['p'] > 0) ? $_GET['p'] : 1;
            return $page;
        }

        private function getAmountOfPages()
        {
            $amount = call_user_func_array(array($this->model, 'getAmountForAdminOverview'), array());
            return $amount/$this->perPage;
        }

        private function generatePagination($currentPage, $totalPages)
        {
            if ($totalPages <= 1) {
                return "";
            }
            $pagination = "<ul class='pagination'>";

            // Prev
            if ($currentPage > 1) {
                $pagination .= "<li class='paginate_button previous'><a href='?p=" . ($currentPage-1) ."'>Previous</a></li>";
            } else {
                $pagination .= "<li class='paginate_button previous disabled'><span>Previous</span></li>";
            }

            for ($page = $currentPage-5; $page < $currentPage; $page++) {
                if ($page > 0) {
                    $pagination .= "<li class='paginate_button'><a href='?p={$page}'>{$page}</a></li>";
                }
            }
            $pagination .= "<li class='paginate_button active'><a href='?p={$currentPage}'>{$currentPage}</a></li>";
            for ($page = $currentPage+1; $page <= $totalPages; $page++) {
                $pagination .= "<li class='paginate_button'><a href='?p={$page}'>{$page}</a></li>";
            }

            // Next
            if ($currentPage < $totalPages) {
                $pagination .= "<li class='paginate_button next'><a href='?p=" . ($currentPage+1) ."'>Next</a></li>";
            } else {
                $pagination .= "<li class='paginate_button next disabled'><span>Next</span></li>";
            }

            $pagination .= "</ul>";

            return $pagination;
        }

        public function formatString($string, $params)
        {
            array_unshift($params, $string);
            return call_user_func_array('sprintf', $params);
        }

        private function getIconFromAction($action)
        {
            if (array_key_exists('icon', $action)) {
                return array('icon' => $action['icon'], 'class' => 'btn-default');
            }

            switch ($action['type']) {
                case 'edit':
                    return array('icon' => 'fa-edit', 'class' => 'btn-success');
                case 'remove':
                    return array('icon' => 'fa-times', 'class' => 'btn-danger');
                default:
                    return array('icon' => 'fa-check', 'class' => 'btn-default');
            }
        }
    }