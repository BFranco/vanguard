<?php

    namespace Brecht\Components\Admin\Layout;

    use Brecht\Components\BaseComponent;
    use Brecht\Components\ComponentFactory;
    use Brecht\Components\Admin;

    class LayoutComponent extends BaseComponent{

        protected $view;
        protected $parent = null;
        protected $components = null;
        protected $js = array();

        public function __construct($init = array())
        {
            foreach ($init as $key => $value) {
                $this->$key = $value;
            }

            if (isset($this->components)) {
                foreach ($this->components as &$component) {
                    $component = ComponentFactory::create($component['type'], $component, 'Admin', $this);
                }
            }
        }

        public function getJs()
        {
            return implode($this->js);
        }

        // Implement otherwise if not
        public function isInput()
        {
            return false;
        }

        public function getComponents()
        {
            return $this->components;
        }
    }