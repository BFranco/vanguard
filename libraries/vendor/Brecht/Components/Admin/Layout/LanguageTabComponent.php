<?php

    namespace Brecht\Components\Admin\Layout;
    use Brecht\Config\ConfigFactory;
    use Brecht\Components\ComponentFactory;

    class LanguageTabComponent extends LayoutComponent {
        protected $template = 'components/layout/languagetab.twig';
        protected $config;
        protected $componentsGrouped = array();
        protected $invalidGroup = array();

        // Languagetabs need to be duplicated for all languages
        public function __construct($params = array())
        {
            $this->config = ConfigFactory::create();

            $params['id'] = (isset($params['id'])) ?: uniqid('component_');

            foreach ($params as $key => $value) {
                $this->$key = $value;
            }

            foreach ($this->config->enabledLanguages as $lang) {
                if (isset($this->components)) {
                    foreach ($this->components as $component) {
                        $temp = ComponentFactory::create($component['type'], $component, 'Admin', $this);
                        $temp->setName($temp->name . "_" . $lang);
                        $this->componentsGrouped[$lang][] = $temp;
                    }
                }
            }
        }

        public function render($view)
        {
            $content = $this->renderComponents($view);

            $params = array(
                'content' => $content
            );
            return $view->render($this->template, $params);
        }

        public function renderComponents($view)
        {
            $rendered = array();
            foreach ($this->componentsGrouped as $lang => $components) {
                foreach ($components as $component) {
                    $rendered[$lang][] = $component->render($view);
                }
                $rendered[$lang]['content'] = implode($rendered[$lang]);
                $rendered[$lang]['invalid'] = isset($this->invalidGroup[$lang]) ? $this->invalidGroup[$lang] : false;
            }
            $rendered[$this->config->adminLanguage]['active'] = true;
            return $rendered;
        }

        public function retrieveValue ($item = null)
        {
            foreach ($this->componentsGrouped as $lang => $components) {
                foreach ($components as $component) {
                    if ($item && array_key_exists($lang, $item['translations'])) {
                        $component->retrieveValue(array('record' => $item['translations'][$lang]));
                    } else {
                        $component->retrieveValue();
                    }
                }
            }
        }

        // Languagetab returns translations
        public function getFromSubmit()
        {
            $groups = $this->getComponentsGrouped();
            $data = array(
                'translations' => array()
            );
            foreach ($groups as $lang => $components) {
                $data['translations'][$lang] = array();
                foreach ($components as $component) {
                    $data['translations'][$lang] = array_merge($data['translations'][$lang], $component->getFromSubmit());
                }
            }
            return $data;
        }

        public function getComponentsGrouped()
        {
            return $this->componentsGrouped;
        }

        public function validate()
        {
            $groups = $this->getComponentsGrouped();

            foreach ($groups as $lang => $components) {
                foreach ($components as $component) {
                    $component->validate();
                    if ($component->isInvalid()) {
                        $this->invalid = true;
                        $this->invalidGroup[$lang] = true;
                    }
                }
            }
        }
    }