<?php
    namespace Brecht\Components\Admin\Form;

    use Brecht\Components\InputComponentTrait;
    use Brecht\Validator;

    class TextComponent extends \Brecht\Components\BaseComponent implements \Brecht\Components\FormComponentInterface {
        use InputComponentTrait;
        public $defaultTemplate = 'components/form/text.twig';
        public function __construct($params)
        {
            parent::__construct($params);
        }
    }