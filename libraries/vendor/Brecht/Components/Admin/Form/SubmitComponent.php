<?php
namespace Brecht\Components\Admin\Form;
use Brecht\Components\InputComponentTrait;

class SubmitComponent extends \Brecht\Components\BaseComponent implements \Brecht\Components\FormComponentInterface {

    public $defaultTemplate = 'components/form/submit.twig';
    public function __construct($params)
    {
        parent::__construct($params);
    }
}