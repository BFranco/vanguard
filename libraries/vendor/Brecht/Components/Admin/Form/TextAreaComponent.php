<?php
namespace Brecht\Components\Admin\Form;
use Brecht\Components\InputComponentTrait;

class TextAreaComponent extends \Brecht\Components\BaseComponent implements \Brecht\Components\FormComponentInterface {
    use InputComponentTrait;

    public $defaultTemplate = 'components/form/textarea.twig';
    protected $wysiwyg = true;
    public function __construct($params)
    {
        parent::__construct($params);
    }
}