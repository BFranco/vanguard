<?php
namespace Brecht\Components;

    interface FormComponentInterface {
        public function render($view);
        public function validate();
    }