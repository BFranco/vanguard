<?php

    namespace Brecht\Database;

    use Symfony\Component\Config\Definition\Exception\Exception;

    class Database {

        private $handler;

        const PARAM_COL         = 1001;
        const PARAM_SORT        = 1002;
        const PARAM_LIMIT       = 1003;
        const PARAM_FLOAT       = 1004;
        const PARAM_TABLE       = 1005;
        const PARAM_IN          = 1006;

        const ERR_DB_CONNECT    = 9001;

        const SORT_ASCENDING    = 'ASC';
        const SORT_DESCENDING   = 'DESC';

        public function __construct($dsn, $user, $pwd)
        {
            $this->handler = new \PDO($dsn, $user, $pwd);
        }

        public function getAll($query, $data)
        {
            $statement = $this->execute($query, $data);
            return $statement->fetchAll(\PDO::FETCH_ASSOC);
        }

        public function getRow($query, $data)
        {
            $statement = $this->execute($query, $data);
            return $statement->fetch(\PDO::FETCH_ASSOC);
        }

        public function execute($query, $data)
        {
            try {
                $statement = $this->handler->prepare($query);
                $this->bindParameters($statement, $data);

                $statement->execute();
                if ($statement->errorCode() != '00000') {
                    print_r($statement->queryString);
                    echo '<br/><br/>';
                    print_r($statement->errorInfo());
                    die;
                }
                return $statement;
            } catch (Exception $e) {
                echo $e->getMessage();
                die;
            }
        }

        public function insertOrUpdate($query, $data)
        {
            $this->execute($query, $data);
            return $this->handler->lastInsertId();
        }

        public function bindParameters(&$statement, $data)
        {
            foreach ($data as $placeholder => $replacement) {
                if (is_array($replacement)) {
                    $statement->bindValue($placeholder, $replacement[0], $replacement[1]);
                } else {
                    $statement->bindValue($placeholder, $replacement, $this->getPdoType($replacement));
                }

            }
        }

        public function getValue($query, $data)
        {
            $statement = $this->execute($query, $data);

            return $statement->fetchColumn(0);
        }

        private function getPdoType($value)
        {
            if (is_numeric($value)) {
                return \PDO::PARAM_INT;
            } else {
                return \PDO::PARAM_STR;
            }
        }
    }