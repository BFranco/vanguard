<?php
    namespace Brecht\Database;

    class DatabaseFactory {
        private static $db = null;
        public static function create()
        {
            if (is_null(self::$db)) {
                self::createConfig();
            }

            return self::$db;
        }

        private static function createConfig()
        {
            $config = \Brecht\Config\ConfigFactory::create();
            $dsn = "mysql:host=$config->dbHost;dbname=$config->dbName";
            self::$db = new \Brecht\Database\Database($dsn, $config->dbUser, $config->dbPassword);
        }
    }