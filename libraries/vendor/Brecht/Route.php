<?php

namespace Brecht;

    class Route{
        private $module;
        private $function;
        private $params;

        public function __construct($module, $function, $params = array())
        {
            $this->module = $module;
            $this->function = $function;
            $this->params = $params;
        }
        /*
         * Return the controller
         */
        public function getModule()
        {
            return $this->module;
        }

        public function getFunction()
        {
            return $this->function;
        }

        public function getParams()
        {
            return $this->params;
        }
    }