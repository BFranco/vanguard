<?php

namespace Brecht;

use Brecht\Config\ConfigFactory;

class Uri {
    protected $uri;
    protected $segments;
    protected $config;
    protected $language;
    protected $isAdmin = false;
    public function __construct($uri)
    {
        $uri = strtok($uri,'?');
        $this->config = ConfigFactory::create();
        $this->uri = $uri;
        $this->segments = explode('/', trim($this->uri, '/'));

        $this->detectAdmin();
        $this->detectLanguage();

    }

    public function detectLanguage()
    {
        if (count($this->segments) > 0
            && $this->config->multilingual
            && in_array($this->segments[0], $this->config->enabledLanguages)
        ) {
            $this->config->language = array_shift($this->segments);
        } else {
            $this->config->language = $this->config->defaultLanguage;
        }
    }
    private function detectAdmin()
    {
        // Check if the admin segment is set
        if (count($this->segments) > 0 && $this->segments[0] == $this->config->adminSegment) {
            $this->isAdmin = true;
            // Remove from segments
            array_shift($this->segments);
            // We retrieved the language out of the url, patch it back together without the language
            $this->uri = '/';
            if ($this->segments) {
                $this->uri .= implode('/', $this->segments) . '/';
            }
        }
    }

    public function isAdmin()
    {
        return $this->isAdmin;
    }

    public function getSegments()
    {
        return $this->segments;
    }

    public function getUri()
    {
        return $this->uri;
    }
}