<?php
    namespace Brecht;

    class Input {
        public static function isPost()
        {
            if (empty($_POST)) return false; else return true;
        }

        public static function postDefault($name, $default = null)
        {
            if (array_key_exists($name, $_POST)) {
                return $_POST[$name];
            }

            return $default;
        }

        public static function post($name)
        {
            if (array_key_exists($name, $_POST)) {
                return $_POST[$name];
            }

            return null;
        }

        public static function getDefault($name, $default = null)
        {
            if (array_key_exists($name, $_GET)) {
                return $_GET[$name];
            }

            return $default;
        }
    }