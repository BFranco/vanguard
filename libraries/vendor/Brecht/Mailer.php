<?php

    namespace Brecht;

    class Mailer {

        protected $mailer = null;
        protected $message;
        protected $module = null;

        public function __construct()
        {
            $transport = \Swift_SmtpTransport::newInstance('localhost', 25);
            $this->mailer = \Swift_Mailer::newInstance($transport);
            $this->message = \Swift_Message::newInstance();

            return $this;
        }

        public function setFrom($from = array())
        {
            $this->message->setFrom($from);
            return $this;
        }

        public function setTo($to = array())
        {
            $this->message->setTo($to);
            return $this;
        }

        public function setModule($module)
        {
            $this->module = $module;
            return $this;
        }

        public function setContent($template, $vars)
        {
            $view = new \Brecht\View();

            if (!is_null($this->module)) {
                $view->setModule($this->module);
            }

            $content = $view->render($template, $vars);

            $this->message->setBody($content);
            return $this;
        }

        public function send()
        {
            $this->message->send($this->message);
        }
    }