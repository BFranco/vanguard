<?php

namespace Modules\News\Models;

use Brecht\Model;
use Brecht\Database\Database;
use Brecht\TranslationModel;

class NewsTranslationModel extends TranslationModel{

    public $table = 'news_translations';
    protected $key = array('news_id', 'language');
    protected $relatedCol = 'news_id';

}