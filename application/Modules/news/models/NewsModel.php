<?php

    namespace Modules\News\Models;

    use Brecht\Model;
    use Brecht\Database\Database;

    class NewsModel extends Model{

        public $table = 'news';

        public function getForAdminOverview($amount, $page)
        {
            $query = "
                SELECT n.id, n.time, nt.title

                FROM news n

                INNER JOIN news_translations nt
                ON n.id = nt.news_id

                WHERE nt.language = :lang

                LIMIT :start, :amount

            ";
            $result = $this->getDb()->getAll($query, array(
               ':lang' => $this->config->adminLanguage,
               ':start' =>$amount * ($page -1),
               ':amount' => $amount
            ));


            return $result;
        }

        public function getAmountForAdminOverview()
        {
            $query = "
                SELECT COUNT(*)

                FROM news n

                INNER JOIN news_translations nt
                ON n.id = nt.news_id

                WHERE nt.language = :lang

            ";
            $result = $this->getDb()->getValue($query, array(
                ':lang' => $this->config->adminLanguage,
            ));

            return $result;
        }

        public function getForAdminDetail($id)
        {
            $query = "
                SELECT *

                FROM news n

                WHERE n.id = :id
            ";

            $record = $this->getDb()->getRow($query, array(
                ':id' => $id
            ));

            $query2 = "
                SELECT nt.title, nt.content, nt.language

                FROM news_translations nt

                WHERE nt.news_id = :id
            ";
            $translations = $this->getDb()->getAll($query2, array(
                ':id' => $id
            ));

            $grouped = array();
            foreach($translations as $item)
                $grouped[$item['language']] = $item;

            return array(
                'record' => $record,
                'translations' => $grouped
            );
        }
    }