<?php

    namespace Modules\News\Controllers;

    use Brecht\AdminController as BaseController;
    use Brecht\Components\Admin;
    use Brecht\View;
    use Modules\News\Models\NewsModel;
    use Modules\News\Models\NewsTranslationModel;
    use Brecht\FleetingVariables;

    class AdminController extends BaseController {
        protected $moduleName = 'News';

        public function overview()
        {
            $overview = new Admin\Layout\OverviewComponent(array(
                'model' => 'News\Models\NewsModel',
                'columns' => array(
                    array('title' => 'Titel', 'data' => 'title'),
                    array('title' => 'Datum', 'data' => 'time'),
                ),
                'actions' => array(
                    array('type'=> 'edit', 'action' => 'news/edit/%s/', 'vars' => array('id')),
                    array('type'=> 'remove', 'action' => 'news/remove/%s/', 'vars' => array('id'))
                ),
                'perPage' => 2
            ));

            FleetingVariables::setVariable($this->moduleName, 'overview', $_SERVER['REQUEST_URI']);

            $this->setTitle('News');
            $this->display('admin/overview.twig', array(
                'content' => $overview->output(new View())
            ));
        }

        public function detail($id = null)
        {
            FleetingVariables::keepAlive($this->moduleName, 'overview');
            $backUrl = FleetingVariables::getVariable($this->moduleName, 'overview');

            $this->assign('overviewUrl', $backUrl);

            $detail = new Admin\Layout\FormComponent(array(
                'model' => 'News\Models\NewsModel',
                'id' => $id,
                'components' => array(
                    array(
                        'type' => 'LanguageTabComponent',
                        'components' => array(
                            array('type' => 'TextComponent', 'name' => 'title', 'label' => 'Titel', 'validation' => ['NotEmpty']),
                            array('type' => 'TextAreaComponent', 'name' => 'content', 'label' => 'Inhoud'),
                        ),
                    ),
                    array('type' => 'TextComponent', 'name' => 'author', 'label' => 'Auteur'),

                    array('type' => 'SubmitComponent', 'text' => 'Verzenden')
                ),
                'titleIndex' => 'title',
                'callback' => array($this, 'save')
            ));


            $this->addJs($detail->getJs());

            if ($id) {
                $this->setTitle('Nieuwsitem bewerken' . ' - ' . $detail->getTitle($this->lang));
            } else {
                $this->setTitle('Nieuw nieuwsitem');
            }

            $this->display('admin/detail.twig', array(
                'content' => $detail->output(new View())
            ));
        }

        public function save($data)
        {
            $model = new NewsModel();
            $translationModel = new NewsTranslationModel();

            $translations = $data['translations'];
            unset($data['translations']);

            if (!isset($data['id'])) {
                $data['id'] = $model->save($data);
            }

            foreach ($translations as &$translation) {
                $translation['news_id'] = $data['id'];
            }

            $translationModel->save($translations);
            redirect($this->config->adminUrl . 'news/edit/' . $data['id'] . '/');
        }

        public function remove($id)
        {
            $model = new NewsModel();

            $model->delete($id);

            redirect($this->config->adminUrl . 'news/');
        }
    }