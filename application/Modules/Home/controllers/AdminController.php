<?php
    namespace Modules\Home\Controllers;

    use Brecht\AdminController as BaseController;
    use Brecht\Components\Admin;
    use Brecht\GoogleAnalytics;

    class AdminController extends BaseController {
        protected $moduleName = 'Home';
        public function home()
        {
            $ga = new GoogleAnalytics($this->config->trackingCode, $this->config->siteUrl);
            die;
            $this->setTitle('Dashboard');
            $this->display('admin/home.twig');
        }

        public function logout()
        {
            session_destroy();
            redirect($this->config->siteUrl);
        }

        public function login()
        {
            $this->setTitle('Login');
            $this->display('admin/login.twig');
        }
    }