<?php
    namespace Modules\Home;

    use Brecht\FrontController as BaseController;
    use Brecht\Input;
    use Brecht\Mailer;
    use Brecht\Validator;

    class FrontController extends BaseController{

        protected $moduleName = 'Home';

        public function home()
        {
            if (Input::isPost()) {
                $rules = array(
                    'name' => array('required', 'maxLength:255'),
                    'email' => array('required', 'maxLength:255', 'email'),
                    'message' => array('required')
                );
                $validator = new Validator($rules);

                $data = array(
                    'name' => Input::postDefault('name', null),
                    'email' => Input::postDefault('email', null),
                    'message' => Input::postDefault('message', null),
                );

                if ($validator->validate($data)) {
                    $contactModel = new Models\ContactModel();

                    $mailer = new Mailer();

                    $mailer->setModule('Home')->setFrom('info@brecht-franco.be')->setTo('franco.brecht@gmail.com')
                        ->setContent('mail/mail.twig', $data)->send();

                    $_SESSION['success'] = true;

                    redirect($this->config->siteUrl);
                } else {
                    $this->assign('errors', $validator->getErrors());
                }

            } else if (isset($_SESSION['success'])) {
                unset($_SESSION['success']);
                $this->assign('success', true);
            }

            $this->setDescription('Ik ben Brecht Franco en ik ben een webdeveloper gestationeerd in Limburg. Ik hou mij zowel bezig met front-end als back-end ontwikkeling.');
            $this->display('test.twig');
        }

    }