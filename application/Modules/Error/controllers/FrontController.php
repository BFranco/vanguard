<?php
namespace Modules\Error\Controllers;

use Brecht\Controller as BaseController;

class FrontController extends BaseController{

    public function __construct()
    {
        parent::__construct();
        $this->setTitle('Error');
    }

    protected $moduleName = 'Error';

    public function error404()
    {
        redirect($this->config->siteUrl, 404);
    }

}