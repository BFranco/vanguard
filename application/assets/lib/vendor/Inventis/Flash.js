/**
 * Created with JetBrains PhpStorm.
 * User: janesser
 * Date: 10/09/13
 * Time: 14:32
 * To change this template use File | Settings | File Templates.
 */
define(
    [
        "Inventis/Class"
    ],
    function(Class){
        var __flashClass = 'alert alert-',
            __flashNode = 'div',
            __flashCloseNode = 'i',
            __flashCloseNodeClass = 'icon-remove close',
            __NameSpaces = {};

            Flash = Class.extend({

                _code: null,
                _message: null,
                _time: null,
                _namespace: null,
                _timeout: null,
                _node: null,

                /**
                 * creates a flash message element
                 * from the code and message combination provided
                 *
                 * @param code
                 * @param message
                 * @param time in microseconds this message should stay (undefined is indefinite (till close is clicked)
                 * @private
                 */
                __construct: function(namespace, code, message, time){
                    this._namespace = namespace;
                    this._code = code;
                    this._message = message;
                    if (this._time !== undefined) {
                        this._time = time;
                    }
                },

                renderTo: function(element){
                    var node = document.createElement(__flashNode),
                        close = document.createElement(__flashCloseNode);
                    if (__NameSpaces[this._namespace] !== undefined) {
                        __NameSpaces[this._namespace]._destroyNode();
                    }
                    __NameSpaces[this._namespace] = this;
                    node.className = __flashClass + this._code;
                    node.innerHTML = this._message;
                    close.className = __flashCloseNodeClass;
                    close.addEventListener('click', this._destroyNode.bind(this, node));
                    node.appendChild(close);
                    element.insertBefore(node, element.firstChild);
                    node.scrollIntoView();
                    this._node = node;
                    this._attachDestroyTimer();
                },

                _attachDestroyTimer: function(){
                    if(this._time !== undefined) {
                        this._timeout = setTimeout(this._destroyNode.bind(this), this._time);
                    }
                },

                _destroyNode: function(){
                    clearTimeout(this._timeout);
                    this._node.parentNode.removeChild(this._node);
                    delete __NameSpaces[this._namespace];
                }

            });

        Flash.CODE_ERROR = 'error';
        Flash.CODE_STATUS = 'status';
        Flash.CODE_SUCCESS =  'success';
        Flash.CODE_INFO = 'info';
        return Flash;
    }
);