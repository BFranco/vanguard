/**
 *  file
 *
 * Javascript
 *
 * @category
 * @package    Inventis
 * @subpackage Bricks
 * @author     Inventis Web Architects <info@inventis.be>
 * @license    Copyright © Inventis BVBA  - All rights reserved
 * @link       https://github.com/Inventis/Bricks
 */
define(
    [
        "Inventis/Mixin"
    ],
    function (Mixin, Element) {
        var Draggable = Mixin({
            abstract: ['getDraggableElement', 'getElement', 'getRenderTo'],
            _draggableContainers: [],

            /**
             * define to which containers this component may be dragged
             * @param containers
             * @returns {*}
             */
            setDraggableContainers: function(containers){
                this._draggableContainers = containers;
                return this;
            },

            getDraggableContainers: function(){
                return this._draggableContainers;
            },

            /**
             * this method must be called to initialize a draggable component
             * it will attach listeners to the component and its renderTo
             */
            initDraggable: function(){
                this.initDragNode();
                this.initDragZones();
            },

            initDragNode: function(){
                var draggable = this.getDraggableElement();
                draggable.draggable = true;
                draggable.addEventListener('dragstart', this.onDragStart.bind(this));
                draggable.addEventListener('dragend', this.onDragEnd.bind(this));
            },


            initDragZones: function(){
                var zones = this.getDraggableContainers(),
                    i;
                for (i=0;i<zones.length;i++) {
                    this.initDropZone(zones[i]);
                }
            },

            initDropZone: function(element){
                element.addEventListener('dragover', this.onDragOver.bind(this));
                element.addEventListener('drop', this.onDrop.bind(this));
            },

            onDragOver: function(e){
                e.preventDefault();//indicate drop is allowed
            },

            onDragStart: function(e){
                var element = this.getElement(),
                    positionX = e.x - element.offsetLeft,
                    positionY = e.y - element.offsetTop;

                e.dataTransfer.dropEffect = 'move';
                //the draggable item is not defacto our moving object (e.g. draggable header in window)
                e.dataTransfer.setDragImage(this.getElement(), positionX, positionY);
            },

            onDrop: function(e){
                e.preventDefault();//indicate drop is allowed
            },

            onDragEnd: function(e){
                var positionX = e.x,
                    positionY = e.y,
                    element = this.getElement();
                //for relative parents we must switch to relative positioning
                if (element.parentNode.style.position === 'relative') {
                    positionX = e.target.offsetX;
                    positionY = e.target.offsetY;
                }
                element.style.left = positionX + 'px';
                element.style.top = (positionY - element.offsetHeight) + 'px';
            }

        });
        return Draggable;
    }
);