define(
    [
        "Inventis/Console",
        "Inventis/Mixin",
        "Inventis/Config"
    ],
    function(console, Mixin, Config){

        /**
         * the top level dom element for event listening (or firing if no component element is available)
         * @type {*|HTMLDocument}
         * @private
         */
        var __domListener = window.document;

        /**
         * the property name in the event object that holds options data
         * for later extraction
         * @type {string}
         * @private
         */
        var __optionsProperty = '__observable__options';
        /**
         * if you want all events to show up in the console for debugging
         * you can enable this flag
         * @type {boolean}
         * @private
         * @static
         */
        var __debugEvents = Config.application.debugMode;

        return Mixin({

            /**
             * by default the document is our element for observing
             * @returns HTMLElement
             */
            getElement: function(){
                return __domListener;
            },

            /**
             * removes an event listener from the document or itself when self is true
             * @param name
             * @param {Function} fn the listener function that needs removing
             * @param self
             */
            un: function(name, fn, self){
                var listener = self ? this.getElement() : __domListener;
                listener.removeEventListener(name, fn);
            },

            /**
             * attaches a listener to the document or to its own element if that element is available
             * through the getElement method and self is set to true
             * it is strictly forbidden to listen in on other elements this to avoid tight
             * coupling between components
             * @param eventName
             * @param callback
             * @param {Boolean} [self] [default: false] attach the listener to itself
             * @param {Boolean} [capture] [default: false] listen to reverse stack (true) or normal bubble stack (false)
             */
            on: function(eventName, callback, self, capture){
                var me = this,//reference to this for scope retention inside wrapper

                    //wrap a function around the callback to deal with option extraction and compatibility issues
                    //@TODO we probably need to wrap events for cross browser compatibility (e.g IE cant do event.preventDefault())
                    callbackWrapper = function(event){
                        /*
                         * a browser event wont have an options property set,
                         * in which case we create a reference to a now object inside
                         * the event, which will allow us to pass it on further up the chain
                         */
                        event[__optionsProperty] = event[__optionsProperty] || {};
                        var options = event[__optionsProperty];
                        options.getTarget = function(){
                            return event.target || event.srcElement;
                        };
                        options.isFromParent = function(){
                            return this.getTarget().contains(me.getElement());
                        };

                        var returnValue = callback.call(me, event, options);
                        if (returnValue === false) {
                            //cross browser compliant way of stopping event
                            event.cancelBubble = true;
                            event.stopPropagation();
                        }
                    },

                    listener = self ? (this.getElement() || __domListener) : __domListener;

                listener.addEventListener(eventName, callbackWrapper, capture);
            },

            /**
             * fires an event
             * @param {String} event the name of the event to fire
             * @param {Object} options info you want to pass as 2nd argument to listeners
             * @param {Boolean} [bubble] [Default:true] whether the event should bubble up the dom
             * @param {Boolean} [cancelable] [Default: true] whether a listener can cancel the event
             * @param {Boolean} [self] [Default: true] fire the event from own element (=true) or top level (=false)
             * @returns {*}
             */
            fire: function(event, options, bubble, cancelable, self){
                var evt,
                    self = self !== undefined ? Boolean(self) : true,
                    bubble = bubble !== undefined ? Boolean(bubble) : true,
                    cancelable = cancelable !== undefined ? Boolean(cancelable) : true,
                    triggerElement = self ? (this.getElement() || __domListener) : __domListener;
                var printEventData = function (evt) {
                    if (__debugEvents) {
                        console.debug('Observer Event DEBUG:', event, options, evt, bubble, cancelable);
                    }
                }
                if (!document.createEvent) {
                    // dispatch for paraplegic IE
                    evt = document.createEventObject();
                    evt[__optionsProperty] = options || {};
                    evt.cancelable = cancelable;
                    evt.cancelBubble = !bubble;
                    //@TODO don't we need to blacklist `on + event` methods as some might not need `on` prefixing?
                    printEventData(evt);
                    return triggerElement.fireEvent('on' + event, evt);
                }
                else {
                    // dispatch for real browsers
                    evt = document.createEvent("HTMLEvents");
                    evt.initEvent(event, bubble, cancelable); // event type,bubbling,cancelable
                    evt[__optionsProperty] = options || {};
                    printEventData(evt);
                    return triggerElement.dispatchEvent(evt);
                }
            }
        });
    }
)