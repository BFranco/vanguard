/**
 *  file
 *
 * Javascript
 *
 * @category
 * @package    Inventis
 * @subpackage Bricks
 * @author     Inventis Web Architects <info@inventis.be>
 * @license    Copyright © Inventis BVBA  - All rights reserved
 * @link       https://github.com/Inventis/Bricks
 */
define(
    [
        "Inventis/Mixin"
    ],
    function (Mixin) {
        var __maskClass = 'is-masked';
        var Maskable = Mixin({
            abstract: ['getElement'],

            _mask: null,
            getMask: function(){
                if (this._mask === null) {
                    this._mask = document.createElement('div');
                    this._mask.className = "mask";
                    this._mask.style.top = this.getElement().scrollTop;
                }
                return this._mask;
            },

            /**
             * places a mask is the getElement returned container
             */
            mask: function(){
                var element = this.getElement(),
                    classes = element.className.split(' ');
                element.scrollTop = 0;
                if (classes.indexOf(__maskClass) === -1) {
                    classes.push(__maskClass);
                    element.className = classes.join(' ');
                }
                element.appendChild(this.getMask());
            },

            /**
             * removed a mask from the getElement returned container
             */
            unmask: function(){
                var element = this.getElement(),
                    mask = this.getMask(),
                    classes = element.className.split(' '),
                    maskClassIndex = classes.indexOf(__maskClass);
                if (element.contains(mask)) {
                    if (maskClassIndex !== -1) {
                        classes.splice(maskClassIndex, 1);
                        element.className = classes.join(' ');
                    }
                    element.removeChild(mask);
                }
            }
        });
        return Maskable
    }
);