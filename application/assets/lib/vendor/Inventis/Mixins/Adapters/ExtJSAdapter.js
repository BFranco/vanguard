/**
 * ExtJSAdapter.js file
 *
 * Javascript
 *
 * @category
 * @package    Inventis
 * @subpackage Bricks
 * @author     Inventis Web Architects <info@inventis.be>
 * @license    Copyright © Inventis BVBA  - All rights reserved
 * @link       https://github.com/Inventis/Bricks
 */

/**
 * This Adapter mixin can be used by a class if adapter functionality needs to be implemented
 * it will wrap the most useful features of the framework
 * this mixin already uses the observable mixin as it requires its usage
 * this adapter was written for ExtJS 4.2
 */
define(
    [
        "Inventis/Console",
        "Inventis/Mixin",
        "Inventis/Mixins/Observable",
        //we might be able to only load-in the features that we needs?
        "ExtJS",
        //fix all EXT bugs through overrides
        "Inventis/Ext/Overwrites"
    ],
    function (console, Mixin, Observable) {
        //set adapter defaults
        Ext.Ajax.timeout = 20000;//20 sec
        Ext.Ajax.method = 'POST';//use post as default

        //fixes an Ext bug with tooltips that become to small to be usable
        //and a bug in IE10 where they are not ven drawn
        delete Ext.tip.Tip.prototype.minWidth;
        if(Ext.isIE10) {
            Ext.supports.Direct2DBug = true;
        }

        //ensure tooltips get initialized
        Ext.onReady(function(){
            Ext.QuickTips.init();
        });

        //return the adapter methods
        return Mixin({
            use: [Observable],

            /**
             * object holds json methods like encode/decode
             */
            JSON: {
                /**
                 * method will return a decoded version of the data provided
                 * @param data
                 * @throws SyntaxError
                 * @return {Object}
                 */
                decode: function(data){
                    return Ext.JSON.decode(data);
                },
                /**
                 * encode data into a json object
                 * @param data
                 * @returns {*}
                 */
                encode: function(data){
                    return Ext.JSON.encode(data);
                }
            },

            /**
             * function to use when you need code executed as soon as
             * the dom is ready for ui interaction
             * @param {Function} callback
             * @param {Object} scope for callback function
             */
            ready: function(callback, scope){
                /*
                 * we need to ensure scope is maintained when method is called
                 * so we wrap the method and apply scope as soon as we get called
                 */
                var callbackScopeWrapper = function(){
                    callback.call(scope);
                };
                Ext.onReady(callbackScopeWrapper);
            },

            /**
             * ajax calls are initiated through this method
             * the call will call the callback with 2 arguments:
             *  - the response
             *  - the options object that was used for the call
             * @param {String} url
             * @param {Object} data the parameters that should be included in the request
             * @param {Function} callback the callback method on ajax completion
             * @param {Object} scope
             * @param {Object} [headers]
             *
             * @events beforeAjaxRequest, afterAjaxRequest
             */
            ajax: function(url, data, callback, scope, headers, successHandler, failHandler){
                Ext.Ajax.request({
                    url: url,
                    params: data,
                    headers: headers,
                    success: successHandler,
                    failure: failHandler,
                    scope: scope
                });
            },

            /**
             * this is the default handler for ajax calls that fail
             * @param response
             * @param options
             */
            ajaxFailureHandler: function(response, options){
                console.log('@todo ajax failures are not handled by this adapter handler yet');
            },

            /**
             * holds more information about the browser currently in use
             */
            browser: {
                /**
                 * define browser type in a uniform way
                 * result would look like type: "CHROME" is isChrome
                 */
                type: (Ext.isIE ? "IE"
                    :  Ext.isGecko ? "FF"
                    :  Ext.isChrome ? "CHROME"
                    :  Ext.isOpera ? "OPERA"
                    :  Ext.isSafari ? "SAFARI"
                    : null),
                IE: {
                    /**
                     * important: version here does not account for document mode
                     * so this is not always true:  IE.is8 == (IE.version == 8)
                     */
                    version: Ext.ieVersion,
                    lt8 : Ext.isIE8m,
                    lte8: Ext.isIE8m || Ext.isIE8,
                    is8 : Ext.isIE8,
                    gt8 : Ext.isIE8p
                },
                FF: {
                    version: Ext.firefoxVersion
                },
                CHROME: {
                    version: Ext.chromeVersion
                },
                OPERA: {
                    version: Ext.operaVersion
                },
                SAFARI: {
                    version: Ext.safariVersion
                },
                /**
                 * object holds support flags for various features
                 */
                support: {
                    XMLHttpRequestLevel2: Boolean(window.FormData)
                }
            },

            /**
             *
             * @param element
             * @param animation
             * @param duration
             * @returns Ext.fx.Anim
             */
            animate: function(element, animation, duration) {
                return Ext.create("Ext.fx.Anim", {
                    target: new Ext.dom.Element(element),
                    duration: duration,
                    to: animation
                });
            },

            /**
             * merge 2 object together, overwriting the properties from the receiver
             * @param {Object} receiver the base object to start from
             * @param {Object} provider the object that has properties that should go to the receiver
             */
            merge: function(receiver, provider){
                return Ext.apply(receiver, provider);
            },

            /**
             * outputs a prompt that can be answered by ok or cancle
             * that will fire the optional callback when the user clicks OK
             * @param title
             * @param message
             * @param callback
             */
            prompt: function(title, message, callback){
                Ext.Msg.prompt(title, message, callback);
            },

            /**
             * outputs a alert that on closure calls the optional callback
             * @param title
             * @param message
             * @param [callback]
             */
            alert: function(title, message, callback){
                Ext.Msg.alert(title, message, callback);
            }

        });
    }
);