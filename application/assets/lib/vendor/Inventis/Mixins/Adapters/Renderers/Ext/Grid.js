/**
 *  file
 *
 * Javascript
 *
 * @category
 * @package    Inventis
 * @subpackage Bricks
 * @author     Inventis Web Architects <info@inventis.be>
 * @license    Copyright © Inventis BVBA  - All rights reserved
 * @link       https://github.com/Inventis/Bricks
 */
define(
    [
        "Inventis/Console",
        "Inventis/Mixin",
        "Inventis/Config",
        "Inventis/HTML/Element",
        "Inventis/Application/Webadmin",
        "ExtJS"
    ],
    function (console, Mixin, Config, Element, WA) {

        var Grid = Ext.grid.Panel;
        var Store = Ext.data.Store;

        return Mixin({
            //enforce certain abstract implementations used by this mixin
            abstract: ['getRenderTo', 'getElement', 'getActionUrl'],

            _gridInstance: null,
            _gridConfig: {},
            _gridIsReady: false,
            /**
             * array of AMD loaded renderers
             */
            _gridRenderers: {},
            /**
             * array of AMD loaded handlers
             */
            _gridHandlers: {},
            /**
             * array of AMD loaded listeners
             */
            _gridListeners: {},
            /**
             * to ensure our grid only renders when certain dependencies are finished loading
             * every require that has dependencies that the grid relies on, should ass it to this list
             * so that we can hold the rendering until dependency loading is finished
             */
            _gridLoadedDependencies:[],
            _gridColumns: [],

            _gridStoreInstance: null,
            _gridStoreConfig: null,
            _gridStoreIsReady: false,
            _gridStoreFields: [],
            _gridToolbar: [],
            _gridActionColumn: [],
            _gridDropGroup: null,

            /**
             *
             * @param setup
             */
            setupGridFromConfig: function(setup) {
                //load the renderer and handler calls first as they are AMD loaded
                this.setGridCallbacksFromConfig(setup);
                this.setGridColumnsFromConfig(setup);
                this.setGridConfigFromConfig(setup);
                this.setGridStoreConfigFromConfig(setup);
                this.setGridToolbarFromConfig(setup);
                this.setGridActionColumnFromConfig(setup);
                this.attachListeners();
            },

            /**
             * Attach all listeners
             */
            attachListeners: function() {
                this.on('domRendered', this.onDomRender);
                this.on('redraw', this.onRedraw, true);
            },

            onFilterChange: function(store, filters, eOpts) {
                this.fire('filterChange', {filters: filters}, true);
            },

            onRedraw: function(e, options) {
                try {
                    this.getGridInstance().show();
                    this.getGridInstance().setSize();
                    this.refreshView();
                } catch (e) {
                }
            },

            refreshView: function() {
                this.getGridInstance().getView().refresh();
            },

            /**
             * Fired when DOM is ready of child components
             * @param e
             * @param options
             */
            onDomRender: function(e, options)
            {
                if (this.getGridIsReady()) {
                    if (this.isDescendant(options.getTarget())) {
                        this.getGridInstance().setSize();
                    }
                }
            },

            setGridActionColumnFromConfig: function(setup)
            {
                if (setup.actionColumn) {
                    this.setGridActionColumn(setup.actionColumn);
                }
            },

            setGridActionColumn: function(actionColumn)
            {
                this._gridActionColumn = actionColumn;
                return this;
            },

            getGridActionColumn: function()
            {
                return this._gridActionColumn;
            },

            /**
             * maps the icon to a full path from the icon Url
             * @param {Array} target
             * @param string property [optional][default: 'icon']
             */
            mapIcons: function(target, property){
                property = property || 'icon';
                if (target.length) {
                    //prefix all relative icon references with a iconUrl
                    for (var i in target) {
                        if (target.hasOwnProperty(i)) {
                            if (target[i][property] && String(target[i][property]).substr(0,1) !== '/') {
                                target[i][property] = Config.iconUrl + target[i][property];
                            }
                            if (target[i]['menu']) {
                                this.mapIcons(target[i]['menu']);
                            }
                        }
                    }
                }
            },

            /**
             * sets the toolbar flags received from the server to that we can
             * configure the grid with some basic components
             * @param setup
             */
            setGridToolbarFromConfig: function(setup){
                if (setup.toolbar) {
                    this.setGridToolbar(setup.toolbar);

                }
            },

            /**
             * sets the toolbar configuration flags array
             * that will tell the grid what toolbar components to render in addition to the grid
             * @param toolbar
             * @returns {*}
             */
            setGridToolbar: function(toolbar){
                this.mapIcons(toolbar);
                this._gridToolbar = toolbar;
                return this;
            },

            /**
             * returns currently set toolbar configuration flags
             * @returns {*}
             */
            getGridToolbar: function(){
                return this._gridToolbar;
            },

            /**
             *
             * @param setup
             */
            setGridColumnsFromConfig: function(setup){
                if (setup.columns && setup.columns.length) {
                    if (setup.languageColumns) {
                        var languageColumnsSetup,
                            columnIndex =
                                setup.languageColumns.index !== undefined && setup.languageColumns.index !== false
                                    ? setup.languageColumns.index
                                    : (setup.columns.length);

                        delete setup.languageColumns.index;
                        languageColumnsSetup = this.generateLanguageColumns(setup.languageColumns);
                        languageColumnsSetup.unshift(0);
                        languageColumnsSetup.unshift(columnIndex);
                        setup.columns.splice.apply(setup.columns, languageColumnsSetup);
                    }
                    if (setup.actionColumns) {

                    }
                    this.setGridColumns(setup.columns);
                }
            },

            /**
             * builds an array of columns to match the provided column config
             * for each language available in the application Config object
             * @param config
             * @returns {Array}
             */
            generateLanguageColumns: function(config){
                var columns = [];
                for (var code in Config.languages) {
                    columns.push(WA.merge({
                        text: code,
                        sortable: false,
                        //hint to the renderer mapper that variables should be bound to the renderer call
                        rendererVariables: [code]
                    }, config));
                }
                return columns;
            },

            /**
             *
             * @param setup
             */
            setGridCallbacksFromConfig: function(setup){
                if (setup.callbacks) {
                    var callbacks = setup.callbacks
                    if (callbacks.renderers && callbacks.renderers.length) {
                        this.setGridRenderers(callbacks.renderers);
                    }
                    if (callbacks.handlers && callbacks.handlers.length) {
                        this.setGridHandlers(callbacks.handlers);
                    }
                    if (callbacks.listeners && callbacks.listeners.length) {
                        this.setGridListeners(callbacks.listeners);
                    }
                }

            },

            /**
             *
             * @param setup
             */
            setGridConfigFromConfig: function(setup){
                if (setup.config !== undefined) {
                    if (setup.height) {
                        setup.config.height = setup.height;
                    }
                    if (setup.width) {
                        setup.config.width = setup.width;
                    }
                    this.setGridConfig(setup.config);
                }
            },

            /**
             *
             * @param setup
             */
            setGridStoreConfigFromConfig: function(setup){
                if (setup.store && setup.store.config) {
                    this.setGridStoreConfig(setup.store.config);
                }
            },

            /**
             *
             * @returns {*}
             */
            getGridConfig: function(){
                return this._gridConfig;
            },

            /**
             * configuration to pass to the grid component when initiating
             * @param config
             */
            setGridConfig: function(config){
                this._gridConfig = config;
                return this;
            },

            /**
             *
             * @param instance
             * @returns {*}
             */
            setGridInstance: function(instance){
                this._gridInstance = instance;
                this._attachGridEventListeners(instance);
                return this;
            },

            /**
             *
             * @returns {null}
             */
            getGridInstance: function(){
                return this._gridInstance;
            },

            /**
             * returns a fully configured grid config
             * if you want to extend the grid functionality in any way this would be a very good place
             * to start, it takes the base config provided from the server and add some
             * configuration options to it
             * @returns {Object}
             */
            getProcessedGridConfig: function(){
                var me = this,
                    config = this.getGridConfig() || {},
                    listeners = config.listeners || {};
                config.id = this.getId() + '-instance';
                config.renderTo = this.getRenderTo();
                config.store = this.getGridStoreInstance();
                config.columns = this.getProcessedGridColumns();
                config.dockedItems = [{
                    xtype: 'toolbar',
                    dock: 'top',
                    items: this.getProcessedGridToolbarConfig()
                }];
                config.viewConfig = this.getProcessedViewConfig(config);
                listeners.afterRender = function() {
                    this._gridIsReady = true;
                };
                listeners.scope = this;
                config.listeners = listeners;
                this.mapper('listeners', [config], this.getGridListeners());
                if (config.selectionType) {
                    config.selModel = {mode: config.selectionType};

                }
                // don't allow sorting when you're able to move certain items
                if (config.enableDragDrop === true) {
                    config.sortableColumns = false;
                }
                config.autoRender = true;
                return config;
            },

            getProcessedViewConfig: function(config){
                var viewConfig = config.viewConfig,
                    plugins, dd;

                if (config.enableDragDrop !== true && viewConfig && viewConfig.plugins) {
                    if (viewConfig && viewConfig.plugins) {
                        delete viewConfig.plugins.dragDrop;
                    }
                } else {
                    if (!viewConfig || !viewConfig.plugins || !viewConfig.plugins.dragDrop) {
                        //something wrong with expected config
                        throw new Error(
                            'dragDrop is enabled, but no drag drop view plugin config can be found,'+
                                'ensure there is a config.viewConfig.plugins.dragDrop setup'
                        );
                    }
                    plugins = viewConfig.plugins;
                    dd = plugins.dragDrop;
                    dd.dragGroup = dd.dragGroup || this.getRenderTo() + 'DDGroup';
                    dd.dropGroup = dd.dropGroup || this.getRenderTo() + 'DDGroup';
                    this.setGridDropGroup(dd.dropGroup);
                    delete viewConfig.plugins;
                    viewConfig.plugins = [];
                    for (var p in plugins) {
                        viewConfig.plugins.push(plugins[p]);
                    }
                    viewConfig.autoRender = true;
                }
                //map any listeners that might be setup
                this.mapper('listeners', [viewConfig], this.getGridListeners());
                viewConfig.getRowClass = this.getRowClass.bind(this);
                /**
                 * remove plugins property of config when it is empty, because
                 * Extjs will throw an error when it is
                 */
                var pluginsEmpty = true;
                for (var i in viewConfig.plugins) {
                    if (viewConfig.plugins.hasOwnProperty(i)) {
                        pluginsEmpty = false;
                    }
                }
                if (pluginsEmpty) {
                    delete viewConfig.plugins;
                }

                return viewConfig;
            },

            /**
             * sets the drop group configured during grid setup
             * callable by external listeners when they need them
             * @param group
             * @returns {*}
             */
            setGridDropGroup: function(group){
                this._gridDropGroup = group;
                return this;
            },

            /**
             * returns the drop group configured during grid setup
             * @returns {*}
             */
            getGridDropGroup: function(){
                return this._gridDropGroup;
            },

            getProcessedGridColumns: function(){
                //map renderer string names to the the actual renderer functions
                var columns = this.mapper('renderer', this.getGridColumns(), this.getGridRenderers()),
                    actionColumn = this.getProcessedGridActionColumn(),
                    i;

                if (actionColumn !== false) {
                    columns.push(actionColumn);
                }
                for (i=0;i<columns.length;i++) {
                    //flip the default menuDisabled setting
                    columns[i].menuDisabled = columns[i].menuDisabled === undefined ? true :  columns[i].menuDisabled;
                }
                return columns;
            },

            getProcessedGridActionColumn: function(){
                //inject handlers and listeners
                var column = this.getGridActionColumn();
                if (column.length === 0) {
                    return false;
                }
                var actions = this.mapper('handler', column.items, this.getGridHandlers());
                for (i in actions) {
                    actions[i].isDisabled = this.isActionDisabled.bind(this);
                }
                this.mapIcons(column.items);
                column.items = actions;
                column.xtype = column.xtype || 'actioncolumn';
                column.width = column.width || 150;
                column.menuDisabled = column.menuDisabled || true;
                return column;
            },

            /**
             * this is the default handler for action button disable determination
             * it is based on an available dataIndex on the button and will then
             * identify if that dataIndex resolved to true or false in which case the disabled state
             * of the button is resolved accordingly, if however there is no dataIndex column available
             * in the record, no disabling is done
             * @param grid
             * @param row
             * @param index
             * @param button
             * @param record
             * @returns {boolean}
             */
            isActionDisabled: function(grid, row, index, button, record){
                if (!button.dataIndex) {
                    return false;
                }
                var value =  record.get(button.dataIndex);
                return value !== undefined && !value;
            },

            /**
             * this method returns the config based on toolbar flags
             * @param config
             * @return config
             */
            getProcessedGridToolbarConfig: function(){
                var toolbarConfig = this.getGridToolbar();
                this.mapper('handler', toolbarConfig, this.getGridHandlers());
                this.mapper('listeners', toolbarConfig, this.getGridListeners());
                return toolbarConfig;
            },

            /**
             * loads handler dependencies through AMD loading
             * @param {Array} handlers a string array of handler dependencies (files)
             * @returns {*}
             */
            setGridHandlers: function(handlers) {
                this.callbackLoader(handlers, this.setGridHandler);
                return this;
            },

            /**
             * loads handler dependencies through AMD loading
             * @param {Array} listeners a string array of listener dependencies (files)
             * @returns {*}
             */
            setGridListeners: function(listeners) {
                this.callbackLoader(listeners, this.setGridListener);
                return this;
            },

            /**
             * returns listeners defined for the grid
             * @returns {*}
             */
            getGridListeners: function(){
                return this._gridListeners;
            },

            /**
             * adds a single handler to the handlers list
             * @param name
             * @param handler
             * @returns {*}
             */
            setGridHandler: function(name, handler) {
                this._gridHandlers[name] = handler;
                return this;
            },

            /**
             * adds a single listener to the listener list
             * @param name
             * @param listener
             * @returns {*}
             */
            setGridListener: function(name, listener) {
                this._gridListeners[name] = listener;
                return this;
            },

            /**
             * return currently set handlers
             * @returns {*}
             */
            getGridHandlers: function(){
                return this._gridHandlers;
            },

            /**
             *
             * @returns {boolean}
             */
            getGridIsReady: function(){
                return this._gridIsReady;
            },

            /**
             *
             */
            renderGrid: function(){
                require(this._gridLoadedDependencies, function(){
                    var instance = this.createGridInstance(this.getProcessedGridConfig());
                    this.setGridInstance(instance);
                    //ensures that the grid will always fit its assigned values
                    Ext.EventManager.onWindowResize(function () {
                        instance.setSize(undefined, undefined);
                    });
                }.bind(this));
            },

            /**
             * creates a grid instance from the provided config
             * @param config
             * @returns {Ext.grid.Panel}
             */
            createGridInstance: function(config){
                return new Grid(config);
            },

            /**
             *
             * @param instance
             * @returns {*}
             */
            setGridStoreInstance: function(instance){
                this._gridStoreInstance = instance;
                this._attachStoreEventListeners(instance);
                return this;
            },

            /**
             *
             * @returns Ext.data.Store
             */
            getGridStoreInstance: function(){
                if (this._gridStoreInstance === null) {
                    var instance = this.createStoreInstance(this.getProcessedStoreConfig());
                    this.setGridStoreInstance(instance);
                }
                return this._gridStoreInstance;
            },

            createStoreInstance: function(config){
                return new Store(config);
            },

            /**
             *
             * @param config
             * @returns {*}
             */
            setGridStoreConfig: function(config){
                this._gridStoreConfig = config;
                return this;
            },

            /**
             *
             * @returns {null}
             */
            getGridStoreConfig: function(){
                return this._gridStoreConfig;
            },

            /**
             *
             * @returns {*}
             */
            getProcessedStoreConfig: function()
            {
                var me = this,
                    config = this.getGridStoreConfig(),
                    gridConfig = this.getGridConfig(),
                    listeners = gridConfig.store.listeners || {};

                this._defineStoreModel(config);
                config.storeId = 'store_'+this.getId();
                config.proxy = this._buildStoreApiProxy();
                config.autoLoad = gridConfig.store && gridConfig.store.autoLoad !== undefined
                    ? gridConfig.store.autoLoad
                    : true;
                config.groupField = gridConfig.store.groupField;
                config.groupDir = gridConfig.store.groupDir;
                config.sorters = gridConfig.store.sorters;
                config.buffered = gridConfig.buffered !== undefined ? gridConfig.buffered : true;
                // Always filter server-side
                config.remoteFilter = true;
                config.pageSize = gridConfig.pageSize || 100;
                config.listeners = listeners;
                this.mapper('listeners', [config], this.getGridListeners());
                //default the buffer
                if (config.buffered === true) {
                    config.leadingBufferZone = gridConfig.leadingBufferZone || config.pageSize*2;
                    config.trailingBufferZone = gridConfig.trailingBufferZone || config.pageSize;
                }
                return config;
            },

            /**
             * returns a store api abject to be used as the stores proxy
             * @returns {{type: string, url: null|string, reader: {type: string, root: string}}}
             * @private
             */
            _buildStoreApiProxy: function() {
                return {
                    type: 'ajax',
                    url: this.getActionUrl('getRecords'),
                    actionMethods : 'POST',
                    extraParams : {
                        componentId : this.getId(),
                        componentAction : 'getRecords'
                    },
                    reader : {
                        type : 'json',
                        root : 'result',
                        totalProperty : 'totalCount'
                    }
                };
            },

            /**
             * defined an Ext object that will be used as the stores model definition
             * @param config
             * @private
             */
            _defineStoreModel: function(config){
                Ext.define(config.model, {
                    extend: 'Ext.data.Model',
                    fields: config.fields
                });
            },

            createModel: function(model) {
                var creation = Ext.create(this.getGridStoreConfig().model,
                    model
                );
                return creation;
            },

            callbackLoader: function(dependencies, addCallback){
                var me = this;
                //push all dependencies on the stack that will be testen before grid rendering
                this._gridLoadedDependencies = this._gridLoadedDependencies.concat(dependencies);
                require(dependencies, function(){
                    for (var i=0;i<arguments.length;i++) {
                        for (var name in arguments[i]) {
                            addCallback.call(me, name, arguments[i][name]);
                        }
                    }
                });
            },

            setGridRenderers: function(renderers){
                this.callbackLoader(renderers, this.setGridRenderer);
                return this;
            },

            /**
             * this method can map certain properties in a target to its string value in from
             * it is useful to map functions to their string name variant like for example renderers
             * and handlers, the server side config does not allow us to define functions that will work
             * when injected, but when loaded through AMD objects we can replace them with the real deal
             * when needed, additionally it binds any variables stored in the {property+'Variables'} property
             *
             * @param string property
             * @param {Array} targets
             * @param {Object} from
             * @returns {*}
             */
            mapper: function(property, targets, from){
                var key, listeners;
                //map string from to functions if defined
                for (key in targets) {
                    if (targets[key]['menu']) {
                        this.mapper(property, targets[key]['menu'], from);
                    }
                    if (!targets[key][property]){
                        //does not have what we need, look no further
                        continue;
                    }

                    if (typeof targets[key][property] === "string") {
                        //simple handler functions (renderers/handlers)
                        if (from[targets[key][property]]) {
                            this._mapProperty(property, targets[key], from[targets[key][property]]);
                        } else {
                            if (console.error) {
                                console.error(
                                    this.getId() + ' has invalid callback defined',
                                    targets[key], property +':',
                                    from
                                );
                            }
                            throw new Error(
                                'You defined a ' + property + ' ['+ targets[key][property] +']' +
                                    ' without a callback function definition in ' + this.getId()
                            );
                        }
                    } else if (typeof targets[key][property] === "object") {
                        //map listeners in a listener compliant way
                        if (property === 'listeners') {
                            for (listener in targets[key][property]) {
                                if (from[targets[key][property][listener]]) {
                                    this._mapProperty(
                                        listener,
                                        targets[key][property],
                                        from[targets[key][property][listener]]
                                    );
                                }
                            }
                        }
                    }
                }
                return targets;
            },

            _mapProperty: function(property, target, callable){
                target[property] = callable;
                //we want scoping of the callable to be this object unless defined otherwise
                target.scope = target.scope || this;
                if (target[property+'Variables'] && target[property+'Variables'].length) {
                    //attach the variables to the arguments passed when renderer is called

                    //first argument passed to bind is scope, be we cant change it as ExtJS will change it
                    target[property+'Variables'].unshift(undefined);
                    /*
                     * bind a renderer scoped function that passes the specified arguments as first
                     * arguments to the function called
                     * e.g default arguments passed are [a, b, c]
                     * we bind the defined variables which prefixes existing arguments
                     * e.g. custom vars are [x,y]
                     * result is [x, y, a, b, c]
                     */
                    target[property] = Function.prototype.bind.apply(
                        target[property],
                        target[property+'Variables']
                    );
                    //no need for you anymore
                    delete target[property+'Variables'];
                }
                return target;
            },

            /**
             * returns a renderer callable if defined
             * @param renderer
             * @returns {*}
             */
            getGridRenderer: function(renderer){
                return this._gridRenderers[renderer];
            },

            /**
             * adds a single renderer
             * @param name
             * @param renderer
             * @returns {this}
             */
            setGridRenderer: function(name, renderer){
                this._gridRenderers[name] = renderer;
                return this;
            },

            /**
             * returns all defines renderer callables
             * @returns {*}
             */
            getGridRenderers: function(){
                return this._gridRenderers;
            },

            /**
             *
             * @param columns
             * @returns {*}
             */
            setGridColumns: function(columns){
                this._gridColumns = columns;
                return this;
            },

            /**
             *
             * @returns {*}
             */
            getGridColumns: function(){
                return this._gridColumns;
            },

            /**
             *
             * @param {Ext.grid.Panel} instance
             * @private
             */
            _attachGridEventListeners: function(instance){
                this.on('addFilters', this.onAddFilters, true);
                this.on('setFilters', this.onSetFilters, true);
                this.on('changeRecordsByFilter', this.onChangeRecordsByFilter, true);
                this.on('clearEntries', this.onClear, true);
                this.on('refreshNode', this.onNodeRefresh, true);
                this.on('refresh', this.onRefresh, true);
                instance.on('itemdblclick', this.onRowDoubleClick, this);
                instance.on('selectionchange', this.onSelectionChange, this);
                instance.on('itemclick', this.onRowClick, this);
                instance.on('filterchange', this.onFilterChange, this);
                instance.on('beforeselect', this.onBeforeSelectDisable, this);
                instance.on('beforeitemclick', this.onBeforeSelectDisable, this);
                instance.on('beforeitemdblclick', this.onBeforeSelectDisable, this);
            },

            clearFilters: function () {
                this.getGridStoreInstance().clearFilter(true);
            },

            addFilters: function(filters) {
                this.getGridStoreInstance().filter(filters);
            },

            setFilters: function(filters) {
                this.clearFilters();
                this.getGridStoreInstance().filter(filters);
            },

            /**
             * Checks whether the given record is filtered by the given filters
             * @param filters the filters to match the record to
             * @param record the record that is checked
             * @returns {boolean} false when the record does not matches the filters, else true
             */
            recordMatchesFilters: function(filters, record) {
                for (x in filters) {
                    if (filters.hasOwnProperty(x)) {
                        var field = filters[x].field,
                            value = filters[x].value;
                        if (field === undefined || value === undefined) {
                            throw new Exception('Filter has no field- or value header');
                        }
                        if (record.data[field] !== value) {
                            return false;
                        }
                    }
                }
                return true;
            },

            /**
             * Clears the grid
             */
            clear: function() {
                this.getGridStoreInstance().removeAll();
            },

            /**
             * Change given fields of record to a specified value
             * @param fields array with keys that represent the columns and
             * values that represent the new value of that column
             * @param record the record to be changed
             */
            changeFieldsOfRecord: function(fields, record) {
                for (field in fields) {
                    if (fields.hasOwnProperty(field)) {
                        record.set(field, fields[field]);
                    }
                }
            },

            /**
             * Returns the index of the first record that matches the filter after a specified index
             * @param filters
             * @param startIndex
             * @returns {*|Number|Number|Number|Number}
             */
            findIndexOfRecordInStoreByFilters: function(filters, startIndex) {
                var foundIndex,
                    me = this;
                foundIndex = this.getGridStoreInstance().findBy(
                    function(record, id) {
                        return me.recordMatchesFilters(filters, record);
                    },
                    filters.value,
                    startIndex
                );
                return foundIndex;
            },

            /**
             * handles the event request for changeRecordsByFilter
             * @param e
             * @param options
             */
            onChangeRecordsByFilter: function(e, options) {
                var filters = options.filters,
                    change = options.change;
                if (filters !== undefined && change !== undefined) {
                    this.changeRecordsByFilter(filters, change);
                }
            },

            /**
             * Callback of the changeRecordsByFilter event, which will change the records matching the given filters, by
             * the given values, ending with a commit
             * @param filters
             * @param change
             */
            changeRecordsByFilter: function(filters, change) {
                var foundIndex = 0;
                if (Object.prototype.toString.call(filters) !== '[object Array]') {
                    filters = [].concat(filters);
                }
                while (foundIndex !== -1) {
                    foundIndex = this.findIndexOfRecordInStoreByFilters(filters, foundIndex);
                    if (foundIndex !== -1) {
                        this.changeFieldsOfRecord(change, this.getGridStoreInstance().getAt(foundIndex++));
                    }
                }
                this.commitChanges();
            },

            /**
             * Commit any changes in the store
             */
            commitChanges: function() {
                this.getGridStoreInstance().commitChanges();
            },

            selectionRefire: function() {
                var grid = this.getGridInstance();
                var selected = grid.getSelectionModel().getSelection();
                var ids = this.getSelectedIds(grid, selected);
                this.fire('selectionChange', {id: ids[0], ids: ids, selected: selected});
            },

            /**
             * callback when a selectionchange event is received
             * @param grid
             * @param selected
             * @param eOpts
             */
            onSelectionChange: function(grid, selected, eOpts) {
                var ids = this.getSelectedIds(grid, selected),
                    options = {};
                this.fire('selectionChange', {id: ids[0], ids: ids, selected: selected});
            },

            getSelectedIds: function(grid, selected) {
                var ids = [], i;
                for (i=0;i<selected.length;i++) {
                    ids.push(selected[i].internalId);
                }
                return ids;
            },

            /**
             * adds a raw data record to the grid's store
             * @param {Object} record
             */
            addRecord: function(record) {
                var result = this.getGridStoreInstance().add(this.createModel(record));
                if (result) {
                    this.fire('recordAdded', {record: result[0]});
                }
                return result;
            },

            onRowClick: function(grid, record, item, index, e, eOpts) {
                this.fire('recordClicked', {id: record.get('id'), record: record.data});
            },

            /**
             * called when the grid fires the itemdblclick event
             * this method will fire the edit record event as a default
             * dblclick action
             */
            onRowDoubleClick: function(grid, record, item, index, e, eOpts) {
                this.fire('recordDoubleClicked', {id: record.get('id'), record: record.data});
            },

            /**
             *
             * @param {Ext.data.Store} instance
             * @private
             */
            _attachStoreEventListeners: function(instance){
                //ext event listener
                instance.on('beforeload', this.onBeforeLoadStore, this);
                instance.on('prefetch', this.onPrefetch, this);
                instance.on('load', this.onLoad, this);
            },

            onLoad: function() {
                this.unmask();
            },

            /**
             *
             * @param store
             * @param operation
             * @param eOpts
             */
            onBeforeLoadStore: function(store, operation, eOpts) {
                this.mask();
            },

            /**
             * this method will ensure the grid displays the move that occurred
             * this method is called by the host component after it verified the move
             * has been completed on the server side
             * @param options
             * @param response
             */
            gridMoveRecord: function(options, response) {
                var start = Math.max(0, options.target.index - (options.position == 'before' ? 1 : 0));
                // TODO investigate add count : 1 diff
                this.getGridStoreInstance().buffered ? {start: start} : {};
                this.gridReloadStore(options);
            },

            /**
             * unified way to correctly reload store data
             * @param options
             */
            gridReloadStore: function(options){
                var store = this.getGridStoreInstance(),
                    grid = this.getGridInstance(),
                    filterOptions = {};
                if (store.buffered) {
                    options.count = options.count || store.pageSize;
                }
                store.reload(options);
                var onLoadRefresh = function(store){
                    grid.getView().refresh();
                    store.un('load', onLoadRefresh);
                    this.unmask();
                }.bind(this);
                store.on('load', onLoadRefresh);
                this.fire('reloaded');
            },

            onNodeRefresh: function(events, options) {
                if (this.getGridStoreInstance().buffered) {
                    var id = parseInt(options.id),
                        node = this.getGridStoreInstance().getById(id);
                    this.updateNode(node);
                } else {
                    this.gridReloadStore({});
                }
                this.selectionRefire();
            },

            updateNode: function(node) {
                var index = 0,
                    options = {};
                if (node) {
                    index = node.index;
                    if (this.getGridStoreInstance().buffered) {
                        options = {start: index, limit: 1};
                    }
                }
                this.gridReloadStore(options);
            },

            onRefresh: function(events, options) {
                this.gridReloadStore({});
                this.selectionRefire();
            },

            /**
             *
             * @param options
             * @param [response] only available if server side requests are made
             * @returns {*}
             */
            gridAddRecord: function(options, response) {
                var record = response ? response.result : options.data;
                return this.addRecord(record);
            },

            /**
             * this method should be called when a record was deleted server-side
             * it will ensure the grids store replicates the effect
             * @param options
             * @param record
             */
            gridDeleteRecord: function(record, response) {
                var options = this.getGridStoreInstance().buffered ? {start: record.index} : {};
                this.gridReloadStore(options);
            },

            /**
             * callback when the clear event is catched. Clears all records from the grid
             * @param e
             * @param options
             */
            onClear: function(e, options) {
                this.clear();
            },

            onSetFilters: function(event, options) {
                this.setFilters(options.filters);
            },

            onAddFilters: function(event, options) {
                this.addFilters(options.filters);
            },

            onBeforeSelectDisable: function(grid, record, index, eOpts) {
                return record.get("allow_edit") !== false;
            },

            onPrefetch: function(store, records, successful, operation, eOpts) {
                for (i in records) {
                    if (records[i].get("allow_edit") === false) {
                        records[i].allowDrag = false;
                    }
                }
            },

            getRowClass: function(record, rowIdx, bla, store) {
                if (record.get('allow_edit') === false) {
                    return "disabled";
                }
                return "";
            }
        });
    }
);