/**
 * FileField file
 *
 * Javascript
 *
 * @category
 * @package    Inventis
 * @subpackage Bricks
 * @author     Inventis Web Architects <info@inventis.be>
 * @license    Copyright © Inventis BVBA  - All rights reserved
 * @link       https://github.com/Inventis/Bricks
 */
define(
    [
        "Inventis/Mixin",
        "ExtJS"
    ],
    function (Mixin) {
        var fileField = Ext.form.File;

        function _attachInstanceEventListeners(){
            var me = this,
                instance = this.getFileFieldInstance();

            instance.on('change', function(field, newValue){
                me.fire('change', {value: newValue}, false, true, true);
            });
        }

        return Mixin({
            _fileFieldInstance:null,
            _fileFieldConfig: {

            },
            _fileFieldIsReady: false,

            setFileFieldConfig: function(config){
                this._fileFieldConfig = config;
                return this;
            },

            getFileFieldConfig: function(){
                return this._fileFieldConfig;
            },

            setFileFieldInstance: function(instance){
                this._fileFieldInstance = instance;
                _attachInstanceEventListeners.call(this);
                return this;
            },

            /**
             *
             * @returns Ext.form.File
             */
            getFileFieldInstance: function(){
                return this._fileFieldInstance;
            },

            setFileFieldValue: function(value){
                this.getFileFieldInstance().setRawValue(value);
                this.getFileFieldInstance().setValue(value);
                return this;
            },

            getFileFieldValue: function() {
                return this.getFileFieldInstance().getValue();
            },

            renderFileField: function(){
                var instance = new fileField(this._getProcessedConfig());
                this.setFileFieldInstance(instance);
            },

            getFileFieldForm: function(){
                return this.getFileFieldElement().form;
            },

            getFileFieldElement: function(){
                return this.getFileFieldInstance().fileInputEl.dom;
            },

            /**
             * updates the config with component specifics
             * @returns {*}
             * @private
             */
            _getProcessedConfig: function(){
                var me = this,
                    config = this.getFileFieldConfig();

                config.renderTo = this.getRenderTo();
                config.name = this.getName();
                config.listeners = {
                    afterrender: function(){
                        me._fileFieldIsReady = true;
                        me.fire('fileFieldReady', {renderedTo: me.getRenderTo()}, false, true, true);
                    }
                };
                return config;
            },

            getFileFieldIsReady: function(){
                return this._fileFieldIsReady;
            },

            getFileFieldFiles: function(){
                return this.getFileFieldElement().files;
            }
        });
    }
);
