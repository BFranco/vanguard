/**
 * CKEditor.js file
 *
 * Javascript
 *
 * @category
 * @package    Inventis
 * @subpackage Bricks
 * @author     Inventis Web Architects <info@inventis.be>
 * @license    Copyright © Inventis BVBA  - All rights reserved
 * @link       https://github.com/Inventis/Bricks
 */

/**
 * This is a renderer mixin that can be swapped out the minute you would like to use
 * a different rich text editor, this is an abstract Rendering adapter that requires
 * the following methods to be present:
 * renderEditor
 * setRichTextInstance
 * getRichTextInstance
 * setEditorConfig
 * getEditorConfig
 */
define(
    [
        "Inventis/Mixin",
        "Inventis/Translator" // used for plugins
    ],
    function (Mixin, _t) {
        this._t = _t;
        // this = window
        // CKEDITOR_BASEPATH is a global variable required by CK editor to load correctly
        this.CKEDITOR_BASEPATH = "/assets/lib/vendor/ckeditor/";


        //private methods that should not be callable by components directly
        /**
         * attached listeners to the current editor instance instance
         * these are listeners which fire new component normalized events instead
         * @private
         */
        function _attachInstanceEventListeners(){
            /* we capture the editor specific event and fire our own component version
             * so that we can standardize across editor implementations
             */
            var me = this,
                instance = this.getRichTextInstance();
            instance.on('instanceReady', function(){
                instance.setData(me._editorTmpValue, function(){
                    this.resetDirty();
                });
                me._editorIsReady = true;
                me.fire('richTextEditorReady', {renderedTo: me.getRenderTo()}, false, true, true);
                return false;//contain the event
            }, true);
        }

        //mixin
        return Mixin({
            _richTextInstance:null,
            _editorConfig:{
                extraPlugins: 'imagepanel',
                toolbar: [
                    ['Styles'],
                    ['Bold', 'Italic'],
                    ['Subscript', 'Superscript'],
                    ['NumberedList', 'BulletedList'],
                    ['ImagePanel'],
                    ['Link', 'Unlink']
                ],
                componentStyles: [
                    // Block-level styles
                    { name: 'Hoofdtitel', element: 'h2' },
                    { name: 'Subtitel' , element: 'h3' }

                    // Inline styles
                    // { name: 'Marker: Yellow', element: 'span', styles: { 'background-color': 'Yellow' } }
                ],
                stylesSet: 'componentStyles'
            },
            _editorIsReady:false,
            //stored to ensure the value is assigned even if the value is assigned before editor ready state
            _editorTmpValue:null,

            /**
             * render the components rich text editor if required
             */
            renderEditor: function(){
                var me = this;
                require(["CKEditor"], function(){
                    var editorConfig = me.getEditorConfig();
                    if (editorConfig.stylesSet) {
                        if (editorConfig.stylesSets === undefined || editorConfig.stylesSets[editorConfig.stylesSet] === undefined) {
                            throw new Error(
                                "Editor config defined a stylesSet ["+editorConfig.stylesSet+"],"+
                                "but it's not defined in the stylesSets config list, add it to your server side config"
                            );
                        }
                        var styleSetName = editorConfig.stylesSet;
                        if (CKEDITOR.stylesSet.get(editorConfig.stylesSet)) {
                            styleSetName += me.getId();
                        }
                        CKEDITOR.stylesSet.add(styleSetName, editorConfig.stylesSets[editorConfig.stylesSet]);
                    }
                    //CKEDITOR.timestamp = this.id + (new Date()).format('U');
                    CKEDITOR.plugins.addExternal('imagepanel', '/assets/lib/vendor/Inventis/CKEditor/plugins/imagepanel/');
                    CKEDITOR.plugins.addExternal('youtube', '/assets/lib/vendor/Inventis/CKEditor/plugins/youtube/');
                    me.setRichTextInstance(
                        // Further configure CKEditor and load custom plugins
                        CKEDITOR.replace(me.getRenderTo(), editorConfig)
                    );
                });
            },

            /**
             * sets an instance of the rich text editor
             * this method is automatically set when the CKEDITOR method
             * has created the instance
             * @param instance
             */
            setRichTextInstance: function(instance) {
                this._richTextInstance = instance;
                //call private attach listeners function in object scope
                _attachInstanceEventListeners.call(this);
                return this;
            },

            /**
             * returns an instance of the rich text editor
             * @throws TypeError when no instance is available
             * @returns {Object}
             */
            getRichTextInstance: function(){
                if (typeof this._richTextInstance !== "object") {
                    throw new TypeError("No instance of CKEditor was found, but is requested.");
                }
                return this._richTextInstance;
            },

            getEditorIsReady: function(){
                return this._editorIsReady;
            },

            /**
             * stores the config object to use with the editor
             * @param config
             */
            setEditorConfig: function(config){
                this._editorConfig = config;
                return this;
            },

            /**
             * returns the stored config for use with the editor instance
             * @returns {*}
             */
            getEditorConfig: function(){
                if (!this._editorConfig.height) {
                    this._editorConfig.height = this.getElement().style.height;
                }
                return this._editorConfig;
            },

            /**
             * this is the editor value update method
             * it changes the content of the editor to the provided value
             * this method will NOT wait to set the value until the editor is ready
             * your component should call the getEditorIsReady method
             * to validate correct path of action
             * @param {String} value
             */
            setEditorValue: function(value){
                if (!this.getEditorIsReady()) {
                    this._editorTmpValue = value;
                } else {
                    this.getRichTextInstance().setData(value, function(){
                        this.resetDirty();
                    });
                }
                return this;
            },

            /**
             * returns the current content of the editor
             * @returns {String}
             */
            getEditorValue: function(){
                if (!this.getEditorIsReady()) {
                    return this._editorTmpValue;
                }
                return this.getRichTextInstance().getData();
            }
        });
    }
);