
/**
 *  file
 *
 * Javascript
 *
 * @category
 * @package    Inventis
 * @subpackage Bricks
 * @author     Inventis Web Architects <info@inventis.be>
 * @license    Copyright © Inventis BVBA  - All rights reserved
 * @link       https://github.com/Inventis/Bricks
 */
define(
    [
        "Inventis/Mixin",
        "Inventis/Config"
    ],
    function (Mixin, Config) {
        var queue,
            loaded;
        require(
            [
                "http://maps.googleapis.com/maps/api/js?v=3&sensor=false&key=" +
                    Config.application.googleAPIKeys['mapsV3'] + "&callback=initializeGM"
            ]
        );
        window.initializeGM = function() {
            loaded = true;
            for (x in queue) {
                if (queue.hasOwnProperty(x)) {
                    queue[x]();
                    queue.splice(x, 1);
                }
            }
        };

        var GoogleMapMixin = Mixin({
            maps: {},
            markers: {},

            toGoogleCoords: function(coords) {
                if (!(coords instanceof window.google.maps.LatLng)) {
                    coords = new window.google.maps.LatLng(coords.lat, coords.lng);
                }
                return coords;
            },

            onMapsLoad: function(callback) {
                queue = queue || [];
                if (!loaded) {
                    queue.push(callback);
                } else {
                    callback();
                }
            },

            setMarkerPosition: function(name, position) {
                this.markers[name].setPosition(this.toGoogleCoords(position));
            },

            addMarker: function(name, options) {
                if (this.markers[name]) {
                    this.setMarkerPosition(name, options.position);
                } else {
                    this.markers[name] = new window.google.maps.Marker(options);
                }
                return this.markers[name];
            },

            clearMarker: function(name) {
                var marker = this.markers[name];
                marker.setMap(null);
                delete this.markers[name];
            },

            initGM: function(element, opts) {
                if (typeof element !== 'object') {
                    element = document.getElementById(element);
                }
                this.maps[element] = new window.google.maps.Map(element, opts);
                return this.maps[element];
            },

            getGMNamespace: function() {
                return window.google.maps;
            }
        });
        return GoogleMapMixin;
    }
);