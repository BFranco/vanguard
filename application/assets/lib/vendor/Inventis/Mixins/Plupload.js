define(
    [
        "Inventis/Mixin",
        "Plupload"
    ],
    function(Mixin, Plupload) {
        var PlupLoad = Mixin({
            /**
             * Config of plupload
             */
            _pluploadConfig: {},

            /**
             * Object of plupload that's created in pluploadInit
             */
            _pluploadObject: null,

            /**
             * Import a whole object of configurations into this plupload config
             * @param config
             */
            setPluploadConfig: function(config) {
                var x;
                for (x in config) {
                    if (config.hasOwnProperty(x)) {
                        this._pluploadConfig[x] = config[x];
                    }
                }
            },

            pluploadRefresh: function() {
                this._pluploadObject.refresh();
            },

            /**
             * Add a new property to the plupload config
             * @param key
             * @param value
             */
            addToPluploadConfig: function(key, value) {
                this._pluploadConfig[key] = value;
            },

            /**
             * Set the id of the link that'll open the browse window
             * @param button id of the link that'll open the browse window
             */
            setBrowseButton: function(button) {
                this.addToPluploadConfig('browse_button', button);
            },

            /**
             * Create new plupload object and initialize it
             */
            pluploadInit: function() {
                this._pluploadObject = new plupload.Uploader(this._pluploadConfig);
                this._pluploadObject.init();
            },

            /**
             * Bind a callback on a plupload event
             * @param event
             * @param callback
             */
            bindPluploadEvent: function(event, callback) {
                this._pluploadObject.bind(event, callback, this);
            },

            /**
             * Start uploading files in buffer
             */
            startPlupload: function() {
                this._pluploadObject.start();
            },

            /**
             * extracts data from a plupload file object and returns
             * the data in a readable object
             * @param file
             * @returns object
             */
            getDataFromFile: function(file) {
                return {
                    id: file.id,
                    filename: file.name,
                    size: file.size,
                    progress: file.loaded + '%'
                };
            }
        });
        return PlupLoad
    }
)