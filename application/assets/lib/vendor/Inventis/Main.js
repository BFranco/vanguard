/**
 * main.js is the require.js main starting point file
 * it should be loaded as a default when require.js is initialized in the view
 */

define(
    [
        "Inventis/Config",
        "Inventis/Application/Components/Manager"
    ],
    function(Config, Manager){
        var Module;
        //init async call and set manager
        Manager.createComponent(Config.componentConfig, function(component){
            Module = component;
        });
    }
);