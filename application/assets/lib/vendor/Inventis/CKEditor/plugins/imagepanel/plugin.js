﻿/**
 * Image upload and selection panel for Inventis Webadmin
 *
 * @created 2011-05-17
 * @modified 2013-04-30
 */

CKEDITOR.plugins.add(
    'imagepanel',
    {
        // Initialize plugin
        init : function(editor)
        {
            // Define commands and pass handling to dialog
            editor.addCommand('createImagePanelElement', {
                exec: CKEDITOR.plugins.imagepanel.showImageDialog
            });
            editor.addCommand('deleteImagePanelImage', {
                exec: CKEDITOR.plugins.imagepanel.deleteImage
            });

            // Set image urls
            var relatedModule = window.location.pathname.split('/')[2];
            CKEDITOR.plugins.imagepanel.imageUrls.small     = '/images/' + relatedModule + '/small/';
            CKEDITOR.plugins.imagepanel.imageUrls.medium    = '/images/' + relatedModule + '/medium/';
            CKEDITOR.plugins.imagepanel.imageUrls.banner    = '/images/' + relatedModule + '/banner/';
            CKEDITOR.plugins.imagepanel.imageUrls.lightbox  = '/images/lightbox/';

            // Set plugin path
            CKEDITOR.plugins.imagepanel.pluginPath          = this.path;

            // Add button to toolbar
            editor.ui.addButton('ImagePanel', {
                label       : _t('INVENTIS.CKEDITOR.PLUGINS.IMAGEPANEL.PLUGIN.INSERT_IMAGE', 'Afbeelding invoegen'),
                command     : 'createImagePanelElement',
                icon        : this.path + 'images/icon.png'
            });

            // Add context menu
            if (editor.addMenuItems){
                editor.addMenuGroup('imagepanel');
                editor.addMenuItems({
                    imagepanel  : {
                        label       : _t('INVENTIS.CKEDITOR.PLUGINS.IMAGEPANEL.PLUGIN.EDIT_IMAGE', 'Afbeelding wijzigen'),
                        command     : 'createImagePanelElement',
                        group       : 'imagepanel',
                        icon        : this.path + 'images/icon.png'
                    },
                    imagedelete: {
                        label       : _t('INVENTIS.CKEDITOR.PLUGINS.IMAGEPANEL.PLUGIN.DELETE_IMAGE', 'Afbeelding verwijderen'),
                        command     : 'deleteImagePanelImage',
                        group       : 'imagepanel',
                        icon        : this.path + 'images/icon.png'
                    }
                });
            }

            if(editor.contextMenu){
                editor.contextMenu.addListener(function(element, selection){
                    if(element && element.getName() == 'img' && CKEDITOR.plugins.imagepanel.isEditableImage(element.getAttribute('src'))){
                        return { imagepanel: CKEDITOR.TRISTATE_ON, imagedelete: CKEDITOR.TRISTATE_ON };
                    }
                    return null;
                });
            }

            // Handle doubleclick on inserted image
            editor.on('dialogShow', function(evt) {
                var dialog = evt.data;
                if (dialog.getName() === 'link' && CKEDITOR.plugins.imagepanel.imagePanelDialog.isVisible()) {
                    evt.data.hide();
                }
            }, this);

            // Handle doubleclick on inserted image
            editor.on('doubleclick', function(evt) {
                // Fetch and pass selected image to image dialog
                // getSelectedImage() does not work reliable here in Webkit browsers
                if(evt.data.element && evt.data.element.getName() == 'img') {

                    var dialog = CKEDITOR.plugins.imagepanel.getImagePanelDialog();
                    dialog.mode = 'edit';
                    var element = CKEDITOR.dom.element.get(evt.data.element);

                    if(CKEDITOR.plugins.imagepanel.isEditableImage(element.getAttribute('src'))){
                        CKEDITOR.plugins.imagepanel.showImageDialog(editor, element);
                    }
                }
                return false;
            }, this);

            // Add CSS for inserted images
            CKEDITOR.addCss('figure { cursor: pointer; }');
            CKEDITOR.addCss('.left { float: left; margin: 0 10px 10px 0; }');
            CKEDITOR.addCss('.right { float: right; margin: 0 0 10px 10px; }');
            CKEDITOR.addCss('figure.center { text-align:center; }');
            CKEDITOR.addCss('figure.center img { float: none; clear: both; display: block; margin: 10px auto; }');
        },

        // Initialize data processing
        afterInit: function(editor)
        {
            var dataProcessor   = editor.dataProcessor,
                dataFilter      = dataProcessor && dataProcessor.dataFilter,
                htmlFilter      = dataProcessor && dataProcessor.htmlFilter;

            // Filter input data => editor
            if (dataFilter){
                dataFilter.addRules({
                    elements: {
                        'img': function(element){
                            if(CKEDITOR.plugins.imagepanel.isEditableImage(element.attributes.src)){
                                element.attributes['class'] = (element.attributes['class'] || '') + ' cke_imagepanel_image';
                            }
                            //element.attributes.contenteditable = 'false';
                        }
                    }
                });
            }
        }
    }
);

CKEDITOR.plugins.imagepanel =
{
    // Define path to image formats (created on plugin init)
    imageUrls : {
        small           : null,
        medium          : null,
        banner          : null,
        lightbox        : null
    },

    // Define path to plugin (created on plugin init)
    pluginPath          : null,

    // ImagePanelDialog instance (created on first show)
    imagePanelDialog    : null,

    open: false,

    // Create or replace and insert image element to editor
    createImage: function(editor, oldElement, attributes) {
        // Create DOM element for image
        var imageElement = new CKEDITOR.dom.element('img', editor.document);
        // disable drag and drop
        imageElement.on('mousedown', function(event) {
            event.data.preventDefault();
            event.stop();
            event.cancel();
            return false;
        });

        imageElement.setAttributes({
            'src'                           : CKEDITOR.plugins.imagepanel.imageUrls[attributes.size] + attributes.filename,
            'alt'                           : attributes.alt || ''
        });

        var figureElement = new CKEDITOR.dom.element('figure'),
            figureCaption = new CKEDITOR.dom.element('figcaption');
        figureElement.append(imageElement);
        if (attributes.caption) {
            figureCaption.append(new CKEDITOR.dom.text(attributes.alt || ''));
            figureElement.append(figureCaption);
        }

        var imageClasses = [attributes.align, attributes.size, 'cke_imagepanel_image'];
        figureElement.setAttributes({
            'class'                         : imageClasses.join(' ')
        });

        // Create DOM element for lightbox link
        var linkElement;
        if(attributes.lightbox){
            linkElement = new CKEDITOR.dom.element('a', editor.document);
            linkElement.setAttributes({
                'class'                     : 'lightbox',
                'href'                      : CKEDITOR.plugins.imagepanel.imageUrls.lightbox + attributes.filename,
                'title'                     : attributes.alt || ''
            });
            linkElement.append(figureElement);
            figureElement = linkElement;
        }

        // Replace old element or insert new
        if(oldElement) {
            var toReplace = oldElement;
            if (toReplace.getParent().getName() === 'figure') {
                toReplace = toReplace.getParent();
            }
            if(toReplace.getParent().getName() == 'a') {
                toReplace = toReplace.getParent()
            }
            figureElement.replace(toReplace);
        } else {
            editor.insertElement(figureElement);
        }

        return imageElement;
    },

    deleteImage: function(editor, image){

        var selectedNode = image || CKEDITOR.plugins.imagepanel.getSelectedImage(editor);
        var node = selectedNode.getParent().is('a') ? selectedNode.getParent() : selectedNode;
        node.remove();

    },

    // Find and return node for selected image
    getSelectedImage : function(editor){

        // Get selected node. Traverse into p and/or a tag and find an image tag if needed (for Webkit browsers)
        var node = editor.getSelection().getSelectedElement() || editor.getSelection().getStartElement();

        // Validate that node exists, is element, is img and we can edit it
        if(node && node.type == CKEDITOR.NODE_ELEMENT && node.getName() == 'img' && CKEDITOR.plugins.imagepanel.isEditableImage(node.getAttribute('src'))){
            return node;
        }
        return null;
    },

    getImagePanelDialog: function() {
        // Initialize dialog
        var dialog = CKEDITOR.plugins.imagepanel.imagePanelDialog || new Webadmin.CKEditorImagePanelDialog();
        CKEDITOR.plugins.imagepanel.imagePanelDialog = dialog;
        return dialog;
    },

    // Get selected node if available and show image dialog
    showImageDialog: function(editor, element){
        this.open = true;
        var dialog = CKEDITOR.plugins.imagepanel.getImagePanelDialog();
        // Find selected node and configure dialog
        dialog.configure({
            editor      : editor,
            element     : element || CKEDITOR.plugins.imagepanel.getSelectedImage(editor)
        });
        // Show dialog
        dialog.show();
    },

    // Get size by url (small, medium, null ...)
    getSizeByUrl: function(url){
        for(var size in CKEDITOR.plugins.imagepanel.imageUrls){
            if(String(url).indexOf(CKEDITOR.plugins.imagepanel.imageUrls[size]) !== -1){
                return size;
            }
        }
        return null;
    },

    // Determine if url matches one that we can edit (= no external images etc.)
    isEditableImage: function(url){
        return !!CKEDITOR.plugins.imagepanel.getSizeByUrl(url);
    }
};

/**
 * because ExtJS is loaded through requireJS we need to ensure loading is complete
 * before attempting to create the Ext panel
 */
require(
    [
        "Inventis/Config",
        "Inventis/Translator",
        "Inventis/HTML/Element",
        "ExtJS",
        "Ext.ux/grid/ButtonColumn",
        "Ext.ux/buttons/cancel",
        "Ext.ux/buttons/save"
    ],
    function(Config, _t, Element){
        /**
         * Image upload/selection/configure dialog
         *
         * @author Dirk Bonhomme <dirk@inventis.be>
         * @created 2011-05-19
         */
        Ext.ns('Webadmin');
        Webadmin.CKEditorImagePanelDialog = Ext.extend(Ext.Window, {

            selectionPanel      : null,
            imageUploadForm     : null,
            imagesGridPanel     : null,
            configurationForm   : null,
            ckEditor            : null,
            ckElement           : null,

            // Component initialization
            initComponent: function(){
                // Create image upload form
                this.imageUploadForm = new Webadmin.CKEditorImagePanelImageUploadForm();
                this.imageUploadForm.on('upload', function(filename) {
                    this.addUploadedImage(filename);
                    this.showPanel('configurationForm');
                }, this);

                // Create images grid panel
                this.imagesGridPanel = new Webadmin.CKEditorImagePanelImagesGridPanel();
                this.imagesGridPanel.on('select', function(grid, record, index, eOpts){
                    this.addSelectedImage(record.get('filename'));
                    this.showPanel('configurationForm');
                }, this);

                this.imagesGridPanel.on('deletegridimage', function(rowIndex, record){
                    this.deleteGridImage(record);
                }, this);

                // Create image selection panel
                this.selectionPanel = new Ext.Panel({
                    layout          : 'border',
                    items           : [this.imageUploadForm, this.imagesGridPanel]
                });

                // Create configuration form
                this.configurationForm = new Webadmin.CKEditorImagePanelConfigurationForm();
                this.configurationForm.on('save', function(){
                    this.saveConfigurationForm();
                    this.hide();
                }, this);
                this.configurationForm.on('cancel', function(){
                    if (this.mode && this.mode === 'create') {
                        CKEDITOR.plugins.imagepanel.deleteImage(this.ckEditor, this.ckElement);
                    }
                    this.hide();
                }, this);

                // Window config
                Ext.apply(this, Ext.apply(this.initialConfig, {
                    closeAction     : 'hide',
                    title           : '-- set on show --',
                    layout          : 'card',
                    border          : false,
                    items           : [this.selectionPanel, this.configurationForm],
                    resizable       : true,
                    modal           : true,
                    width           : 550,
                    minWidth        : 550,
                    height          : 550,
                    minHeight       : 300,
                    renderTo        : Ext.getBody()
                }));

                // Call the parent initComponent
                Webadmin.CKEditorImagePanelDialog.superclass.initComponent.apply(this, arguments);

                // Load grid on window show
                this.on('beforeshow', function(){
                    if(this.ckElement){
                        this.mode = 'edit';
                        this.showPanel('configurationForm');
                    }else{
                        this.mode = 'create';
                        this.showPanel('selectionPanel');
                        this.doLayout(); // needed for f*cked up layout
                    }
                }, this);
            },
            // delete image from grid
            deleteGridImage: function(record){

                Ext.Msg.confirm(
                    _t('CKEDITOR.IMAGEUPLOADPANEL.CONFIRM', 'Bevestiging')
                    ,_t('CKEDITOR.IMAGEUPLOADPANEL.CONFIRM_DELETION', 'Bent u zeker dat u deze afbeelding wil verwijderen?')
                    ,function(btn){
                        if(btn === 'yes') {
                            var masked = new Element(this.imagesGridPanel.el.dom);
                            masked.mask();
                            // Do an ajax request to remove this record
                            Ext.Ajax.request({
                                url        : Config.adminUrl + 'images/remove',
                                success    : function(response) {
                                    this.removeSuccess(response, [], [record], masked);
                                }.bind(this),
                                params     : { image_id: record.get('id') }
                            });
                        }
                    }
                    ,this
                );
            },

            removeSuccess: function(response, options, record, masked){
                var r = Ext.decode(response.responseText);

                if (r.success == true) {
                    record[0].store.remove(record);
                }

                masked.unmask();
            },

            // Set correct title and show corresponding panel
            showPanel: function(panel){
                switch(panel){
                    case 'configurationForm':
                        this.setTitle(_t('INVENTIS.CKEDITOR.PLUGINS.IMAGEPANEL.PLUGIN.PROPERTIES_OF_IMAGE', 'Eigenschappen van afbeelding'));
                        this.loadConfigurationForm();
                        this.getLayout().setActiveItem(1);
                        break;

                    case 'selectionPanel':
                    default:
                        this.setTitle(_t('INVENTIS.CKEDITOR.PLUGINS.IMAGEPANEL.PLUGIN.INSERT_IMAGE', 'Afbeelding invoegen'));
                        this.imageUploadForm.getForm().reset(true);
                        this.imageUploadForm.getForm().baseParams = {
                            related_table   : this.ckEditor.relatedTable + '_ckeditor',
                            related_id      : this.ckEditor.getRelatedId()
                        };
                        this.imagesGridPanel.load(this.ckEditor.relatedTable + '_ckeditor', this.ckEditor.getRelatedId());
                        this.getLayout().setActiveItem(0);
                        break;
                }
            },

            // Create new image element from file upload
            addUploadedImage: function(filename){
                this.addSelectedImage(filename);
            },

            // Create new image element from grid selection
            addSelectedImage: function(filename){
                this.mode = 'create';
                var element = CKEDITOR.plugins.imagepanel.createImage(this.ckEditor, this.ckElement, {
                    filename    : filename,
                    align       : 'left',
                    size        : 'medium',
                    caption     : 0
                });
                element.scrollIntoView();
                this.ckElement = element;
            },

            // Load configuration form
            loadConfigurationForm: function(){
                if(this.ckElement){
                    var ckElement = this.ckElement,
                        lightbox = false,
                        // Filename
                        filename = ckElement.getAttribute('src').split('/').pop(),
                        // Alt attribute
                        alt = ckElement.getAttribute('alt'),
                        // Size
                        size = CKEDITOR.plugins.imagepanel.getSizeByUrl(ckElement.getAttribute('src'));

                    if (ckElement.getParent() && ckElement.getParent().getName() === 'a') {
                        ckElement = ckElement.getParent();
                        lightbox = ckElement.hasClass('lightbox') ? true : false;
                    }

                    // Select figure tag
                    ckElement = ckElement.getParent();

                    // Alignment
                    var align = 'left';
                    if (ckElement.hasClass('center')) {
                        align = 'center';
                    } else if (ckElement.hasClass('right')) {
                        align = 'right';
                    }

                    // Lightbox

                    // Load form
                    this.configurationForm.getForm().setValues({
                        filename    : filename,
                        alt         : alt,
                        align       : align,
                        size        : size,
                        lightbox    : lightbox
                    });

                    // Update preview image
                    this.configurationForm.updatePreviewPanel();
                } else {
                    this.configurationForm.getForm().reset(true);
                }
            },

            // Save parameters from configuration form to existing or new element
            saveConfigurationForm: function(){
                CKEDITOR.plugins.imagepanel.createImage(
                    this.ckEditor,
                    this.ckElement,
                    this.configurationForm.getForm().getValues()
                );
            },

            // Set CKEditor items and configure panels
            configure: function(parameters){
                this.ckEditor       = parameters.editor;
                this.ckElement      = parameters.element;
            }

        });



        /**
         * Image upload form
         *
         * @author Dirk Bonhomme <dirk@inventis.be>
         * @created 2011-05-19
         */
        Webadmin.CKEditorImagePanelImageUploadForm = Ext.extend(Ext.form.Panel, {

            // Component configs
            region      : 'north',
            height      : 95,
            border      : false,
            fileUpload  : true,

            // Component initialisation
            initComponent: function(){

                // Add custom events
                this.addEvents(
                    'upload'
                );

                // Build form items
                var items = [{
                    xtype           : 'fileuploadfield',
                    fieldLabel      : _t(
                        'INVENTIS.CKEDITOR.PLUGINS.IMAGEPANEL.PLUGIN.SELECT_HELP',
                        'Selecteer een jpg, gif of png bestand om te uploaden of kies een reeds geupload bestand in de lijst hieronder'
                    ),
                    name            : 'file',
                    hiddenName      : 'file',
                    buttonCfg       : {
                        iconCls         : 'image-icon'
                    },
                    allowBlank      : false,
                    listeners       : {
                        change      : this.submitHandler,
                        scope           : this
                    },
                    buttonOnly: true,
                    labelAlign: 'top',
                    width: '100%',
                    labelStyle: 'padding: 12px',
                    buttonText: _t('INVENTIS.CKEDITOR.PLUGINS.IMAGEPANEL.PLUGIN.BROWSE', 'Selecteer...')
                }];

                // Apply config
                Ext.apply(this, {
                    items           : items,
                    url: Config.adminUrl + 'images/uploadckeditor'
                });

                // Call the parent initComponent
                Webadmin.CKEditorImagePanelImageUploadForm.superclass.initComponent.apply(this, arguments);
            },

            // Submit form and handle result
            submitHandler: function(){

                // Mask Webadmin
                if(this.getForm().isValid()) {
                    //Webadmin.mask(_t('even geduld ...'));
                }

                // Submit form

                this.getForm().submit({
                    success: function(form, action){
                        if( action.result.success === true) {
                            this.fireEvent('upload', action.result.record.filename);
                        }
                        //Webadmin.unmask();
                    },
                    failure: function(form, action){
                        if (action.failureType == Ext.form.Action.CLIENT_INVALID) {
                            Webadmin.flash(Webadmin.FORMERRORMESSAGE, Webadmin.ERRORICON + Webadmin.FORMERRORTITLE, {duration: 3});
                        } else if (action.failureType == Ext.form.Action.CONNECT_FAILURE) {
                            Webadmin.failedRequest();
                        } else if(action.result && action.result.success === false && typeof action.result.error == 'string') {
                            Webadmin.displayError(action.result.error);
                        }
                        //Webadmin.unmask();
                    },
                    scope: this
                });
            }
        });



        /**
         * Image selection panel
         *
         * @author Dirk Bonhomme <dirk@inventis.be>
         * @created 2011-05-19
         */
        Webadmin.CKEditorImagePanelImagesGridPanel = Ext.extend(Ext.grid.Panel, {

            // Component configs
            region      : 'center',
            border      : false,

            // Component initialization
            initComponent: function(){

                // Add custom events
                this.addEvents(
                    'select',
                    'cancel',
                    'deletegridimage'
                );
                // Create data store
                var store = new Ext.data.JsonStore({
                    proxy: {
                        type: 'ajax',
                        url     : Config.adminUrl + 'images/getallckeditor',
                        reader: {
                            type: 'json',
                            root: 'results',
                            idProperty: 'id'
                        }
                    }
                });

                // Grid renderers
                var renderers = {

                    thumbnail: function(value, meta, record){
                        var thumbPath   = Config.baseUrl + 'images/wasmall/' + record.get('filename');
                        var previewPath = Config.baseUrl + 'images/wamedium/' + record.get('filename');
                        return Ext.String.format('<img src="{0}" ext:qwidth="312" ext:qtip="<img style=\'margin: 2px 0;width:300px;\' src=\'{1}\' />" width="60" class="pic" />', thumbPath, previewPath);
                    }

                };
                // Configure actions
                var actions = [
                    {
                        index : 0,
                        icon        : Config.iconUrl + 'tick.png',
                        tooltip     : _t(
                            'INVENTIS.CKEDITOR.PLUGINS.IMAGEPANEL.PLUGIN.ADD_IMAGE',
                            'Voeg deze afbeelding toe aan je tekst'
                        ),
                        handler : function(grid, rowIndex, colIndex, button, event, record, tr){
                            grid.fireEvent('select', grid, record, rowIndex);
                        },
                        scope: this
                    },
                    {
                        icon        : Config.iconUrl + 'delete.png',
                        text        : _t('INVENTIS.CKEDITOR.PLUGINS.IMAGEPANEL.DELETE', 'Verwijder'),
                        tooltip     : _t('INVENTIS.CKEDITOR.PLUGINS.IMAGEPANEL.DELETE.TOOLTIP', 'Verwijder deze afbeelding'),
                        handler : function(grid, rowIndex){
                            var record = grid.getStore().getAt(rowIndex);
                            this.fireEvent('deletegridimage', rowIndex, record);
                        },
                        scope: this
                    }
                ];

                // Configure columns
                var autoExpandColId = Ext.id();
                this.columns = [
                    {
                        header: _t('INVENTIS.CKEDITOR.PLUGINS.IMAGEPANEL.PLUGIN.IMAGE', 'Afbeelding'),
                        dataIndex: 'id',
                        width: 75,
                        renderer: renderers.thumbnail
                    },
                    {
                        header: _t('INVENTIS.CKEDITOR.PLUGINS.IMAGEPANEL.PLUGIN.FILENAME', 'Bestandsnaam'),
                        dataIndex: 'filename',
                        flex: 1,
                        id: autoExpandColId
                    },
                    {
                        items: actions,
                        width: 50,
                        sortable : false,
                        xtype: 'actioncolumn'
                    }
                ];

                // Apply grid config
                Ext.apply(this, {
                    store               : store,
                    autoScroll          : true,
                    sm                  : new Ext.selection.RowModel({singleselect: true}),
                    loadMask            : true,
                    stripeRows          : true,
                    autoExpandColumn    : autoExpandColId
                });

                // Call the parent initComponent
                Webadmin.CKEditorImagePanelImagesGridPanel.superclass.initComponent.apply(this, arguments);

                // Handle doubleclick
                this.on('rowdblclick', function(grid, rowIndex){
                    var record = grid.getStore().getAt(rowIndex);
                    grid.fireEvent('select', record.get('filename'));
                }, this);
            },

            // Load grid with related table and id
            load: function(relatedTable, relatedId){
                this.store.load({
                    params: {
                        related_table   : relatedTable,
                        related_id      : relatedId
                    }
                });
            }

        });



        /**
         * Image configuration form
         *
         * @author Dirk Bonhomme <dirk@inventis.be>
         * @created 2011-05-19
         */
        Webadmin.CKEditorImagePanelConfigurationForm = Ext.extend(Ext.form.Panel, {

            previewPanel    : null,

            // Component configs
            border          : false,
            style: 'padding: 12px;background: #ffffff;',

            // Component initialisation
            initComponent: function(){

                // Add custom events
                this.addEvents(
                    'save'
                );

                // Create preview panel
                this.previewPanel = new Ext.Panel({
                    region      : 'west',
                    border      : false,
                    width       : 200,
                    html        : 'TEST'
                });

                // Build form items
                var items = [{
                    // Fieldset: preview + filename + alt + lightbox
                    xtype       : 'fieldset',
                    title       : _t('INVENTIS.CKEDITOR.PLUGINS.IMAGEPANEL.PLUGIN.IMAGE', 'Afbeelding'),
                    items       : [{
                        xtype       : 'panel',
                        border      : false,
                        height      : 120,
                        layout      : 'border',
                        items       : [this.previewPanel, {
                            xtype       : 'panel',
                            border      : false,
                            layout      : 'form',
                            region      : 'center',
                            items       : [{
                                xtype       : 'hidden',
                                name        : 'filename'
                            },{
                                xtype       : 'textfield',
                                name        : 'alt',
                                fieldLabel  : _t('INVENTIS.CKEDITOR.PLUGINS.IMAGEPANEL.PLUGIN.TITLE', 'Titel'),
                                anchor      : '-10',
                                allowBlank  : true
                            },{
                                xtype       : 'checkbox',
                                name        : 'caption',
                                fieldLabel  : false, // _t('INVENTIS.CKEDITOR.PLUGINS.IMAGEPANEL.PLUGIN.POPUP', 'Popup')
                                boxLabel    : _t('INVENTIS.CKEDITOR.PLUGINS.IMAGEPANEL.PLUGIN.SHOW_CAPTION', 'Toon titel onder de afbeelding'),
                                inputValue  : 1
                            },{
                                xtype       : 'checkbox',
                                name        : 'lightbox',
                                fieldLabel  : false, // _t('INVENTIS.CKEDITOR.PLUGINS.IMAGEPANEL.PLUGIN.POPUP', 'Popup')
                                boxLabel    : _t('INVENTIS.CKEDITOR.PLUGINS.IMAGEPANEL.PLUGIN.SHOW_ENLARGED_IMAGE', 'Toon vergroting bij klik op afbeelding'),
                                inputValue  : 1
                            }]
                        }]
                    }]
                },{
                    // Fieldset: align
                    xtype       : 'fieldset',
                    title       : _t('INVENTIS.CKEDITOR.PLUGINS.IMAGEPANEL.PLUGIN.ALIGNMENT', 'Uitlijning'),
                    defaults    : {
                        hideLabel   : true
                    },
                    items       : [{
                        xtype       : 'radiogroup',
                        columns     : 3,
                        items       : [{
                            xtype       : 'radio',
                            name        : 'align',
                            boxLabel    : Ext.String.format('Links <br /><img src="{0}images/align_left.png" />', CKEDITOR.plugins.imagepanel.pluginPath),
                            inputValue  : 'left'
                        },{
                            xtype       : 'radio',
                            name        : 'align',
                            boxLabel    : Ext.String.format('Midden <br /><img src="{0}images/align_center.png" />', CKEDITOR.plugins.imagepanel.pluginPath),
                            inputValue  : 'center'
                        },{
                            xtype       : 'radio',
                            name        : 'align',
                            boxLabel    : Ext.String.format('Rechts <br /><img src="{0}images/align_right.png" />', CKEDITOR.plugins.imagepanel.pluginPath),
                            inputValue  : 'right'
                        }]
                    }]
                },{
                    // Fieldset: size
                    xtype       : 'fieldset',
                    title       : _t('INVENTIS.CKEDITOR.PLUGINS.IMAGEPANEL.PLUGIN.SIZE', 'Grootte'),
                    defaults    : {
                        hideLabel   : true
                    },
                    items       : [{
                        xtype       : 'radiogroup',
                        columns     : 3,
                        items       : [{
                            xtype       : 'radio',
                            name        : 'size',
                            boxLabel    : Ext.String.format('Klein <br /><img src="{0}images/size_small.png" />', CKEDITOR.plugins.imagepanel.pluginPath),
                            inputValue  : 'small'
                        },{
                            xtype       : 'radio',
                            name        : 'size',
                            boxLabel    : Ext.String.format('Medium <br /><img src="{0}images/size_medium.png" />', CKEDITOR.plugins.imagepanel.pluginPath),
                            inputValue  : 'medium'
                        },{
                            xtype       : 'radio',
                            name        : 'size',
                            boxLabel    : Ext.String.format('Groot <br /><img src="{0}images/size_banner.png" />', CKEDITOR.plugins.imagepanel.pluginPath),
                            inputValue  : 'banner'
                        }]
                    }]
                },{
                    // Cancel button
                    xtype       : 'cancelbutton',
                    name        : 'cancel',
                    anchor      : 0,
                    style       : 'float:right; margin-right: 10px;',
                    handler     : function(){
                        this.fireEvent('cancel');
                    },
                    scope       : this,
                    text        : _t('INVENTIS.CKEDITOR.PLUGINS.IMAGEPANEL.PLUGIN.CANCEL', 'Annuleren')
                },{
                    // Submit button
                    xtype       : 'savebutton',
                    name        : 'submit',
                    cls         : 'success',
                    type        : 'submit',
                    anchor      : 0,
                    style       : 'float:right;',
                    handler     : function(){
                        this.fireEvent('save');
                    },
                    scope       : this,
                    text        : _t('INVENTIS.CKEDITOR.PLUGINS.IMAGEPANEL.PLUGIN.SAVE', 'Opslaan')
                }];

                // Apply config
                Ext.apply(this, {
                    items           : items
                });

                // Call the parent initComponent
                Webadmin.CKEditorImagePanelConfigurationForm.superclass.initComponent.apply(this, arguments);
            },

            // Update preview panel
            updatePreviewPanel: function(){
                var filename = this.getForm().findField('filename').getValue();
                var thumbPath   = Config.baseUrl + 'images/ckpreviewpanel/' + filename;
                var previewPath = Config.baseUrl + 'images/wamedium/' + filename;
                var imageTag    = Ext.String.format('<img src="{0}" ext:qwidth="312" ext:qtip="<img style=\'margin: 2px 0;width:300px;\' src=\'{1}\' />" width="180" height="120" />', thumbPath, previewPath);
                this.previewPanel.update(imageTag);
            }
        });
    }
);
