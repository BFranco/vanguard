/**
 * Element file
 *
 * Javascript
 *
 * @category
 * @package    Inventis
 * @subpackage Bricks
 * @author     Inventis Web Architects <info@inventis.be>
 * @license    Copyright © Inventis BVBA  - All rights reserved
 * @link       https://github.com/Inventis/Bricks
 */
/**
 * an observable version of a HTMLElement
 */
define(
    [
        "Inventis/Mixins/Observable",
        "Inventis/Class",
        "Inventis/Mixins/Maskable"
    ],
    function (Observable, Class, Maskable) {
        var Element = Class.extend({
            use: [Observable, Maskable],
            _element:null,

            /**
             * create an observable version of a default HTMLElement
             * @param {HTMLElement} Element
             */
            __construct: function(Element) {
                this._element = Element;
            },

            getElement: function() {
                return this._element;
            },

            /**
             * attaches a listener to its own element
             *
             * @param eventName
             * @param callback
             * @param {Boolean} [capture] [default: false] listen to reverse stack (true) or normal bubble stack (false)
             */
            on: function(eventName, callback, capture){
                Observable.on.call(this, eventName, callback, true, capture);
            }
        });

        return Element;
    }
);