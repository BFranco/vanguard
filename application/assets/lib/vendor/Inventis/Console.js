/**
 *  file
 *
 * Javascript
 *
 * @category
 * @package    Inventis
 * @subpackage Bricks
 * @author     Inventis Web Architects <info@inventis.be>
 * @license    Copyright © Inventis BVBA  - All rights reserved
 * @link       https://github.com/Inventis/Bricks
 */
/**
 * Console
 * This is a dependency for places where you want to leave debugging logs
 * at all times to speed up dev debugging, like in the Observable's event handling
 */
define(
    [
    ],
    function () {
        if(window.console !== undefined && window.console.debug !== undefined) {
            return window.console;
        }
        return {
            debug: function(){
            },

            log: function(){
            },

            error: function(){
            }
        }
    }
);