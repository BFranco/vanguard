/**
 *  file
 *
 * Javascript
 *
 * @category
 * @package    Inventis
 * @subpackage Bricks
 * @author     Inventis Web Architects <info@inventis.be>
 * @license    Copyright © Inventis BVBA  - All rights reserved
 * @link       https://github.com/Inventis/Bricks
 */
require(//does not define anything, just fix broken Ext
    [
        "Inventis/Translator",
        "ExtJS"
    ],
    function (_) {
        Ext.override(Ext.selection.Model, {
            /**
             * this override fixes multiple bugs when store is a buffered type
             * and fixed a bug that causes the initial hasId to not work because
             * the store.getById() method expects an ID and not the record
             * also forcing the double verification when the record is not found because
             * it places both checks in a single if statement
             * @param record
             * @returns {boolean}
             */
            storeHasSelected: function(record) {
                var store = this.store,
                    records,
                    len, id, i, m;

                if (record.hasId()) {
                    return store.getById(record.getId());
                } else {
                    if (store.buffered) {//on buffered stores the map holds the data items
                        records = [];
                        for (m in store.data.map) {
                            records = records.concat(store.data.map[m].value);
                        }
                    } else {
                        records = store.data.items;
                    }
                    len = records.length;
                    id = record.internalId;

                    for (i = 0; i < len; ++i) {
                        if (id === records[i].internalId) {
                            return true;
                        }
                    }
                }
                return false;
            }
        });

        Ext.override(Ext.grid.plugin.DragDrop, {
            dragText : _('INVENTIS.EXT.OVERWRITES.SELECTED_ROWS', '{0} geselecteerde rij{1}')
        });

        Ext.override(Ext.dd.DragSource, {
            beforeDragOver: function(target, e, id) {
                var view = target.view,
                    overRecord = view.getRecord(view.findItemByChild(e.target)),
                    draggedRecord = view.getRecord(view.findItemByChild(e.currentTarget.activeElement)),
                    me = this,
                    result = this.view.fireEvent('beforedragover', overRecord, draggedRecord, me, target, e, id) !== false;
                if (result === false) {
                    this.proxy.setStatus(target.dropNotAllowed);
                }
                return result;
            }
        });

        Ext.override(Ext.view.DragZone, {
            getDragText: function() {
                var count = this.dragData.records.length;
                return Ext.String.format(this.dragText, count, count == 1 ? '' : 'en');
            }
        });

        /**
         * this override fixes bugs in the PageMap implementation that
         * that cause the underlying row renderer that is deeply nested in closures
         * to fail without any errors when they try to add a beforeSelect class
         * on a row that does not exist in range, getAt never throws exceptions in the default
         * implementation, so it should not so so now either
         */
        Ext.override(Ext.data.PageMap, {
            /**
             * original (4.2.1.883) method does not catch potential exceptions thrown
             * @param index
             * @returns {*}
             */
            getAt: function(index) {
                try {
                    return this.getRange(index, index)[0];
                } catch (err) {
                    return null;
                }
            },

            /**
             * add page did a floow for last page, but this must be a ceil as even where there is
             * only one record must you get at least one page...
             * @param pageNumber
             * @param records
             */
            addPage: function(pageNumber, records) {
                var me = this,
                    lastPage = pageNumber + Math.ceil((records.length - 1) / me.pageSize),
                    startIdx,
                    page;



                for (startIdx = 0; pageNumber <= lastPage; pageNumber++, startIdx += me.pageSize) {
                    page = Ext.Array.slice(records, startIdx, startIdx + me.pageSize);
                    me.add(pageNumber, page);
                    me.fireEvent('pageAdded', pageNumber, page);
                }
            }
        });

        Ext.override(Ext.window.MessageBox, {
            buttonText: {
                ok: 'OK',
                yes: _('INVENTIS.EXT.OVERWRITES.YES', 'Ja'),
                no: _('INVENTIS.EXT.OVERWRITES.NO', 'Nee'),
                cancel: _('INVENTIS.EXT.OVERWRITES.CANCEL', 'Annuleren')
            }
        });

        /**
         * because ExtJS does not properly handle constraining to an outer box when aspectRatio
         * must be maintained we do yet again its job for it...
         */
        Ext.override(Ext.resizer.ResizeTracker, {
            resize: function(box, direction, atEnd) {
                var me = this,
                    target = me.getResizeTarget(atEnd);
                if (!atEnd && this.constrainTo) {
                    var b = box,
                        a = this.constrainTo.getBox(),
                        collision = (a.x + a.width < b.x+ b.width || a.y + a.height < b.y + b.height
                            || a.x > b.x || a.y > b.y);
                    if (collision) return false;
                }
                target.setBox(box);
                this.lastBox = box;

                if (me.originalTarget && (me.dynamic || atEnd)) {
                    me.originalTarget.setBox(box);
                }
            },
            onEnd: function(e) {
                this.resize(this.lastBox, null, true);
                if (this.proxy) {
                    this.proxy.hide();
                }
            }
        });
    }
);