
define(
    [
        'Inventis/Config',
        'Inventis/Application/Webadmin',
        'ExtJS'
    ],
    function (Config, WA) {

        Ext.ns('Ext.ux.Cropper');

        Ext.ux.Cropper.Panel = Ext.extend(Ext.Panel, {
            cropDimensions: false,
            initComponent: function(){
                this.addEvents('imageloaded');
                var config = {
                    bodyStyle: {
                        padding: '15px 10px 10px 10px'
                    }
                };
                Ext.apply(this, Ext.apply(this.initialConfig, config));
                // Call the parent initComponent
                Ext.ux.Cropper.Panel.superclass.initComponent.apply(this, arguments);
            }

            ,onRender: function(){
                Ext.ux.Cropper.Panel.superclass.onRender.apply(this, arguments);
                this.img = Ext.DomHelper.append(this.body, {
                    tag: 'img',
                    src: Config.siteUrl + 'images/cropper/' + this.image
                }, true);
                this.body.mask(_('afbeelding laden'));
                this.img.on('load', function(event, target){
                    var img = Ext.get(target);
                    this.fireEvent('imageloaded', img);

                    var w = img.getWidth();
                    var h = img.getHeight();

                    img.remove();
                    this.imageCrop = new Ext.ux.ImageCrop({
                        draggable       : true,
                        width           : w,
                        height          : h,
                        cropId          : this.cropId,
                        cropDimensions  : this.cropDimensions,
                        image           : this.image
                    });
                    this.add(this.imageCrop);
                    this.body.unmask();
                }, this);
            }

            ,preserveRatio: function(preserve){
                this.imageCrop.preserveRatio = preserve;
            }

            ,getImageSize: function(){
                return {
                    width  : this.imageCrop.width,
                    height : this.imageCrop.height
                };
            }

        });

        Ext.ux.Cropper.Window = Ext.extend(Ext.Window, {
            cropperPanel    : false
            ,cropperSelector : false
            ,cropperSizes    : []
            ,minWidth: 280
            ,widthOffset     : 32
            ,heightOffset    : 94 + 27

            ,initComponent: function() {
                var cropperSize,
                    formats = Config.moduleConfig.cropperFormats;
                // Determine what cropper sizes we need
                this.cropperSizes = [];
                for (cropperSize in formats) {
                    if (formats.hasOwnProperty(cropperSize)) {
                        var props = formats[cropperSize];
                        this.cropperSizes.push([
                            cropperSize,
                            props.name,
                            props.info,
                            props.width,
                            props.height
                        ]);
                    }
                }
                // Combobox template
                var tpl = new Ext.XTemplate(
                    '<tpl for="."><div class="x-boundlist-item crop-item">',
                    '<b>{name:capitalize}</b>',
                    '<span class="help-text">{info:nl2br}</span>',
                    '</div></tpl>'
                );

                // Create the combobox with cropper sizes
                this.cropperSelector = new Ext.form.ComboBox({
                    store           : new Ext.data.ArrayStore({
                        fields  : ['id', 'name', 'info', 'width','height'],
                        data    : this.cropperSizes,
                        idIndex : 0
                    }),
                    tpl             : tpl,
                    itemSelector    : 'div.crop-item',
                    displayField    : 'name',
                    valueField      : 'id',
                    mode            : 'local',
                    forceSelection  : true,
                    editable        : false,
                    triggerAction   : 'all'
                });

                // Get the first item from the combobox
                var firstCropsize = this.cropperSelector.store.getAt(0);

                // And select it
                this.cropperSelector.setValue(firstCropsize.get('id'));
                this.cropperSelector.on('select', function(combo, selections){
                    var record = selections.shift();
                    //if no width or height is defined we can remove preserveRatio
                    this.cropperPanel.preserveRatio(Boolean(record.get('width') && record.get('height')));
                    this.cropperPanel.imageCrop.loadCropData(record.get('id'), record.get('width'), record.get('height'));
                }, this);

                // Create the cropper panel
                this.cropperPanel = new Ext.ux.Cropper.Panel({
                    plain           : true,
                    border          : false,
                    image           : this.initialConfig.image,
                    cropId          : firstCropsize.get('id'),
                    cropDimensions  : {
                        width  : firstCropsize.get('width'),
                        height : firstCropsize.get('height')
                    }
                });

                // Set the event to resize the window when the image has loaded
                this.cropperPanel.on('imageloaded', function(img){
                    var newWidth  = img.getWidth()  + this.widthOffset;
                    var newHeight = img.getHeight() + this.heightOffset;
                    this.setWidth(newWidth).setHeight(newHeight);
                    this.formatToolbar();
                    this.center();
                }, this);

                var config = {
                    layout         : 'fit'
                    ,title          : _('Afbeelding verkleiningen bijsnijden')
                    ,border         : false
                    ,draggable      : true
                    ,items          : [ this.cropperPanel ]
                    ,width          : 400
                    ,height         : 400
                    ,modal          : true
                    ,resizable      : false
                    ,tbar           : [ this.cropperSelector ]
                    ,bbar           : [{
                        text    : _('Reset')
                        ,icon    : Config.iconUrl + 'refresh.png'
                        ,handler : this.reset
                        ,scope   : this
                    },'->',{
                        text    : _('Opslaan')
                        ,icon    : Config.iconUrl + 'add.png'
                        ,handler : this.save
                        ,scope   : this
                    },{
                        text    : _('Sluiten')
                        ,icon    : Config.iconUrl + 'stop.png'
                        ,scope   : this
                        ,handler : function(){
                            this.hide();
                        }
                    }]
                };
                config = Ext.apply(this.initialConfig, config);
                Ext.apply(this, config);

                // Call the parent initComponent
                Ext.ux.Cropper.Window.superclass.initComponent.apply(this, config);
            }

            ,formatToolbar: function(){
                this.cropperSelector.setWidth(this.getWidth() - 14);
            }

            ,reset: function(){
                this.cropperPanel.imageCrop.fitCropperSize(this.cropperPanel.cropDimensions.width, this.cropperPanel.cropDimensions.height, 0, 0);
                this.save();
            }

            ,save: function(){

                var imageSize = this.cropperPanel.getImageSize();
                var cropData  = this.cropperPanel.imageCrop.getCropData();
                var cropId    = this.cropperPanel.imageCrop.getCropId();

                var params = {
                    image       : this.image,
                    image_data  : Ext.encode(imageSize),
                    crop_data   : Ext.encode(cropData),
                    crop_id     : cropId
                };

                this.body.mask();

                Ext.Ajax.request({
                    url : Config.adminUrl + 'images/savecrop',
                    params: params,
                    success: function(r){
                        var response = Ext.decode(r.responseText);
                        if (response.success == true) {
                            this.body.unmask();
                            WA.alert(_('Succes'), _('Afbeelding succesvol bijgesneden.'));
                            //Webadmin.flash(Webadmin.SUCCESSICON + _('Afbeelding succesvol bijgesneden.'));
                        } else {
                            Ext.Msg.alert(_('Fout'), _('Het bewaren van de gegevens is mislukt, probeer opnieuw.'));
                        }
                    },
                    scope: this
                });

            }

        });

        Ext.ux.ImageCrop = Ext.extend(Ext.Component, {

            preserveRatio   : true,
            minWidth        : 10,
            minHeight       : 10,
            cropId          : null,

            initComponent: function() {
                Ext.ux.ImageCrop.superclass.initComponent.apply(this, arguments);
            },

            updateCropImagePosition: function(x, y){
                this.cropperEl.getEl().setStyle({
                    'background-position':(-x)+'px '+(-y)+'px'
                });
            },

            onRender: function(){
                Ext.ux.ImageCrop.superclass.onRender.apply(this, arguments);

                // Create url of the image
                this.imageUrl = Config.siteUrl + 'images/cropper/' + this.image;

                // Create the image div that serves as background
                this.imageEl = this.getEl().insertFirst({
                    tag: 'div'
                }).setStyle({
                    'position'         : 'relative',
                    'background-image' : 'url(' + this.imageUrl + ')',
                    'background-repeat': 'no-repeat',
                    'z-index'          : 9997,
                    'left'             : 0,
                    'right'            : 0
                }).setSize(this.width, this.height);

                this.imageEl.insertFirst({
                    tag: 'div'
                }).setStyle({
                    opacity: 0.8,
                    position: 'absolute',
                    'background-color': '#FFFFFF',
                    'z-index': 9998

                }).setSize(this.width, this.height);

                // The element to serve as cropper
                this.cropperEl = Ext.create('Ext.Img',{
                    opacity: 1.0,
                    renderTo: this.getEl(),
                    resizable: {
                        pinned: true,
                        preserveRatio: this.preserveRatio,
                        handles: 'all'
                    },
                    draggable: {
                        constrain: true,
                        constrainTo: this.imageEl,
                        listeners: {
                            dragstart: function() {

                            },
                            dragend: function() {
                                var res = this.getCropData();
                                this.updateCropImagePosition(res.x, res.y);
                                this.fireEvent('changeCrop', this, res);
                                this.fireEvent('moveCrop', this, res);
                            },
                            drag: function(){
                                var res = this.getCropData();
                                this.updateCropImagePosition(res.x, res.y);
                            },
                            scope: this
                        }
                    },
                    constrainTo: this.imageEl,
                    src: Ext.BLANK_IMAGE_URL,
                    minWidth         : this.minWidth,
                    minHeight        : this.minHeight,
                    width         : this.width,
                    height        : this.height,
                    left: 0,
                    top: 0,
                    style:{
                        cursor: 'move',
                        position: 'absolute',
                        'z-index': 9999,
                        'background-image': 'url('+this.imageUrl+')',
                        'background-repeat': 'no-repeat',
                        'background-position':'0px 0px'
                    },
                    listeners: {
                        resize: function() {
                            var res = this.getCropData();
                            this.updateCropImagePosition(res.x, res.y);
                            this.fireEvent('changeCrop', this, res);
                            this.fireEvent('resizeCrop', this, res);
                        },
                        scope: this
                    }
                });
                this.cropperEl.setX(this.imageEl.getX());
                this.cropperEl.setY(this.imageEl.getY());

                // Load already saved cropdata
                this.loadCropData(this.cropId, this.cropDimensions.width, this.cropDimensions.height);
            },

            getCropData: function(){

                var imagePos = this.imageEl.getXY(),
                    cropperPos = this.cropperEl.getXY(),
                    offsetX = cropperPos[0] - imagePos[0],
                    offsetY = cropperPos[1] - imagePos[1],
                    width = this.cropperEl.getWidth(),
                    height = this.cropperEl.getHeight();
                return {
                    x: offsetX,
                    y: offsetY,
                    width: width,
                    height: height
                }

            },

            fitCropperSize: function(width, height){

                // Calculate the fitting size
                var fitSizes = this.getFitSize({
                    width  : this.width,
                    height : this.height
                }, {
                    width  : width,
                    height : height
                });

                // Center position
                var centerX = Math.round((this.width / 2) - ( fitSizes.width / 2))

                // Set fit dimensions
                this.setCropperSize(fitSizes.width, fitSizes.height, centerX, 0);
            },

            setCropperSize: function(w, h, x, y){
                x = parseInt(x), y = parseInt(y), w = parseInt(w), h = parseInt(h);
                // Calculate positions based on image offset
                var posX = this.imageEl.getX() + x,
                    posY = this.imageEl.getY() + y;

                // Set the sizes and position
                this.cropperEl.resizer.resizeTracker.preserveRatio = this.preserveRatio;

                this.cropperEl.resizerComponent.updateBox({x: posX, y: posY, width: w, height: h});
                this.cropperEl.updateBox({x: posX, y: posY, width: w, height: h});

                this.cropperEl.fireEvent('resize');
            },

            getFitSize:function(outerSizes, innerSizes){

                // Target dimensions
                var max_width  = outerSizes.width;
                var max_height = outerSizes.height;

                // Get current dimensions
                var old_width  = innerSizes.width;
                var old_height = innerSizes.height;

                // Calculate the scaling we need to do to fit the image inside our frame
                var scale      = Math.min(max_width/old_width, max_height/old_height);

                // Get the new dimensions
                var new_width  = Math.ceil(scale * old_width);
                var new_height = Math.ceil(scale * old_height);

                return {
                    width  : new_width || max_width,
                    height : new_height || max_height
                };

            },

            loadCropData: function(cropId, width, height){

                this.setCropId(cropId);

                this.el.mask(_('Cropdata laden')+' ...');
                this.cropperEl.hide();

                Ext.Ajax.request({
                    url     : Config.adminUrl + 'images/loadcrop',
                    params  :  {
                        id      : cropId,
                        image   : this.image
                    },
                    success: function(r){

                        this.el.unmask();
                        this.cropperEl.show();
                        var response = Ext.decode(r.responseText);
                        if (response.data !== false && response.data.o_w && response.data.o_h) {
                            this.setCropperSize(response.data.o_w, response.data.o_h, response.data.o_x, response.data.o_y);
                        } else {
                            this.fitCropperSize(width, height)
                        }

                    },
                    scope: this
                });

            },

            setCropId: function(id){
                this.cropId = id;
            },

            getCropId: function(){
                return this.cropId;
            }

        });
    }
);
