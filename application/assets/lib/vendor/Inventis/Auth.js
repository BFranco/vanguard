/**
 * main.js is the require.js main starting point file
 * it should be loaded as a default when require.js is initialized in the view
 */

define(
    [
        "Sizzle"
    ],
    function($){
        var forgot_password_hidden = true;
        function hideForgotPassword() {
            $('.forgot-password-form')[0].style.display = 'none';
            $('.inner-box .close')[0].style.display = 'none';
            forgot_password_hidden = true;
        }

        try {
            $('.forgot-password-link')[0].addEventListener('click', function(e) {
                e.preventDefault(); // Prevent redirect to #
                if (forgot_password_hidden) {
                    $('.forgot-password-form')[0].style.display = 'block';
                    $('.inner-box .close')[0].style.display = 'inline';
                    forgot_password_hidden = false;
                } else {
                    hideForgotPassword();
                }
            });
            $('.inner-box .close')[0].onclick = hideForgotPassword;
        } catch (e) {

        }
    }
);