/* global require, requireModule */
require(
    [
        'jquery',
        'Inventis/Application/Front/Lightbox',
        'Inventis/Application/Front/FlashMessages',
        'Inventis/Application/Front/TextareaAutoGrow',
        requireModule
    ],
    function ($) {
        'use strict';

        // Enable javascript validation
        //$('form').html5validator();

    }
);