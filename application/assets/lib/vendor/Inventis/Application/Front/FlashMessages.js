/* global define */
define(['jquery'], function ($) {
    'use strict';

    // Closing flash message
    $('.flash-container .close').show().on('click', function(e) {
        e.preventDefault();
        $(this).parents('.flash-container').addClass('hide').delay(800).queue(function() {
            $(this).remove();
        });
    });

});