/* global require, define */
define(['jquery'], function ($) {
    'use strict';

    // This require forces magnificPopup to be loaded after $ is available
    require(['magnificPopup'], function() {
        // Lightbox
        if ($('a.lightbox').length) {
            $('a.lightbox').magnificPopup({type:'image'});
        }

        // Lightbox gallery
        $('.lightbox-gallery').each(function() { // the containers for all your galleries should have the class gallery
            $(this).magnificPopup({
                delegate: 'a', // the container for each your gallery items
                type: 'image',
                gallery:{enabled:true}
            });
        });
    });

});