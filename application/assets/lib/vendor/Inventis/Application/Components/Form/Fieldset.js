/**
 *  file
 *
 * Javascript
 *
 * @category
 * @package    Inventis
 * @subpackage Bricks
 * @author     Inventis Web Architects <info@inventis.be>
 * @license    Copyright © Inventis BVBA  - All rights reserved
 * @link       https://github.com/Inventis/Bricks
 */
define(
    [
        "Inventis/Application/Components/Component",
        "Inventis/Button",
        "Sizzle"
    ],
    function (Component, Button, $) {
        var Fieldset = Component.extend({
            _collapsible: null,
            _collapseButtonSelector: 'legend',
            _collapsed: null,

            setupFromConfig: function(setup){
                this.__super(setup);
                this.setCollapsibleFromSetup(setup);
            },

            setCollapsibleFromSetup: function(setup){
                if (setup.collapsible !== undefined) {
                    this.setCollapsible(setup.collapsible);
                }
            },

            setCollapsed: function(collapsed){
                this._collapsed = Boolean(collapsed);
                return this;
            },

            getCollapsed: function(){
                return this._collapsed;
            },

            setCollapsible: function(collapsible){
                this._collapsible = Boolean(collapsible);
                return this;
            },

            getCollapsible: function(){
                return this._collapsible;
            },

            onDomReady: function(){
                this.__super();
                if (this.getCollapsible()) {
                    this._attachCollapseHandling();
                }
            },

            /**
             * attaches a collapse button and the click listener event to the fieldset
             * @private
             */
            _attachCollapseHandling: function(){
                var element = this.getElement(),
                    elementClasses = element.className.split(" "),
                    index;

                //create a button (no ref to the button required)
                new Button($(this._collapseButtonSelector, this.getElement()).shift(), {for: this.getId()});

                this.on('buttonClicked', function(e, options){
                    if (options.for === this.getId()) {
                        index = elementClasses.indexOf('collapsed');
                        if (index !== -1) {
                            this.setCollapsed(false);
                            elementClasses.splice(index, 1);
                        } else {
                            this.setCollapsed(true);
                            elementClasses.push('collapsed');
                        }
                        element.className = elementClasses.join(" ");
                    }
                }, true);
            }
        });
        return Fieldset;
    }
);