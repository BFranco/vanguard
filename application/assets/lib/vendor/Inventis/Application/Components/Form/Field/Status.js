/**
 *  file
 *
 * Javascript
 *
 * @category
 * @package    Inventis
 * @subpackage Bricks
 * @author     Inventis Web Architects <info@inventis.be>
 * @license    Copyright © Inventis BVBA  - All rights reserved
 * @link       https://github.com/Inventis/Bricks
 */
define(
    [
        "Inventis/Application/Components/Component"
    ],
    function (Component) {
        var Status = Component.extend({
            _datePanelId: null,
            _statusSelectId: null,

            _datePanel: null,
            _statusSelect: null,

            /**
             * overwrite and attach custom setup handlers
             * @param setup
             */
            setupFromConfig: function(setup){
                this.__super(setup);
                this.setDependenciesFromSetup(setup);
                this.on('componentInitialized', this.onComponentInitialized);
                this.on('change', this.onSelectChange, true);
            },

            onSelectChange: function(event, options) {
                if (options.id === this._statusSelectId) {
                    var newValue = options.getTarget().value;
                    if (newValue === "pending") {
                        this._datePanel.fire('show', {}, false);
                    } else {
                        this._datePanel.fire('hide', {}, false);
                    }
                }
            },

            setDependenciesFromSetup: function(setup) {
                if (setup.dependencies) {
                    this.setDatePanelFromSetup(setup);
                    this.setStatusSelectFromSetup(setup);
                }
            },

            setDatePanelFromSetup: function(setup) {
                if (setup.dependencies && setup.dependencies.datePanel) {
                    this._datePanelId = setup.dependencies.datePanel;
                }
            },

            setStatusSelectFromSetup: function(setup) {
                if (setup.dependencies && setup.dependencies.statusSelect) {
                    this._statusSelectId = setup.dependencies.statusSelect;
                }
            },

            onComponentInitialized: function(event, options) {
                if (options.id === this._statusSelectId) {
                    this._statusSelect = options.element;
                } else if (options.id === this._datePanelId) {
                    this._datePanel = options.element;
                }
            }
        });
        return Status;
    }
);