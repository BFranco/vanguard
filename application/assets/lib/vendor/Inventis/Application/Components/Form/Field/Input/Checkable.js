/**
 *  file
 *
 * Javascript
 *
 * @category
 * @package    Inventis
 * @subpackage Bricks
 * @author     Inventis Web Architects <info@inventis.be>
 * @license    Copyright © Inventis BVBA  - All rights reserved
 * @link       https://github.com/Inventis/Bricks
 */
define(
    [
        "Inventis/Application/Components/Form/Field/Input"
    ],
    function (Input) {
        var Checkable =  Input.extend({

            onDomReady: function() {
                var element = this.getElement(),
                    disabled = element.disabled;
                this.__super();
                this.on('change', function(e, options) {
                    if (options.component === undefined) {
                        options.checked = this.getElement().checked;
                        options.fieldName = this.getName();
                        options.value = this.getElement().checked;
                        options.component = true;
                        this.fire('change', options);
                    }
                }, true, true);
                this.on('check', this.onCheck, true);
                this.on('uncheck', this.onUnCheck, true);
                // IE fix, can't get value of disabled element
                element.disabled = false;
                this.fire('change', {fieldName: this.getName(), value: this.checked});
                element.disabled = disabled;
            },

            onCheck: function(event, options) {
                this.setValue(this.getElement().value);
            },

            onUnCheck: function(event, options) {
                this.setValue(false);
            },

            /**
             * sets the value of the element
             * @param {*} value
             * @returns {*}
             */
            setValue: function(value){
                var element = this.getElement(),
                    disabled = element.disabled;
                // IE fix, can't get value of disabled element
                element.disabled = false;
                element.checked = value == element.value;
                this.fire('change', {fieldName: this.getName(), value: this.checked});
                element.disabled = disabled;
                return this;
            },

            clear: function(defaults) {
                var value = defaults[this.getName()] !== undefined ?
                    defaults[this.getName()] :
                    false;
                this.setValue(value);
            },

            getValue: function(){
                var disabled = this.getElement().disabled,
                    value;
                this.getElement().disabled = false;
                value = this.getElement().checked ? this.__super() : null;
                this.getElement().disabled = disabled;
                return value;
            }

        });
        return Checkable;
    }
);