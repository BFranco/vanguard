/**
 *  file
 *
 * Javascript
 *
 * @category
 * @package    Inventis
 * @subpackage Bricks
 * @author     Inventis Web Architects <info@inventis.be>
 * @license    Copyright © Inventis BVBA  - All rights reserved
 * @link       https://github.com/Inventis/Bricks
 */
define(
    [
        "Inventis/Application/Components/Form/Field",
        "Inventis/Application/Components/Component/ApiAwareMixin",
        "Sizzle"
    ],
    function (Field, ApiAware, $) {
        var Select = Field.extend({
            use: [ApiAware],

            _items: null,
            _multiple: false,
            _valueField: null,
            _displayField: null,

            /**
             * overwrite and attach custom setup handlers
             * @param setup
             */
            setupFromConfig: function(setup){
                this.__super(setup);
                this.setValueFieldFromSetup(setup);
                this.setDisplayFieldFromSetup(setup);
                this.setMultipleFromSetup(setup);
                this.setApiFromConfig(setup);
                this.setItemsFromSetup(setup);

                if (this.getItems() === null) {
                    this.fetch();
                }
                this.on('change', this.onChange, true);
            },

            onChange: function(event, options) {
                if (options.id === undefined) {
                    this.fire('change', {fieldName: this.getName(), value: this.getValue(), id: this.getId()}, true);
                    return false;
                }
            },

            onDomReady: function(){
                this.__super();
                this.on('reload', this.onReload, true);
            },

            onReload: function(e, options){
                this.fetch();
            },

            /**
             * initiates an AJAX call to fetch the items for the select and reloads it
             */
            fetch: function(){
                this.apiRequest('getItems', {data:null}, function(response){
                    if (response.success) {
                        this.setItems(response.result);
                        this.reloadItems();
                    } else {
                        throw new Error('Loading the items for the select through AJAX failed!');
                    }
                });
            },

            /**
             * forces the select to reload all its items from the items array
             * also selects the correct value(s) if set
             */
            reloadItems: function(){
                var value = this.getValue(),
                    items = this.getItems(),
                    options = this.getElement().options;

                options.length = 0;

                if (typeof value !== 'object') {
                    value = new Array(value);
                }

                for(var i = 0; i < items.length; ++i) {
                    options[i] = new Option(
                        items[i][this.getDisplayField()],
                        items[i][this.getValueField()],
                        false,
                        value.indexOf(items[i][this.getValueField()]) !== -1
                    );
                }
            },

            /**
             * @param setup
             */
            setValueFieldFromSetup: function(setup){
                if (setup.valueField !== undefined) {
                    this.setValueField(setup.valueField);
                }
            },

            /**
             * @param setup
             */
            setDisplayFieldFromSetup: function(setup){
                if (setup.displayField !== undefined) {
                    this.setDisplayField(setup.displayField);
                }
            },

            /**
             * @param setup
             */
            setItemsFromSetup: function(setup){
                if (setup.items !== undefined) {
                    this.setItems(setup.items);
                }
            },

            /**
             * @param setup
             */
            setMultipleFromSetup: function(setup){
                if (setup.multiple !== undefined) {
                    this.setMultiple(setup.multiple);
                }
            },

            clear: function(defaults) {
                var items = this.getItems(),
                    value = defaults && defaults[this.getName()] !== undefined ?
                        defaults[this.getName()] :
                        items[0][this.getValueField()];
                this.setValue(value);
            },

            /**
             * the field assigned the value to the html element, not validating it
             * that is the responsibility of extending field components
             * @param value
             * @return this
             */
            setValue: function(value){
                if (typeof value === 'object' && !this.getMultiple()) {
                    throw 'Array specified for select element ' + this.getId() +
                        ', which has only one selection.';
                }

                // check if the user wants to set multiple selections
                if(typeof value === 'object') {
                    var options = this.getElement().options;

                    for(var i = 0; i < options.length; ++i) {
                        options[i].selected = (value.indexOf(options[i].value) !== -1);
                    }

                    this._value = value;
                    return this;
                }

                return this.__super(value);
            },

            /**
             * returns the value of the current HTML element
             */
            getValue: function(){
                var value = null;

                if(this.getMultiple()) {
                    value = new Array();

                    var options = this.getElement().options;

                    for(var i = 0; i < options.length; ++i) {
                        if(options[i].selected) {
                            value.push(options[i].value); 
                        }
                    }

                    return value;
                }

                return this.__super();
            },

            /**
             *
             * @param valueField
             * @returns this
             */
            setValueField: function(valueField){
                this._valueField = valueField;
                return this;
            },

            /**
             *
             * @returns {string}
             */
            getValueField: function(){
                return this._valueField;
            },

            /**
             *
             * @param displayField
             * @returns this
             */
            setDisplayField: function(displayField){
                this._displayField = displayField;
                return this;
            },

            /**
             *
             * @returns {string}
             */
            getDisplayField: function(){
                return this._displayField;
            },

            /**
             *
             * @param items
             * @returns this
             */
            setItems: function(items){
                this._items = items;
                return this;
            },

            /**
             *
             * @returns {array}
             */
            getItems: function(){
                return this._items;
            },

            /**
             *
             * @param multiple
             * @returns this
             */
            setMultiple: function(multiple){
                this._multiple = multiple;
                return this;
            },

            /**
             *
             * @returns {array}
             */
            getMultiple: function(){
                return this._multiple;
            }
        });
        return Select;
    }
);