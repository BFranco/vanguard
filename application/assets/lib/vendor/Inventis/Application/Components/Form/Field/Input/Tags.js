/**
 *  file
 *
 * Javascript
 *
 * @category
 * @package    Inventis
 * @subpackage Bricks
 * @author     Inventis Web Architects <info@inventis.be>
 * @license    Copyright © Inventis BVBA  - All rights reserved
 * @link       https://github.com/Inventis/Bricks
 */
define(
    [
        "Inventis/Application/Components/Form/Field/Input",
        "Inventis/Button",
        "Sizzle"
    ],
    function (Input, Button, $) {
        var Tags = Input.extend({
            _separator: null,

            _mostUsed: [],

            _mostUsedATags: [],

            /**
             * overwrite and attach custom setup handlers
             * @param setup
             */
            setupFromConfig: function(setup){
                this.__super(setup);
                this.setSeparatorFromSetup(setup);
                this.setTagsFromSetup(setup);
            },


            setTagsFromSetup: function(setup) {
                if (setup.mostUsed !== undefined) {
                    this.setMostUsed(setup.mostUsed);
                }
            },

            setMostUsed: function(mostUsed) {
                var x,
                    aTag,
                    button;
                this._mostUsed = mostUsed;
                this._mostUsedATags = $('#' + this.getId() + ' ~.tags > a');
                for(x in this._mostUsedATags) {
                    if (this._mostUsedATags.hasOwnProperty(x)) {
                        aTag = this._mostUsedATags[x];
                        button = new Button(aTag, {text: aTag.innerHTML});
                        button.on('buttonClicked', this.onTagClicked.bind(this), true);
                    }
                }
            },

            onTagClicked: function(event, options) {
                var text = options.text,
                    element = this.getElement(),
                    value = element.value;
                element.value +=
                    value === '' || value[value.length - 1] === ',' ?
                        '' :
                        this.getSeparator() + ' ';
                element.value += text;
            },

            /**
             * @param setup
             */
            setSeparatorFromSetup: function(setup){
                if (setup.separator !== undefined) {
                    this.setSeparator(setup.separator);
                }
            },

            /**
             *
             * @param separator
             * @returns this
             */
            setSeparator: function(separator){
                this._separator = separator;
                return this;
            },

            /**
             *
             * @returns {String}
             */
            getSeparator: function(){
                return this._separator;
            },

            onButtonClicked: function(event, options) {
            }
        });
        return Tags;
    }
);