/**
 * Created with JetBrains PhpStorm.
 * User: janesser
 * Date: 16/05/13
 * Time: 15:58
 * To change this template use File | Settings | File Templates.
 */
define(
    [
        "Inventis/Application/Components/Component",
        "Inventis/HTML/Element"
    ],
    function(Component, Element) {
        var SelectManager = Component.extend({

            _managerComponentId: null,

            _managerComponent: null,

            _entityComponentId: null,

            _entityComponent: null,

            setupFromConfig: function(setup){
                this.__super(setup);
                this.dependenciesFromConfig(setup);
                this.on("componentInitialized", this.onComponentInitialized, true);
                this.getManagerComponent().on('recordSaved', this.onRecordSaved.bind(this), true);
            },

            dependenciesFromConfig: function(setup) {
                if (setup.dependencies === undefined) {
                    return;
                }
                this.managerComponentIdFromDependencies(setup.dependencies);
                this.entityComponentIdFromDependencies(setup.dependencies);
            },

            managerComponentIdFromDependencies: function(dependencies) {
                if (dependencies.managerComponent === undefined) {
                    return;
                }
                this._managerComponentId = dependencies.managerComponent;
            },

            entityComponentIdFromDependencies: function(dependencies) {
                if (dependencies.entityComponent === undefined) {
                    return;
                }
                this._entityComponentId = dependencies.entityComponent;
            },

            getEntityComponent: function() {
                if (!this._entityComponent) {
                    this._entityComponent = new Element(document.getElementById(this._entityComponentId));
                }
                return this._entityComponent;
            },

            getManagerComponent: function() {
                if (!this._managerComponent) {
                    this._managerComponent = new Element(document.getElementById(this._managerComponentId));
                }
                return this._managerComponent;
            },

            onRecordSaved: function(event, options) {
                this.getEntityComponent().fire('reload', {}, false);
            },

            onComponentInitialized: function(event, options) {
                switch (options.id) {
                    case this._managerComponentId:
                        this._managerComponent = options.element;
                        break;
                    case this._entityComponentId:
                        this._entityComponent = options.element;
                        break;
                }
            }
        });
        return SelectManager;
    }
);