/**
 *  file
 *
 * Javascript
 *
 * @category
 * @package    Inventis
 * @subpackage Bricks
 * @author     Inventis Web Architects <info@inventis.be>
 * @license    Copyright © Inventis BVBA  - All rights reserved
 * @link       https://github.com/Inventis/Bricks
 */

/**
 * Text component uses an adapter mixin that will allow
 * quick swap with another one would you like to do so
 */
define(
    [
        "Inventis/Application/Components/Form/Field",
        "Inventis/Mixins/Adapters/Renderers/CKEditor"
    ],
    function (Field, CKEditorRenderer) {
        var Text = Field.extend({
            use: [CKEditorRenderer],

            _richText:null,
            _relatedTable:null,

            /**
             *
             * @param setup
             */
            setupFromConfig: function(setup){
                this.__super(setup);
                this.setRichTextFromSetup(setup);
                this.setRichTextEditorConfigFromSetup(setup);
                this.setRelatedTableFromConfig(setup);
            },

            /**
             *
             * @param setup
             */
            setRelatedTableFromConfig: function(setup){
                if (setup.relatedTable !== undefined) {
                    this.setRelatedTable(setup.relatedTable);
                }
            },

            /**
             *
             * @param table
             * @returns Text
             */
            setRelatedTable: function(table){
                this._relatedTable = table;
                return this;
            },

            /**
             *
             * @returns {String}
             */
            getRelatedTable: function(){
                return this._relatedTable;
            },

            /**
             *
             * @param setup
             */
            setRichTextFromSetup: function(setup){
                if (setup.richText !== undefined) {
                    this.setRichText(setup.richText);
                }
            },

            /**
             *
             * @param setup
             */
            setRichTextEditorConfigFromSetup: function(setup){
                if (setup.config) {
                    this.setEditorConfig(setup.config);
                }
            },

            /**
             *
             * @param state
             * @returns {*}
             */
            setRichText: function(state){
                this._richText = Boolean(state);
                return this;
            },

            /**
             *
             * @returns {Boolean}
             */
            getRichText: function() {
                return this._richText;
            },

            /**
             *
             */
            render: function(){
                this.__super();
                if (this.getRichText()) {
                    this.on('richTextEditorReady', this.onRichTextEditorReady, true);
                    this.renderEditor();
                }
            },

            /**
             * called when the rich text editor is ready for user manipulation
             * @param e
             * @param options
             */
            onRichTextEditorReady: function(e, options){
                var instance = this.getRichTextInstance();
                instance.relatedTable = this.getRelatedTable();
                instance.on(
                    'change',
                    function() {
                        this.getFormElement().fire('changed', {}, false);
                    }.bind(this)
                );
                /*
                 * create a function to return the ID as it will change constantly,
                 * while the editor instance will not be reloaded in between
                 * this code does not go into the adapter as its framework related
                 */
                var me = this;
                instance.getRelatedId = function() {
                    var options = {};
                    me.fire('getLoadedRecord', options);
                    //check if we have valid data
                    if (!options.record) {
                        throw new Error(
                            'Failed to retrieve loaded record through the getLoadedRecord event, '+
                            'ensure this component is part of a form that can handle this type of request'
                        );
                    }
                    return options.record.id;
                };
            },

            /**
             * when we are dealing with a rich text instance we need to update its value rather
             * then updating our default element
             * @param value
             */
            setValue: function(value){
                if (this.getRichText()) {
                    this.setEditorValue(value);
                } else {
                    this.__super(value);
                }
                return this;
            },

            /**
             *
             * @returns {String}
             */
            getValue: function(){
                if (this.getRichText()) {
                    return this.getEditorValue();
                } else {
                    return this.__super();
                }
            }
        });
        return Text;
    }
);