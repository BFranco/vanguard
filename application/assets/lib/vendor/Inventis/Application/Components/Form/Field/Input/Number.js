/**
 *  file
 *
 * Javascript
 *
 * @category
 * @package    Inventis
 * @subpackage Bricks
 * @author     Inventis Web Architects <info@inventis.be>
 * @license    Copyright © Inventis BVBA  - All rights reserved
 * @link       https://github.com/Inventis/Bricks
 */
define(
    [
        "Inventis/Application/Components/Form/Field/Input"
    ],
    function (Input) {
        var Number = Input.extend({
            getValue: function(){
                var value = this.__super();
                value = value.replace(',', '.');
                return value;
            }
        });
        return Number;
    }
);