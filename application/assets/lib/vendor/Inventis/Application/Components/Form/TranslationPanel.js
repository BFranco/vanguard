/**
 * Created with JetBrains PhpStorm.
 * User: janesser
 * Date: 28/05/13
 * Time: 10:06
 * To change this template use File | Settings | File Templates.
 */
define(
    [
        "Inventis/Application/Components/Component",
        "Inventis/HTML/Element"
    ],
    function(Component, Element){
        var TranslationPanel = Component.extend({
            //default dependency setting
            _languageCheckboxGroup: 'translationPanelLanguageGroup',

            /**
             * initialize component with provided setup
             * @param setup
             */
            init: function(setup){
                this.__super(setup);
                var layoutId = setup.dependencies.layout,
                    element = new Element(document.getElementById(layoutId));
                element.fire('disableAllTabs', {id: layoutId}, false);
            },

            onDomReady: function(){
                this.__super();
                this.on('groupChange', function(e, options){
                    if (options.groupName === this._languageCheckboxGroup) {
                        var language = String(options.fieldName).substr(8);
                        this.fire('translationPanelLanguageStateChange', {language: language, enabled: options.checked});
                    }
                    return false;
                }, true);
            }
        });
        return TranslationPanel;
    }
);