/**
 *  file
 *
 * Javascript
 *
 * @category
 * @package    Inventis
 * @subpackage Bricks
 * @author     Inventis Web Architects <info@inventis.be>
 * @license    Copyright © Inventis BVBA  - All rights reserved
 * @link       https://github.com/Inventis/Bricks
 */
define(
    [
        "Inventis/Application/Components/Component"
    ],
    function (Component) {
        var CheckboxGroup = Component.extend({
            _groupName: null,

            onDomReady: function(){
                this.__super();
                this.on('change', function(e, options){
                    options.groupName = this.getGroupName();
                    this.fire('groupChange', options);
                    return false;//stop change from further propagating
                }, true);
            },

            setupFromConfig: function(setup){
                this.__super(setup);
                this.setGroupNameFromSetup(setup);
            },

            setGroupNameFromSetup: function(setup){
                if (setup.groupName) {
                    this.setGroupName(setup.groupName);
                }
            },

            setGroupName: function(name){
                this._groupName = name;
                return this;
            },

            getGroupName: function(){
                return this._groupName;
            }
        });
        return CheckboxGroup;
    }
);