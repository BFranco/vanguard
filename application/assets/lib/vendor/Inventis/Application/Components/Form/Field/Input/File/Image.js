/**
 *  file
 *
 * Javascript
 *
 * @category
 * @package    Inventis
 * @subpackage Bricks
 * @author     Inventis Web Architects <info@inventis.be>
 * @license    Copyright © Inventis BVBA  - All rights reserved
 * @link       https://github.com/Inventis/Bricks
 */
define(
    [
        "Inventis/Application/Components/Form/Field/Input/File",
        "Inventis/Config",
        "Inventis/Translator",
        "Inventis/Ext/cropper"
    ],
    function (File, Config, _t, Cropper) {
        Cropper = Ext.ux.Cropper;

        var Image = File.extend({
            _apiField: 'image_id',

            getFileDirectoryPath: function(){
                return Config.baseUrl + 'images/download/';
            },

            setValue: function(value){
                this.__super(value);

                if (value !== '' && value !== null) {
                    if (!this.infoDiv) {
                        this.createInfoDiv();
                    }

                    if (Config.moduleConfig.cropperFormats !== undefined) {
                        // Create the cropper button.
                        var icon = document.createElement('i');
                        icon.className = 'icon-crop';
                        icon.src = Config.siteUrl + 'assets/webadmin/images/icons/crop.png';

                        var button = document.createElement('button');
                        button.type = 'button';
                        button.className = 'component button';
                        if (!this.allowDownload && this.getRequired()) {
                            button.className += ' first';
                        }

                        button.appendChild(icon);
                        button.appendChild(document.createTextNode(
                            _t('INVENTIS.APPLICATION.COMPONENTS.FORM.FIELD.INPUT.FILE.IMAGE.BUTTON.CROP', 'Crop')
                        ));

                        button.onclick = function() {
                            cropperWindow = new Cropper.Window({
                                image           : value//,
                                //related_id      : record.get('related_id'),
                                //related_table   : record.get('related_table')
                            });

                            cropperWindow.show();
                        }

                        this.infoDiv.children[0].appendChild(button);
                    }

                    newline = document.createElement('br');

                    // Add preview of image.
                    var imageTag = document.createElement('img');
                    imageTag.src = Config.baseUrl + 'images/imageuploadfield/' + value;
                    imageTag.alt = 'showcase of selected image';
                    this.infoDiv.children[0].insertBefore(newline, this.infoDiv.children[0].firstChild);
                    this.infoDiv.children[0].insertBefore(imageTag, newline);
                }
            }
        });
        return Image;
    }
);
