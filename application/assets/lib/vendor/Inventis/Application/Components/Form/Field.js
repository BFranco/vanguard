/**
 * Field.js file
 *
 * Javascript
 *
 * @category
 * @package    Inventis
 * @subpackage Bricks
 * @author     Inventis Web Architects <info@inventis.be>
 * @license    Copyright © Inventis BVBA  - All rights reserved
 * @link       https://github.com/Inventis/Bricks
 */
define(
    [
        "Inventis/Application/Components/Component",
        "Inventis/HTML/Element",
        "Sizzle"
    ],
    function (Component, Element, $) {
        var __errorClass = 'error',
            __errorMessageClass = 'error-text';

        var Field = Component.extend({

            _name: null,
            _value: null,
            _required: null,
            _handling: [],

            /**
             * shows the wrapper instead of the element
             */
            show: function(){
                //this.getElement().style.display = 'block';
                var classNames = this.getElement().parentNode.className.split(' ');
                var hide_index = classNames.indexOf('hidden');
                if (hide_index !== -1) {
                    classNames.splice(hide_index, 1);
                    this.getElement().parentNode.className = classNames.join(' ');
                }
                this.fire('domRendered');
            },

            /**
             * hides the wrapper instead of the element
             */
            hide: function(){
                //this.getElement().style.display = 'none';
                var classNames = this.getElement().parentNode.className.split(' ');
                var hide_index = classNames.indexOf('hidden');
                if (hide_index === -1) {
                    classNames.push('hidden');
                    this.getElement().parentNode.className = classNames.join(' ');
                }
            },

            init: function(setup){
                this.__super(setup);
                this.attachListeners();
            },

            setupFromConfig: function(setup){
                this.__super(setup);
                this.setNameFromSetup(setup);
                this.setValueFromSetup(setup);
                this.setHandlingFromSetup(setup);
                this.setRequiredFromSetup(setup);
            },

            setRequired: function (required) {
                this._required = required;
                return this;
            },

            getRequired: function(){
                return this._required;
            },

            setRequiredFromSetup: function(setup) {
                this.setRequired(setup.required);
            },

            setNameFromSetup: function(setup) {
                if (setup.name) {
                    this.setName(setup.name);
                }
            },

            setValueFromSetup: function(setup) {
                if (setup.value) {
                    // bypass setter since the value should already have been set on the component when
                    // loading
                    this._value = setup.value;
                }
            },

            setHandlingFromSetup: function(setup) {
                if (setup.handling) {
                    this._handling = setup.handling;
                }
            },

            attachListeners: function(){
                //we are only interested in events fired from the form we are in, so attach with capture=true
                var capture = true;//assigned for clarity of later on listener
                var self = true;//assigned for clarity of later on listener
                if (undefined !== this.getFormElement()) {
                    this.getFormElement().on(
                        'recordLoaded',
                        this.createFormEventsHandler(this.handleRecordLoaded).bind(this),
                        true
                    );
                    this.getFormElement().on(
                        'recordSaved',
                        this.createFormEventsHandler(this.handleRecordLoaded).bind(this),
                        true
                    );
                    this.getFormElement().on(
                        'recordSaveFailed',
                        this.createFormEventsHandler(this.handleRecordSaveFailed).bind(this),
                        true
                    );
                    for (var x in this._handling) {
                        if (this._handling.hasOwnProperty(x)) {
                            this.getFormElement().on(
                                'change',
                                function(event, options) {
                                    var name = this._handling[x]['name'];
                                    name = (typeof name === 'object' ? name : [name]);
                                    if (name.indexOf(options.fieldName) !== -1) {
                                        var valid = (options.value + '').match(this._handling[x]['match']),
                                            fire  = (valid !== null ?
                                                this._handling[x]['valid'] :
                                                this._handling[x]['invalid']);
                                        if (fire) {
                                            this.fire(fire, options, false);
                                        }
                                    }
                                }.bind(this),
                                true
                            )
                        }
                    }
                }
                this.on('beforeFormSave', this.createFormEventsHandler(this.handleBeforeFormSave));
                this.getFormElement().on('clear', this.onClear.bind(this), true);
                this.getElement().onchange = function() {
                    this.getFormElement().fire('changed', {}, false);
                }.bind(this);
            },

            handleRecordSaveFailed: function(e, options){
                if (!options.errors) {
                    return;
                }
                if (options.errors[this.getName()] !== undefined) {
                    this.setError(options.errors[this.getName()]);
                } else {
                    this.clearError();
                }
            },

            setError: function(message){
                var element = this.getElement().parentNode,
                    classes = element.className.split(' '),
                    messageContainer = $('.'+__errorMessageClass, element).shift();
                if (classes.indexOf(__errorClass) === -1) {
                    classes.push(__errorClass);
                    element.className = classes.join(' ');
                }
                if (messageContainer) {
                    messageContainer.innerHTML = message;
                }
                this.fire('hasError', {errorClass : __errorClass}, true);
            },

            clearError: function() {
                var element = this.getElement().parentNode,
                    classes = element.className.split(' '),
                    messageContainer = $('.'+__errorMessageClass, element).shift(),
                    errorIndex = classes.indexOf(__errorClass);
                if (errorIndex !== -1) {
                    classes.splice(errorIndex, 1);
                    element.className = classes.join(' ');
                }
                if (messageContainer) {
                    messageContainer.innerHTML = '';
                }
            },

            getFormElement: function() {
                if (!this.getForm()) {
                    return undefined;
                }
                return new Element(this.getForm());
            },

            onClear: function(e, options) {
                this.clear(options.defaults);
            },

            clear: function(defaults) {
                var value = defaults && defaults[this.getName()] !== undefined ?
                    defaults[this.getName()] :
                    '';
                this.setValue(value);
                this.clearError();
            },

            /**
             * this method will return an event compliant handler that will
             * call the provided callback only when the event is related to
             * the form that the element belongs to
             * @param {Function} callback
             * @returns {Function}
             */
            createFormEventsHandler: function(callback){
                var me = this;
                return function(e, options){
                    //long live IE for its compliance!!
                    if (options.getTarget() === me.getForm()) {
                        callback.call(me, e, options);
                    }
                };
            },

            /**
             * returns a reference to the form that is relevant to the current field
             * @returns {*}
             */
            getForm: function(){
                return this.getElement().form;
            },

            /**
             * when a recordLoaded even is fired this method can deal with its processing
             * with relation to the form
             * @param {HTMLEvent} e the html event object
             * @param options event options that where set
             */
            handleRecordLoaded: function(e, options){
                var name = this.getName();
                this.clearError();
                // allow_name enables or disables the field for input
                if (options.metadata && options.metadata[name] !== undefined) {
                    this.handleMetaData(options.metadata[name]);
                }
                if (options.record &&
                    options.record[name] !== undefined) {
                    this.setValue(
                        options.record[name]
                    );
                    this.fire('change', {fieldName: this.getName(), value: this.getValue()});
                }
                this.fire('noError', {errorClass : __errorClass});
            },

            handleMetaData: function(fieldMetadata)
            {
                var element = this.getElement();
                if (fieldMetadata.enabled !== undefined) {
                    element.disabled = !fieldMetadata.enabled;
                }
            },

            /**
             * handles the form firing a beforeFormSubmit event
             * this will allow the field to attach its values to the form field
             * values so that ist can be send off to the server for processing
             * @param {HTMLEvent} e the html event object
             * @param options
             */
            handleBeforeFormSave: function(e, options){
                options.fields[this.getName()] = this.getValue();
            },

            /**
             * the field assigned the value to the html element, not validating it
             * that is the responsibility of extending field components
             * @param value
             * @return this
             */
            setValue: function(value){
                var value = value === null ? '' : value;
                this.getElement().value = this._value = value;
                return this;
            },

            /**
             * returns the value of the current HTML element
             */
            getValue: function(){
                var disabled = this.getElement().disabled,
                    value;
                this.getElement().disabled = false;
                value = this.getElement().value;
                this.getElement().disabled = disabled;
                return value;
            },

            /**
             * set the elements name attribute
             * @param name
             * @returns {*}
             */
            setName: function(name){
                this._name = name;
                var element = this.getElement();
                if (element.getAttribute('name')) {
                    element.setAttribute('name', name);
                }
                return this;
            },

            /**
             * returns this field's form field name
             */
            getName: function(){
                return this.getElement().getAttribute('name') || this._name;
            }
        });
        return Field;
    }
);