/**
 *  file
 *
 * Javascript
 *
 * @category
 * @package    Inventis
 * @subpackage Bricks
 * @author     Inventis Web Architects <info@inventis.be>
 * @license    Copyright © Inventis BVBA  - All rights reserved
 * @link       https://github.com/Inventis/Bricks
 */
define(
    [
        "Inventis/Application/Components/Form/Field"
    ],
    function (Field) {
        var Input = Field.extend({
            _type: null,

            onDomReady: function(){
                this.__super();
                //attach change event data
                this.on('change', function(e, options){
                    options.fieldName = this.getName();
                    options.fieldValue = this.getValue();
                }, true, true);//its important that we capture the event before anyone gets to process it
            },

            /**
             * overwrite and attach custom setup handlers
             * @param setup
             */
            setupFromConfig: function(setup){
                this.__super(setup);
                this.setTypeFromSetup(setup);
            },

            /**
             * at the moment there is only one type important to us an that is file
             * when a file component is loaded we need to ensure the correct value is returned
             * so we store the type to allow us to handle this
             * @param setup
             */
            setTypeFromSetup: function(setup){
                if (setup.type !== undefined) {
                    this.setType(setup.type);
                }
            },

            setValue: function(value) {
                if (this.getType() !== 'submit') {
                    this.__super(value);
                }
            },

            /**
             *
             * @param type
             * @returns this
             */
            setType: function(type){
                this._type = type;
                return this;
            },

            /**
             *
             * @returns {String}
             */
            getType: function(){
                return this._type;
            }
        });
        return Input;
    }
);