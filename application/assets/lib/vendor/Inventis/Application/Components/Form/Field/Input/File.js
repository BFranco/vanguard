/**
 *  file
 *
 * Javascript
 *
 * @category
 * @package    Inventis
 * @subpackage Bricks
 * @author     Inventis Web Architects <info@inventis.be>
 * @license    Copyright © Inventis BVBA  - All rights reserved
 * @link       https://github.com/Inventis/Bricks
 */
define(
    [
        "Inventis/Application/Components/Form/Field/Input",
        "Inventis/Config",
        "Inventis/Mixins/Adapters/Renderers/Ext/FileField",
        "Inventis/Application/Components/Component/ApiAwareMixin",
        "Inventis/Translator"
    ],
    function (Input, Config, fileField, ApiAware, _t) {
        var File = Input.extend({
            use: [fileField, ApiAware],

            allowDownload: false,

            infoDiv: null,
            _idField: null,
            _apiField: 'file_id',

            /**
             * overwrite and attach custom setup handlers
             * @param setup
             */
            setupFromConfig: function(setup){
                this.__super(setup);
                this.setAllowDownloadFromSetup(setup);

                //ApiAware function
                this.setApiFromConfig(setup);
                this.setSuccessMessageFromConfig(setup);
                this.setIdFieldFromConfig(setup);
            },

            setIdFieldFromConfig: function(setup){
                this.setIdField(setup.idField);
            },

            setIdField: function(field){
                this._idField = field;
                return this;
            },

            getIdField: function(){
                return this._idField;
            },

            setAllowDownloadFromSetup: function(setup) {
                if (setup.allowDownload !== undefined) {
                    this.allowDownload = setup.allowDownload;
                }
            },

            onDomReady: function(){
                this.__super();
                this.fire('fileFieldComponentLoaded', {field: this.getName()});
                this.on('change', function(){
                    if (!this.getValue()) {
                        //clear info div
                        this.removeInfoDiv();
                    }
                }, true);
            },

            getForm: function(){
                return this.getFileFieldForm();
            },

            createInfoDiv: function(){
                this.infoDiv = document.createElement('div');
                this.getElement().appendChild(this.infoDiv);

                this.infoDiv.className = 'showcase';
                this.infoDiv.innerHTML = '';
                this.infoDiv.appendChild(document.createElement('div'));
            },

            removeInfoDiv: function(){
                if (this.infoDiv) {
                    this.getElement().removeChild(this.infoDiv);
                    this.infoDiv = null;
                }
            },

            getFileDirectoryPath: function(){
                return Config.baseUrl + 'files/download/';
            },

            setValue: function(value){
                var me = this;

                this.setFileFieldValue(value);

                this.removeInfoDiv();

                if (value !== '' && value !== null) {
                    this.createInfoDiv();

                    if (this.allowDownload) {
                        // Create a download button.
                        var icon = document.createElement('i');
                        icon.className = 'icon-download';

                        var button = document.createElement('button');
                        button.type = 'button';
                        button.className = 'component button first';

                        button.appendChild(icon);
                        button.appendChild(document.createTextNode(
                            _t('INVENTIS.APPLICATION.COMPONENTS.FORM.FIELD.INPUT.FILE.BUTTON.DOWNLOAD', 'Download')
                        ));

                        button.onclick = function() {
                            window.location = me.getFileDirectoryPath() + value;
                        }

                        this.infoDiv.children[0].appendChild(button);
                    }

                    if (!this.getRequired()) {
                        // Create a remove button.
                        var removeIcon = document.createElement('i');
                        removeIcon.className = 'icon-remove';

                        var removeButton = document.createElement('button');
                        removeButton.type = 'button';
                        removeButton.className = 'component button';
                        if (!this.allowDownload) {
                            removeButton.className += ' first';
                        }

                        removeButton.appendChild(removeIcon);
                        removeButton.appendChild(document.createTextNode(
                            _t('INVENTIS.APPLICATION.COMPONENTS.FORM.FIELD.INPUT.FILE.BUTTON.REMOVE', 'Verwijder')
                        ));

                        removeButton.onclick = function() {
                            var data = {};
                            me.fire('getLoadedRecord', data);
                            if (!data.record[me.getIdField()]) {
                                throw new Error(
                                    'missing id_field configuration or id_field ['
                                        + me.getIdField()
                                        + '] not available in the form record'
                                );
                            }
                            var params = {};
                            params[me._apiField] = data.record[me.getIdField()];
                            me.apiRequest('delete', params, me.deleteCompleted.bind(me, data.record));
                        }

                        this.infoDiv.children[0].appendChild(removeButton);
                    }
                }
            },

            deleteCompleted: function(record, response){
                if (response.success) {
                    this.setFileFieldValue(null);
                    this.removeInfoDiv();
                    this.fire('recordChanged',{record: record}, true);
                } else {
                    console.log('error: ' + response.message);
                }
            },

            getValue: function(){
                return this.getFileFieldValue();
            },

            render: function(){
                this.renderFileField();
            },

            setSuccessMessageFromConfig: function(setup) {
                if (setup.successMessage) {
                    this.setSuccessMessage(setup.successMessage);
                }
            },

            setSuccessMessage: function(message){
                this._successFlash = new Flash(this.getId(), Flash.CODE_SUCCESS, message, 7000);
                return this;
            },

            showSuccessMessage: function(){
                return this._successFlash.renderTo(this.getElement());
            },

            /**
             * handles the form firing a beforeFormSubmit event
             * this will allow the field to attach its values to the form field
             * values so that ist can be send off to the server for processing
             * this method overwrites the parent to assign file references when required
             * @param {HTMLEvent} e the html event object
             * @param options
             */
            handleBeforeFormSave: function(e, options){
                options.files[this.getName()] = this.getFileFieldFiles();
            }
        });
        return File;
    }
);
