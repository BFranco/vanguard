/**
 *  file
 *
 * Javascript
 *
 * @category
 * @package    Inventis
 * @subpackage Bricks
 * @author     Inventis Web Architects <info@inventis.be>
 * @license    Copyright © Inventis BVBA  - All rights reserved
 * @link       https://github.com/Inventis/Bricks
 */
define(
    [
        "Inventis/Application/Components/Component"
    ],
    function (Component) {
        var Button = Component.extend({
            _event: null,

            setupFromConfig: function(setup){
                this.__super(setup);
                this.setEventFromSetup(setup);
            },

            setEventFromSetup: function(setup){
                if (setup.event !== undefined) {
                    this.setEvent(setup.event);
                }
            },


            onDomReady: function(){
                this.on('click', this.onClick, true);
                this.__super();
            },

            /**
             * listens on the default click event
             * so that we can translate it into another event
             */
            onClick: function(e){
                this.fire(this.getEvent(), {id: this.getId()});
                return false;
            },

            /**
             *
             * @param event
             * @returns {*}
             */
            setEvent: function(event){
                this._event = event;
                return this;
            },

            /**
             *
             * @returns {null}
             */
            getEvent: function(){
                return this._event;
            }
        });
        return Button;
    }
);