/**
 * Component.js file
 *
 * Javasript
 *
 * @category
 * @package    Inventis
 * @subpackage Bricks
 * @author     Inventis Web Architects <info@inventis.be>
 * @license    Copyright © Inventis BVBA  - All rights reserved
 * @link       https://github.com/Inventis/Bricks
 */

/**
 * The component is the base for all components
 * it will provide the minimal functionality
 * components need to function within the modules
 */
define(
    [
        "Inventis/Class",
        "Inventis/Mixins/Observable",
        "Inventis/Mixins/Maskable",
        "Inventis/Application/Components/Component/EventRoutingMixin",
        "Inventis/Application/Webadmin",
        "Inventis/HTML/Element",
        "Sizzle"
    ],
    function (Class, Observable, Maskable, EventRouting, WA, Element, $) {

        /**
         * Component is an extension of Class so that we get
         * build-in extensibility and mixin type parallel extension
         */
        return Class.extend(function(){

            /**
             * this method only fires the events before and after rendering
             * to ensure all components do this when rendering through normal
             * component processes
             */
            function _renderComponent(){
                this.fire('componentBeforeRender');
                this.render();
                this.fire('componentAfterRender');
            }

            /**
             * everything defined below is the public
             * scope for the component object and can be extended
             */
            return {
                use: [Observable, Maskable, EventRouting],

                /**
                 * renderTo is the selector that will be used
                 * to render the component to after init
                 * @private
                 */
                _renderTo:null,

                /**
                 * the components ID, every component has one
                 * it will be used to fire events from a given element
                 * @private
                 */
                _id:null,

                /**
                 * the first element that was found matching the renderTo selector
                 * @var HTMLElement
                 * @private
                 */
                _element:null,

                /**
                 * init will setup component from config and will ensure the component
                 * executes all dom ui related tasks as soon as the dom is ready
                 * @param setup
                 */
                init: function(setup){
                    if (setup) {
                        this.setupFromConfig(setup);
                    }
                    WA.ready(this.onDomReady, this);
                },

                /**
                 *
                 * @param parent
                 * @returns bool
                 */
                isDescendant: function(parent) {
                    var child = this.getElement();
                    if (parent.compareDocumentPosition !== undefined) {
                        return parent.compareDocumentPosition(child) & Node.DOCUMENT_POSITION_CONTAINED_BY;
                    }
                    var node = child.parentNode;
                    while (node !== null) {
                        if (node === parent) {
                            return true;
                        }
                        node = node.parentNode;
                    }
                    return false;
                },

                /**
                 * this method is what gets called at the end
                 * of the component initialization process and only as
                 * soon as the DOM is ready for manipulation
                 * it allows components to set/remove handlers
                 * as part of the component initialization process
                 * after its finished it will fir a component initialized event
                 * that provides access an HTMLElement wrapped that implements Observable and the element id
                 */
                onDomReady: function(){
                    if (this.getRenderTo()) {
                        _renderComponent.call(this);
                    }
                    this.fire('componentInitialized', {element: new Element(this.getElement()), id: this.getId()});
                },

                /**
                 * this method will call all known object methods from the object
                 * to setup the object as far as possible from configuration
                 * @protected
                 */
                setupFromConfig: function(setup){
                    if (setup === undefined) {
                        throw new SyntaxError(
                            'setupFromConfig did not receive a setup parameter, '+
                            'ensure you passed the setup to your __super method'
                        );
                    }
                    this.setRenderToFromSetup(setup);
                    this.setIdFromSetup(setup);
                    this.setComponentsFromSetup(setup);
                    this.setEventRoutingFromConfig(setup);

                    this.on('disable', this.onDisable, true);
                    this.on('enable', this.onEnable, true);
                    this.on('show', this.onShow, true);
                    this.on('hide', this.onHide, true);
                },

                onDisable: function(options, event) {
                    this.disable();
                    return false;
                },

                onEnable: function(options, event) {
                    this.enable();
                    return false;
                },

                /**
                 * adds components available in the config as child components
                 * @param setup
                 */
                setComponentsFromSetup: function(setup){
                    this.fire('createComponents', {components: setup.components}, false, false, false);
                },

                /**
                 * set the id as specified in the provided setup
                 * @param setup
                 */
                setIdFromSetup: function(setup){
                    if (setup.id) {
                        this.setId(setup.id);
                    }
                },

                /**
                 * sets renderTo as specified in the provided setup
                 * @param setup
                 */
                setRenderToFromSetup: function(setup){
                    if (setup.renderTo) {
                        this.setRenderTo(setup.renderTo);
                    }
                },

                /**
                 * set the ID of a component
                 * @param id
                 * @return this
                 */
                setId: function(id){
                    this._id = id;
                    return this;
                },

                /**
                 * returns the set component id
                 * @returns string
                 */
                getId: function(){
                    return this._id;
                },

                /**
                 * sets the dom selector to render the component to
                 * this should be a unique selector as only one element can be used per
                 * component
                 * @param selector
                 * @return this
                 */
                setRenderTo: function(selector){
                    this._renderTo = selector;
                    return this;
                },

                /**
                 * get the currently set renderTo selector
                 */
                getRenderTo: function(){
                    return this._renderTo;
                },

                /**
                 * sets the HTMLElement for use with this component
                 * @param element
                 * @return this
                 */
                setElement: function(element){
                    this._element = element;
                    return this;
                },

                /**
                 * returns the currently set HTMLElement
                 * the component is set to render to
                 * this is also the element that will trigger
                 * events when set (when not set the document triggers events instead
                 * @return HTMLElement
                 */
                getElement: function(){
                    if (!this._element) {
                        var selector = this.getId();
                        if (!selector) {
                            throw new Error('No component id is set, but is required by getElement');
                        }
                        var element = document.getElementById(selector);
                        if (!element) {
                            throw new Error("Element not found, invalid id ["+selector+"] given.");
                        }
                        this.setElement(element);
                    }
                    return this._element;
                },

                /**
                 * render is called on DomReady event
                 * the base component doesn't need rendering,
                 * but this is the method called and should be overwritten
                 * if the extending component needs more rendering
                 */
                render: function(){
                    var target = $(this.getRenderTo()).shift();
                    if (target) {
                        target.appendChild(this.getElement());
                    }
                },

                /**
                 * disables the component to prevent further user interaction
                 */
                disable: function(){
                    this.mask();
                    this.fire('componentDisabled', {id:this.getId(), componentType: this.getElement().getAttribute("data-componentType")});
                },

                /**
                 * enables the component to prevent further user interaction
                 */
                enable: function(){
                    this.unmask();
                    this.fire('componentEnabled', {id:this.getId(), componentType: this.getElement().getAttribute("data-componentType")});
                },

                onShow: function() {
                    this.show();
                },

                onHide: function() {
                    this.hide();
                },

                /**
                 * opens the window
                 */
                show: function(){
                    //this.getElement().style.display = 'block';
                    var classNames = this.getElement().className.split(' ');
                    var hide_index = classNames.indexOf('hidden');
                    if (hide_index !== -1) {
                        classNames.splice(hide_index, 1);
                        this.getElement().className = classNames.join(' ');
                    }
                    this.fire('domRendered');
                },

                /**
                 * closes the window
                 */
                hide: function(){
                    //this.getElement().style.display = 'none';
                    var classNames = this.getElement().className.split(' ');
                    var hide_index = classNames.indexOf('hidden');
                    if (hide_index === -1) {
                        classNames.push('hidden');
                        this.getElement().className = classNames.join(' ');
                    }
                }
            }
        });
    }
);