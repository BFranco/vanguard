/**
 *  file
 *
 * Javascript
 *
 * @category
 * @package    Inventis
 * @subpackage Bricks
 * @author     Inventis Web Architects <info@inventis.be>
 * @license    Copyright © Inventis BVBA  - All rights reserved
 * @link       https://github.com/Inventis/Bricks
 */
define(
    [
        "Inventis/Application/Components/Button",
        "Inventis/Application/Webadmin",
        "Sizzle"
    ],
    function (Button, WA, $) {
        var AjaxButton = Button.extend({
            _successMessage: null,
            _failMessage: null,
            _event: null,
            _flash: null,
            _renderFlashTo: null,

            setupFromConfig: function(setup){
                this.__super(setup);
                this.setUrlFromSetup(setup);
                this.setSuccessMessageFromSetup(setup);
                this.setFailMessageFromSetup(setup);
                this.setRenderFlashToFromSetup(setup);
            },

            setSuccessMessageFromSetup: function(setup) {
                if (setup.successMessage !== undefined) {
                    this.setSuccessMessage(setup.successMessage);
                }
            },

            setFailMessageFromSetup: function(setup) {
                if (setup.failMessage !== undefined) {
                    this.setFailMessage(setup.failMessage);
                }
            },

            setRenderFlashToFromSetup: function(setup) {
                if (setup.renderFlashTo !== undefined) {
                    this.setRenderFlashTo(setup.renderFlashTo);
                }
            },

            setUrlFromSetup: function(setup){
                if (setup.url !== undefined) {
                    this.setUrl(setup.url);
                }
            },

            showFlash: function(success, message) {
                this._successFlash = new Flash(
                    this.getId(),
                    success ? Flash.CODE_SUCCESS : Flash.CODE_ERROR,
                    message,
                    7000
                );
                return this._successFlash.renderTo($(this.getRenderFlashTo()).shift());
            },

            /**
             */
            onClick: function(e){
                this.__super(e);
                WA.ajax(
                    this.getUrl(),
                    {},
                    function(response, options) {
                        this.showFlash(
                            response.success,
                            response.success ? this.getSuccessMessage() : this.getFailMessage()
                        );
                    },
                    this,
                    {}
                );
                return false;
            },

            setUrl: function(url){
                this._url = url;
                return this;
            },

            getUrl: function(){
                return this._url;
            },

            setSuccessMessage: function(successMessage){
                this._successMessage = successMessage;
                return this;
            },

            getSuccessMessage: function(){
                return this._successMessage;
            },

            setFailMessage: function(failMessage){
                this._failMessage = failMessage;
                return this;
            },

            getFailMessage: function(){
                return this._failMessage;
            },

            setRenderFlashTo: function(renderFlashTo){
                this._renderFlashTo = renderFlashTo;
                return this;
            },

            getRenderFlashTo: function(){
                return this._renderFlashTo;
            }
        });
        return AjaxButton;
    }
);