/**
 *  file
 *
 * Javascript
 *
 * @category
 * @package    Inventis
 * @subpackage Bricks
 * @author     Inventis Web Architects <info@inventis.be>
 * @license    Copyright © Inventis BVBA  - All rights reserved
 * @link       https://github.com/Inventis/Bricks
 */
define(
    [
        "Inventis/Application/Components/Component",
        "Inventis/Mixins/Adapters/Renderers/Ext/Grid",
        "Inventis/Application/Components/Component/ApiAwareMixin"
    ],
    function (Component, GridRenderer, ApiAwareMixin) {
        var Grid = Component.extend({
            use: [GridRenderer, ApiAwareMixin],

            setupFromConfig: function(setup){
                this.__super(setup);
                //let mixins configure themselves from setup
                this.setApiFromConfig(setup);
                this.setupGridFromConfig(setup);
            },

            onDomReady: function(){
                this.__super();
                this.attachEventHandlers();
            },

            /**
             * this method attaches event handlers that are NOT part of the framework
             * dependency (e.g ExtJS) as these event can be handled uniformly across
             * dependencies
             */
            attachEventHandlers: function(){
                this.on('addRecord', this.onAddRecord, true);
                this.on('moveRecord', this.onMoveRecord, true);
                this.on('deleteRecord', this.onDeleteRecord, true);
            },

            onDeleteRecord: function(e, options){
                this.mask();
                this.apiRequest('deleteRecord', {id: options.record.data.id}, function(response){
                    if (response.success) {
                        this.gridDeleteRecord(options.record, response.result);
                    }
                });
                this.fire('deleted');
            },

            /**
             * handler for the addREcordWithoutRefresh event
             * @param e
             * @param options
             */
            onAddRecord: function(e, options) {
                if (options.data === undefined) {
                    return;
                }
                //only fire server side call if not specifically requested not to
                if (options.commit !== false) {
                    this.apiRequest('addRecord', {id: null}, function(response){
                        if (response.success) {
                            this.gridAddRecord(options, response.result);
                        }
                    });
                } else {
                    this.gridAddRecord(options);
                }
            },

            /**
             * this handler is called when the grid has defined that a record
             * needs to be moved from one location to another
             * @param e
             * @param options
             */
            onMoveRecord: function(e, options){
                this.mask();
                var data = {
                    id: options.id,
                    position: options.position,
                    target: options.target.get('id')
                };
                this.apiRequest('moveRecord', data, function(response){
                    if (response.success) {
                        options.originalHandler.call();
                        this.gridMoveRecord(options, response.result);
                    }
                });
            },

            /**
             * render the grid when rendering is requested
             */
            render: function(){
                this.__super();
                this.renderGrid();
            }
        });
        return Grid;
    }
);