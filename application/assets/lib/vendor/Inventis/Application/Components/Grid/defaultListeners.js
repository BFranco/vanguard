

/**
 *  file
 *
 * Javascript
 *
 * @category
 * @package    Inventis
 * @subpackage Bricks
 * @author     Inventis Web Architects <info@inventis.be>
 * @license    Copyright © Inventis BVBA  - All rights reserved
 * @link       https://github.com/Inventis/Bricks
 */
define(
    [

    ],
    function () {
        return {
            /**
             * this listener can be used in the grid on the filter
             * when it is changed it will update the store by reloading with additional parameters
             * we attach a listener to 'el' instead if the ext component as this will cause the change event
             * to only trigger on blur and ENTER key pressing
             */
            filterChange: {
                element: 'el', //defining this will return the dom element instead of values
                fn: function(e, input){
                    var value = input.value,
                        store = this.getGridStoreInstance(),
                        grid = this.getGridInstance(),
                        filters = store.filters.items,
                        newFilters = [],
                        columns = grid.columns,
                        x,
                        filtered = columns.map(function(column) {
                            if (column.filterable) {
                                return {
                                    id: column.dataIndex + '_filter',
                                    property: column.dataIndex,
                                    value: input.value
                                };
                            }
                            return false;
                        }, value).filter(function(element) { return element; });

                    if (value === '') {
                        var gridFilters = filters;
                        for (var y in filtered) {
                            if (filtered.hasOwnProperty(y)) {
                                var idToRemove = filtered[y].id;
                                for (x in gridFilters) {
                                    if (gridFilters[x].id === idToRemove) {
                                        gridFilters.splice(x, 1);
                                    }
                                }
                            }
                        }
                        this.gridReloadStore({filters: gridFilters});
                    } else {
                        newFilters = filtered;
                        this.addFilters(newFilters);
                    }
                }
            },
            //todo: this is a demo listener
            comboChangeTest: {
                fn: function(combobox, newValue, oldValue){
                    var store = this.getGridStoreInstance();
                    store.filter({
                        id: combobox.getId(),
                        property: combobox.filterColumn,
                        value: newValue
                    });
                }
            },
            beforeDragOver: function() {
                return true;
            },

            /**
             * because the grid is trying to update the records, but fails at it horribly
             * we need to tell it to do its job for it by overwriting the dropHandler with a
             * custom one
             * @param node
             * @param data
             * @param overModel
             * @param dropPosition
             * @param dropHandlers
             */
            beforeDrop: function(node, data, overModel, dropPosition, dropHandlers) {
                var me = this;
                // Defer the handling
                dropHandlers.wait = true;
                dropHandlers.originalDropHandler = dropHandlers.processDrop;
                //the actual handler overwrite
                dropHandlers.processDrop = function () {
                    /*
                     * fires the moveRecord event that declares 3 properties
                     * record: the record that is being dragged
                     * target: the record to which the target is dropped
                     * position: the position relative to the target [if any]
                     */
                    me.fire('moveRecord', {id: data.records[0].get('id'), target: overModel, position: dropPosition, originalHandler: dropHandlers.originalDropHandler});
                };
                /**
                 */
                dropHandlers.processDrop();
                dropHandlers.cancelDrop();//removes the indicator
            },

            groupedDragOver: function(overRecord, draggedRecord, dragSource, target, event, id) {
                if (!overRecord) {
                    return false;
                }
                var overCategory = overRecord.get('category_id'),
                    draggedCategory = draggedRecord.get('category_id');
                return overCategory === draggedCategory;
            }
        };
    }
);