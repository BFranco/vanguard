/**
 *  file
 *
 * Javascript
 *
 * @category
 * @package    Inventis
 * @subpackage Bricks
 * @author     Inventis Web Architects <info@inventis.be>
 * @license    Copyright © Inventis BVBA  - All rights reserved
 * @link       https://github.com/Inventis/Bricks
 */
define(
    [
        "Inventis/Application/Components/Grid",
        "Inventis/Application/Webadmin",
        "ExtJS"
    ],
    function (Grid, WA) {
        var TreePanel = Ext.tree.Panel,
            Store     = Ext.data.TreeStore,


        Tree = Grid.extend({

            itemCount: 0,

            /**
             * overrides creation of a grid with creation of a tree
             * @param config
             * @returns {Ext.tree.Panel}
             */
            createGridInstance: function(config) {
                return new TreePanel(config);
            },

            /**
             * overrides creation of the grid's normal store with a tree compatible store
             * @param config
             * @returns {Ext.data.TreeStore}
             */
            createStoreInstance: function(config) {
                return new Store(config);
            },

            getRowClass: function() {
                return '';
            },

            _attachStoreEventListeners: function(instance){
                //ext event listener
                instance.on('beforeload', this.onBeforeLoad.bind(this), this);
                instance.on('beforeexpand', this.onBeforeExpand.bind(this), this);
                instance.on('load', this.onLoad.bind(this), this);
                instance.on('expand', this.onExpand.bind(this), this);
            },

            /**
             *
             * @param {Ext.grid.Panel} instance
             * @private
             */
            _attachGridEventListeners: function(instance){
                this.__super(instance);
                instance.un('beforeitemclick', this.onBeforeSelectDisable, this);
                instance.un('beforeselect', this.onBeforeSelectDisable, this);
            },

            onBeforeLoad: function() {
                this.mask();
            },

            onBeforeExpand: function() {
                this.mask();
            },

            onLoad: function(store, grid, records) {
                if (records) {
                    this.itemCount = records.length;
                }
                this.unmask();
            },

            onExpand: function() {
                this.unmask();
            },

            updateNode: function(node) {
                this.apiRequest('getRecord', {id : node.getId()}, function(response) {
                    if (response.success) {
                        var newNode = this.createModel(response.result),
                            parent = node.parentNode,
                            index = node.get('index');
                        parent.removeChild(node);
                        parent.insertChild(index, newNode);
                    }
                });
            },

            /**
             * called after drop is confirmed this method
             * will ensure the record is moved server side
             * and only then execute the actual move
             */
            onBeforeRecordMoveDrop: function(options) {
                this.mask();
                var data = {
                    id: options.id,
                    position: options.position,
                    target: options.target.get('id')
                };
                this.apiRequest('moveRecord', data, function(response){
                    if (response.success) {
                        if (options.callback) {
                            options.callback.call();
                        }
                        this.getGridInstance().getView().refresh();
                    }
                    this.unmask();
                });
            },

            /**
             *
             * @param record
             * @param [response]
             */
            gridDeleteRecord: function(record, response) {
                var store  = this.getGridStoreInstance(),
                    grid = this.getGridInstance();
                if (record) {
                    record.remove();
                    this.fire('recordRemoved', {record: record});
                    this.itemCount--;
                }
                this.unmask();
            },

            setFields: function(record, fields) {
                for (var x in fields) {
                    if (fields.hasOwnProperty(x)) {
                        record.set(x, fields[x]);
                    }
                }
            },

            /**
             * called when the ad button is dropped in the tree
             * @param options
             */
            onAddButtonDropRecord: function(options) {
                var data = {},
                    me = this,
                    sendApiRequest = function() {
                        data = WA.merge(data, options.record.raw);
                        if(options.target !== null) {
                            data.target = options.target.get('id');
                        }
                        data.position = options.position;
                        this.mask();
                        this.apiRequest('addRecord', data, function(response) {
                            if (response.success) {
                                this.setFields(options.record, response.result);
                                if (options.callback) {
                                    options.callback.call();
                                }
                                options.record.commit();
                            }
                            this.unmask();
                        });
                    }.bind(this);

                if (options.record === undefined) {
                    return;
                }
                // Do this to prevent duplicates
                if (options.position == 'append') {
                    options.target.expand(false, sendApiRequest);
                } else {
                    sendApiRequest();
                }
            }
        });

        return Tree;
    }
);