/**
 *  file
 *
 * Javascript
 *
 * @category
 * @package    Inventis
 * @subpackage Bricks
 * @author     Inventis Web Architects <info@inventis.be>
 * @license    Copyright © Inventis BVBA  - All rights reserved
 * @link       https://github.com/Inventis/Bricks
 */
define(
    [
        "Inventis/Console",
        "Inventis/Config",
        "Inventis/Translator"
    ],
    function (console, Config, _) {
        return {
            /**
             * grid toolbar handler
             */
            add: function(){
                this.fire('addRecord', {id: null});
            },

            /**
             * row action handler
             * @param grid
             * @param rowIndex
             * @param colIndex
             * @param button
             * @param event
             * @param record
             * @param tr
             */
            edit: function(grid, rowIndex, colIndex, button, event, record, tr){
                this.fire('edit', {id: record.get('id'), record: record.data});
            },

            /**
             * row action handler
             * @param grid
             * @param rowIndex
             * @param colIndex
             * @param button
             * @param event
             * @param record
             * @param tr
             */
            delete: function(grid, rowIndex, colIndex, button, event, record, tr) {
                var window = new Ext.window.MessageBox();
                window.confirm(
                    _('INVENTIS.APPLICATION.COMPONENTS.GRID.DEFAULT_HANDLERS.DELETE', 'Verwijderen'),
                    _('INVENTIS.APPLICATION.COMPONENTS.GRID.DEFAULT_HANDLERS.ARE_YOU_SURE', 'Ben je zeker?'),
                    function(btn){
                        if (btn === 'yes') {
                            this.fire('deleteRecord', {record: record}, true);
                        }
                    }.bind(this)
                );
            },

            /**
             * row action handler
             * @param grid
             * @param rowIndex
             * @param colIndex
             * @param button
             * @param event
             * @param record
             * @param tr
             */
            print: function(grid, rowIndex, colIndex, button, event, record, tr) {
                window.open('print/' + record.get('id'), 'printwin', 'width=500,height=400,location=0,status=0');
            },

            test: function(){
                console.log(arguments);
            }
        };
    }
);