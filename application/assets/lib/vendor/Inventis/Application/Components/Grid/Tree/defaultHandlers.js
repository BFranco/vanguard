/**
 *  file
 *
 * Javascript
 *
 * @category
 * @package    Inventis
 * @subpackage Bricks
 * @author     Inventis Web Architects <info@inventis.be>
 * @license    Copyright © Inventis BVBA  - All rights reserved
 * @link       https://github.com/Inventis/Bricks
 */
define(
    [
        'Inventis/Application/Webadmin'
    ],
    function (WA) {
        return {
            addButtonDrop: function(){
                this.fire('addRecord', {id: null, parent_id: null});
            },

            buttonClick: function(button) {
                var showAddButtonAlert = function(){
                        WA.alert(button.alert.title, button.alert.text);
                    };
                if (this.itemCount === 0) {
                    var record = button.dd.getDragData().item;
                    this.onAddButtonDropRecord({
                        record: record,
                        target: null,
                        position: 'after',
                        callback: function() {
                            this.itemCount = 1;
                            this.gridReloadStore({});
                        }.bind(this)
                    });
                } else {
                    showAddButtonAlert.call();
                }
            }
        };
    }
);