/**
 *  file
 *
 * Javascript
 *
 * @category
 * @package    Inventis
 * @subpackage Bricks
 * @author     Inventis Web Architects <info@inventis.be>
 * @license    Copyright © Inventis BVBA  - All rights reserved
 * @link       https://github.com/Inventis/Bricks
 */
define(
    [
        "Inventis/Config"
    ],
    function (Config) {
        return {
            /**
             * this listener can be used in the grid on the filter
             * when it is changed it will update the store by reloading with additional parameters
             * we attach a listener to 'el' instead if the ext component as this will cause the change event
             * to only trigger on blur and ENTER key pressing
             */
            filterChange: {
                element: 'el', //defining this will return the dom element instead of values
                fn: function(e, input){
                    var i, filters=[],
                        store = this.getGridStoreInstance(),
                        columns = this.getGridColumns();
                    if (input.value) {
                        for (i=0;i<columns.length;i++) {
                            if (columns[i].filterable) {
                                filters.push({
                                    /*
                                     * adding an id will cause the filter to be
                                     * replaces when pushed into filters again,
                                     * the alternative is to use clearFilters(true)
                                     * but this will fail if you have multiple filter types
                                     * e.g a combo and this search filter because the one will clear
                                     * the other
                                     */
                                    id: columns[i].dataIndex + '_filter',
                                    property: columns[i].dataIndex,
                                    value: input.value
                                });
                            }
                        }
                        //clear filter without updating UI
                        store.filter(filters);
                    } else {
                        store.clearFilter();
                    }
                }
            },

            nodeDragOver: function( target, position, dragData, e, eOpts ) {
                var record = dragData.records[0];
                // user 'Inventis' can do everything, so skip the checks!
                if (Config.application.group_id != "1") {
                    // some rules only apply on append
                    if (position === 'append') {
                        // target doesn't allow children
                        if (target.get('allow_children') === false) {
                            return false;
                        }
                        // record's parent_id doesn't match the id of the target to append to, but doesn't allow parent change
                        if (record.getId() !== 0 &&
                            record.get('parent_id') !== target.get('id') &&
                            record.get('allow_parentchange') === false) {
                            return false;
                        }
                        /*
                         * record's level doesn't match the level of the children of the target to append to, but doesn't allow
                         * level change
                         */
                        if (record.getId() !== 0 &&
                            record.get('depth') !== (target.get('depth') + 1) &&
                            record.get('allow_levelchange') === false) {
                            return false;
                        }
                    } else {
                        // some rules only apply on before and after, which are treated the same for move checks
                        /*
                         * record's parent_id doesn't match the id of the target where the record will be placed before or after,
                         * but the record isn't allowed to change parent
                         */
                        if (record.getId() !== 0 &&
                            record.get('parent_id') !== target.get('parent_id') &&
                            record.get('allow_parentchange') === false) {
                            return false;
                        }
                        /*
                         * record's level doesn't match the level of the target where the record will be placed before or after,
                         * but doesn't allow level change
                         */
                        if (record.getId() !== 0 &&
                            record.get('depth') !== target.get('depth') &&
                            record.get('allow_levelchange') === false) {
                            return false;
                        }
                    }
                }
                return true;
            },
            /**
             * because the grid is trying to update the records, but fails at it horribly
             * we need to tell it to do its job for it by overwriting the dropHandler with a
             * custom one
             * @param node
             * @param data
             * @param overModel
             * @param dropPosition
             * @param dropHandlers
             */
            beforeDrop: function(node, data, overModel, dropPosition, dropHandlers) {
                var me = this,
                    originalDropHandler = dropHandlers.processDrop;
                // Defer the handling
                dropHandlers.wait = true;
                //the actual handler overwrite

                overModel.expand(false, function() {
                    dropHandlers.processDrop = function () {
                        originalDropHandler.bind(null, arguments);
                        if (data.processDrop) {
                            data.processDrop(originalDropHandler, dropPosition, overModel);
                        } else {
                            me.onBeforeRecordMoveDrop({
                                id: data.records[0].get('id'),
                                target: overModel,
                                position: dropPosition,
                                callback: originalDropHandler
                            });
                        }
                    };

                    dropHandlers.processDrop();
                });
            },

            /**
             * method is set as the default handler for the button's afterrender listener
             * in the tree's default config
             * @param button
             */
            afterAddButtonRender: function(button){
                var me = this;
                button.generateNewNode = function() {
                    // create a new record from config defaults or empty object
                    var record = Config.moduleConfig.defaultValues;
                    record['enabled_languages'] = '';
                    for (x in Config.moduleConfig.mandatoryLanguages) {
                        if (Config.moduleConfig.mandatoryLanguages.hasOwnProperty(x)) {
                            record['enabled_languages'] += ',' + Config.moduleConfig.mandatoryLanguages[x];
                        }
                    }
                    record['enabled_languages'] = record['enabled_languages'].substr(1);
                    return me.createModel(Config.moduleConfig.defaultValues || {});
                };
                var record = button.generateNewNode();
                button.dd = new Ext.dd.DragSource(button.getEl(), {
                    ddGroup: this.getGridDropGroup(),
                    dragData: {
                        item: record,
                        records: [record],
                        processDrop: function(originalDropHandler, position, target){
                            var callback = function(){
                                originalDropHandler.call();
                                var record   =  button.generateNewNode(),
                                    dragData = button.dd.getDragData();
                                //for whatever reason ext 4.2.1 needs both set...
                                dragData.item = record;
                                dragData.records = [record];
                            };
                            me.onAddButtonDropRecord(
                                {position: position, target: target, record: button.dd.getDragData().item, callback: callback}
                            );
                        }
                    }
                });
            }
        }
    }
);