/**
 *  file
 *
 * Javascript
 *
 * @category
 * @package    Inventis
 * @subpackage Bricks
 * @author     Inventis Web Architects <info@inventis.be>
 * @license    Copyright © Inventis BVBA  - All rights reserved
 * @link       https://github.com/Inventis/Bricks
 */

/**
 * this is the default list of renderers that is assigned to the grid,
 * when you want customer renderers its best that you add you renderer
 * definitions (full name spaced renderer files) to the grid configuration
 * this should only be a list of commonly used renderers by the whole framework
 */
define(
    [
        'Inventis/Translator'
    ],
    function (_) {

        var renderers = {
            /**
             * Converts bytes to a more appropriate value (kilobytes, megabytes, gigabytes) with
             * the correct abbreviation (B, KB, MB, GB) after the value
             * @param value
             * @param meta
             * @param record
             * @returns {string}
             */
            humanReadableBytes: function(value, meta, record) {
                if (typeof value !== 'number') {
                    value = parseInt(value, 10);
                }
                if (value < 1024) {
                    return value.toFixed(2) + ' B';
                } else if (value < (1024 * 1024)) {
                    return (value / 1024).toFixed(2) + ' KB';
                } else {
                    return (value / (1024 * 1024)).toFixed(2) + ' MB';
                }
            },
            dateRenderer: function(value, meta, record) {
                if (value === null || value === '') {
                    return '';
                }

                addLeadingZero = function(value) {
                    if (value < 10) {
                        value = '0' + value;
                    }
                    return '' + value;
                };
                var date = '',
                    month = '',
                    year = '';
                date = addLeadingZero(value.getDate());
                month = addLeadingZero(value.getMonth() + 1);
                year = value.getFullYear();
                return date + '-' + month + '-' + year;
            },
            language: function(languageCode, value, meta, record) {
                var el = document.createElement('i');
                el.className = 'icon-remove-sign red';

                if (value) {
                    value = value.split(',');
                    if (value.indexOf(languageCode) !== -1) {
                        el.className = 'icon-ok-sign green';
                    }
                }

                return el.outerHTML;
            },
            trueOrFalse: function(value) {
                var el = document.createElement('i');
                el.className = 'icon-remove-sign red';
                if (value) {
                    el.className = 'icon-ok-sign green';
                }

                return el.outerHTML;
            },
            imageRenderer: function(value) {
                if (value !== null && value != '') {
                    return '<img src="/images/wagrid/' + value + '" />';
                }

                return '';
            },
            /**
             * Renderer for the status fieldset
             */
            statusRenderer: function(value, meta, record) {
                var now = new Date().getTime(),
                    publish_ts,
                    unpublish_ts,
                    publish_date,
                    unpublish_date,
                    online_style,
                    offline_style,
                    pending_style,
                    el;

                publish_ts = record.get('publish_date') ?
                    record.get('publish_date').getTime() :
                    now;


                unpublish_ts = record.get('unpublish_date') ?
                    record.get('unpublish_date').getTime() :
                    now;

                online_style = 'label-online';
                offline_style = 'label-offline';
                pending_style = 'label-pending';

                el = document.createElement('span');

                switch (value) {
                    case 'online':
                        el.className = online_style;
                        el.innerHTML = _('GENERAL.STATUS.ONLINE', 'Gepubliceerd');
                        break;
                    case 'pending':

                        publish_date = renderers.dateRenderer(new Date(publish_ts), meta, record);
                        unpublish_date = renderers.dateRenderer(new Date(unpublish_ts), meta, record);

                        if (now >= publish_ts && now <= unpublish_ts) {
                            el.className = online_style;
                            if (record.get('publish_date')) {
                                el.innerHTML = _('GENERAL.STATUS.PENDING.ONLINE_SINCE', 'Gepubliceerd na') +
                                    ' ' + publish_date;
                            } else {
                                el.innerHTML = _('GENERAL.STATUS.PENDING.ONLINE_TILL', 'Gepubliceerd t.e.m.') +
                                    ' ' + unpublish_date;
                            }

                        } else if (now > unpublish_ts) {
                            // OFFLINE through publishing rules
                            el.className = offline_style;
                            el.innerHTML = _('GENERAL.STATUS.PENDING.OFFLINE', 'Gedepubliceerd na') +
                                ' ' + unpublish_date;
                        } else {
                            // Not yet online
                            el.className = pending_style;
                            el.innerHTML = _('GENERAL.STATUS.PENDING.PENDING', 'Publiceren vanaf') +
                                ' ' + publish_date;
                        }
                        break;
                    default:
                        // OFFLINE
                        el.className = offline_style;
                        el.innerHTML = _('GENERAL.STATUS.OFFLINE', 'Niet gepubliceerd');
                }

                return el.outerHTML;
            }
        }

        return renderers;
    }
);
