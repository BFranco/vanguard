/**
 * Window.js file
 *
 * Javascript
 *
 * @category
 * @package    Inventis
 * @subpackage Bricks
 * @author     Inventis Web Architects <info@inventis.be>
 * @license    Copyright © Inventis BVBA  - All rights reserved
 * @link       https://github.com/Inventis/Bricks
 */
define(
    [
        "Inventis/Application/Components/Window",
        "Inventis/Button",
        "Inventis/HTML/Element",
        "Inventis/Config",
        "Inventis/Mixins/Plupload",
        "Inventis/Translator",
        "Sizzle"
    ],
    function (Window, Button, Element, Config, Plupload, _, $) {
        //static scope

        //object definition
        var UploadWindow = Window.extend({
            use: [Plupload],
            /**
             * Upload config received from the PHP components
             */
            _uploadConfig: null,

            /**
             * Related table field of the upload window
             */
            _relatedTable : null,

            /**
             * id of the record in related table, where the file is uploaded to
             */
            _relatedId : 0,

            __construct: function() {
            },

            /**
             * Setup this element with the provided setup
             * @param setup
             */
            setupFromConfig: function(setup) {
                this.__super(setup);
                this.on('changeRelatedId', this.onRelatedIdChange, true);
                this.setUploadConfigFromConfig(setup);
                this.setRelatedTableFromConfig(setup);
            },

            /**
             * Parses the related table
             * @param setup
             */
            setRelatedTableFromConfig: function(setup) {
                if (setup.relatedTable !== undefined) {
                    this.setRelatedTable(setup.relatedTable);
                }
            },

            /**
             * Parses the upload config
             * @param setup
             */
            setUploadConfigFromConfig: function(setup) {
                if (setup.uploadConfig !== undefined) {
                    this.setUploadConfig(setup.uploadConfig);
                }
            },

            // SETTERS

            setUploadConfig: function(uploadConfig) {
                this._uploadConfig = uploadConfig;
            },

            setRelatedTable: function(relatedTable) {
                this._relatedTable = relatedTable;
            },

            setRelatedId: function(relatedId) {
                this._relatedId = relatedId;
            },

            // GETTERS

            getUploadConfig: function() {
                return this._uploadConfig;
            },

            getRelatedTable: function() {
                return this._relatedTable;
            },

            getRelatedId: function() {
                return this._relatedId;
            },
            /**
             * attaches to the init to set up the close button
             * @param setup
             */
            init: function(setup) {
                this.__super(setup);

                this.initPlupload();
            },

            /**
             * Parses the files and sends them to his grid
             * @param files
             */
            filesReady: function(files) {
                for (i in files) {
                    if (files.hasOwnProperty(i)) {
                        var eventData = [];
                        eventData.commit = false;
                        eventData.data = this.getDataFromFile(files[i]);
                        this.fire('fileReady', eventData, true);
                    }
                }
            },

            /**
             * Initialize plupload
             */
            initPlupload: function() {
                // prepares config
                var uploadConfig = this.getUploadConfig();
                uploadConfig.multipart = true;
                uploadConfig.multipart_params = {
                    __SID__ : Config.__SID__,
                    __USERID__ : Config.__USERID__,
                    related_table : this.getRelatedTable()
                };
                // Set config
                this.setPluploadConfig(uploadConfig);

                // Init plupload and add config
                this.pluploadInit();
                // bind events

                // Event when the selected files are added to the buffer
                this.bindPluploadEvent(
                    'FilesAdded',
                    function(up, files) {
                        this.filesReady(files);
                        this.startPlupload();
                    }
                );

                // Event every time a file is uploaded
                this.bindPluploadEvent(
                    'FileUploaded',
                    function(up, file) {
                        this.updateProgress(file.id, 'OK', true);
                    }
                );

                // Event when all files have been uploaded
                this.bindPluploadEvent(
                    'UploadComplete',
                    function(up, files) {
                        this.fire('uploadComplete', {});
                    }
                );

                // Event when the system wants to send the progress of an upload
                this.bindPluploadEvent(
                    'UploadProgress',
                    function(up, file) {
                        this.updateProgress(file.id, file.percent == 100 ? 'OK' : file.percent + '%');
                    }
                );

                // Event when an error occurred
                this.bindPluploadEvent(
                    'Error',
                    function(up, err) {
                        if (err.code == -600) {
                            var _successFlash = new Flash(
                                err.file.name,
                                Flash.CODE_ERROR,
                                _('ERR.FILE_TO_LARGE', 'Bestand \'%1\' is te groot').replace('%1', err.file.name),
                                7000
                            );
                            _successFlash.renderTo($('#' + this.getId() + ' .data-container').shift());
                        } else if (err.file !== undefined) {
                            this.updateProgress(err.file.id, 'ERROR');
                        }
                    }
                );

                this.bindPluploadEvent(
                    "BeforeUpload",
                    function(up, file) {
                        this._pluploadObject.settings.multipart_params.related_id = this.getRelatedId();
                    }
                );
            },

            onShow: function() {
                this.__super();
                this.pluploadRefresh();
            },

            /**
             * Updates the grid when the progress of an upload is sent
             * @param id id of the file
             * @param progress the progress (percentage) of a file
             * @param complete true when the file has received the FileUploaded event
             */
            updateProgress: function(id, progress, complete) {
                complete = complete !== undefined;
                var eventData = {};
                eventData.callee = new Element(this.getElement());
                eventData.filters = [{field: 'id', value: id}];
                if (complete === true) {
                    eventData.filters.push({field: 'progress', value: '100%'});
                }
                eventData.change = {'progress': progress};
                this.fire('uploadFileProgress', eventData, true);
            },

            onRelatedIdChange: function(event, options) {
                this.setRelatedId(options.related_id);
            }
        });

        return UploadWindow;
    }
);
