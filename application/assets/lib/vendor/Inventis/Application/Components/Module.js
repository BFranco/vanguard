/**
 * the module component is normally the top level component
 * in the component hierarchy
 * @author Inventis
 */
define(
    [
        "Inventis/Application/Components/Component"
    ],
    function(Component){
        var Module = Component.extend({

            setupFromConfig: function(setup) {
                this.__super(setup);
            }
        });
        return Module;
    }
);