/**
 *  file
 *
 * Javascript
 *
 * @category
 * @package    Inventis
 * @subpackage Bricks
 * @author     Inventis Web Architects <info@inventis.be>
 * @license    Copyright © Inventis BVBA  - All rights reserved
 * @link       https://github.com/Inventis/Bricks
 */
define(
    [
        "Inventis/Console",
        "Inventis/Config",
        "Inventis/Application/Components/Component",
        "Inventis/Application/Components/Component/ApiAwareMixin",
        "Inventis/Application/Webadmin",
        "Inventis/Flash"
    ],
    function (console, Config, Component, ApiAware, WA, Flash) {

        var Form = Component.extend({
            use: [ApiAware],

            _hasFiles: false,
            _successFlash: null,
            _record: {},
            url: null,
            isDirty: false,

            iframe: null,


            init: function(setup){
                this.__super(setup);
                this.detectFileComponents();
            },

            onDomReady: function(){
                this.attachListeners();
            },

            setupFromConfig: function(setup){
                this.__super(setup);
                //ApiAware function
                this.setApiFromConfig(setup);
                this.setSuccessMessageFromConfig(setup);
                this.setUrlFromConfig(setup);
            },

            setUrlFromConfig: function(setup) {
                if (setup.url) {
                    this.url = setup.url;
                }
            },

            setSuccessMessageFromConfig: function(setup) {
                if (setup.successMessage) {
                    this.setSuccessMessage(setup.successMessage);
                }
            },

            setSuccessMessage: function(message){
                this._successFlash = new Flash(this.getId(), Flash.CODE_SUCCESS, message, 7000);
                return this;
            },

            showSuccessMessage: function(){
                return this._successFlash.renderTo(this.getElement());
            },

            showRecordSaveFailedFlash: function(message){
                var flash = new Flash(this.getId(), Flash.CODE_ERROR, message, 7000);
                flash.renderTo(this.getElement());
            },

            /**
             * to detail with the default behaviour we catch the submit event
             * and dispatch the request to our internal handler instead
             *
             */
            attachListeners: function(){
                //attach on submit listener to own element
                this.on('submit', function(e){
                    if (!this.url) {
                        e.preventDefault();
                        this.submit();
                        return false;
                    }
                }, true);
                //attach edit record event to top level (document)
                this.on('editRecord', function(e, options) {
                    var record = this.getRecord();
                    if (record === undefined || options.id !== record.id) {
                        this.loadRecord(options);
                    }
                }, true);
                this.on('getLoadedRecord', function(e, options){
                    //attach the record to the options object
                    options.record = this.getRecord();
                    return false;//stop bubble
                }, true);
                this.on('changed', this.onChanged, true);
            },

            onChanged: function(event, options) {
                this.changeDirtyState(true);
            },

            /**
             * form is submitted
             * @returns {boolean}
             */
            submit: function(){
                this.disable();
                //test if browser is not HTML5 ready when using files
                if (this._hasFiles && !WA.browser.support.XMLHttpRequestLevel2) {
                    this._processNoneAjaxSubmitRequest();
                } else {
                    var params = {fields: {id: this.getRecord().id}, files: {}};
                    var binaryData = false;

                    this.fire('beforeFormSave', params);

                    if (this._hasFiles) {
                        binaryData = params.files;
                    }

                    this.apiRequest('save', params.fields, this.recordSubmitCompleted, binaryData);
                }
            },

            /**
             * certain browsers are not capable.
             * for them you can call this method to submit a
             * request through an iFrame
             * @private
             */
            _processNoneAjaxSubmitRequest: function(){
                if (!this.iframe) {
                    this.iframe = document.createElement('iframe');
                    this.getElement().action = this.getActionUrl('save');
                    this.getElement().target  = this.iframe.name = this.getId();
                    this.iframe.class = "hidden";
                    document.body.appendChild(this.iframe);
                }
                var e;
                    this.iframe.onload = function() {
                    try {
                        this.recordSubmitCompleted(WA.JSON.decode(this.iframe.contentWindow.document.body.innerHTML));
                    } catch (e) {
                        document.body.innerHTML = this.iframe.contentWindow.document.body.innerHTML;
                    }
                }.bind(this);
                this.getElement().submit();
            },

            /**
             * callback for submit ajax request
             * @param response the response of the request
             */
            recordSubmitCompleted: function(response) {
                this.enable();
                if(response.success === true) {
                    this.recordSubmitSuccess(response);
                } else {
                    this.recordSubmitFailed(response);
                }

            },

            /**
             * on success show a flash message and fire the recordSaved event
             * @param response
             */
            recordSubmitSuccess: function(response){
                this.showSuccessMessage();
                var record, id;
                if (response.result && response.result.data) {
                    record = response.result.data;
                    id = response.result.data.id;
                }
                this.fire(
                    'recordSaved',
                    {
                        record: record,
                        id : id
                    }
                );
                this.setRecord(record);
                this.changeDirtyState(false);
            },

            /**
             * on submit failure we need to process the response for the fields
             * so that they can see if any response was for them
             *
             * @param response
             */
            recordSubmitFailed: function(response){
                if (response.result === undefined){
                    return;
                }
                this.showRecordSaveFailedFlash(response.message);
                this.fire('recordSaveFailed', {errors: this.extractValidationErrors(response.result)});
            },

            extractValidationErrors: function(result) {
                var i, errors = {}, error;

                for (i=0;i<result.length;i++) {
                    error = result[i];
                    errors[error.id] = error.msg;
                }
                return errors;
            },

            /**
             * method is called when the editRecord event is fired
             * and is passed the options object from the event
             * which can then be handled in a form specific way
             * (allows overwriting for additional checks etc.)
             * @param options
             * @event beforeFormRecordLoaded fired before the form requests server side record loading
             * @event formRecordLoaded
             */
            loadRecord: function(options){
                this.disable();
                this.fire('clear', {defaults: []}, false);
                if (options.id !== undefined) {
                    //params for event
                    var data = {id: options.id};
                    this.fire('beforeRecordLoad', {type:'form', data:data});
                    this.apiRequest('get', data, this.recordLoadCompleted);
                }
            },

            changeDirtyState: function(dirty) {
                this.isDirty = dirty;
                this.fire('dirtyChange', {isDirty: this.isDirty, id: this.getId()}, true);
            },

            recordLoadCompleted: function(response){
                if (response.success) {
                    this.setRecord(response.result.data);
                    this.fire('recordLoaded', {type:'form', record:response.result.data});
                    this.changeDirtyState(false);
                } else {
                    this.recordLoadFailed(response);
                }
                this.enable();
            },

            /**
             * stores the currently loaded record
             * @param record
             */
            setRecord: function(record){
                this._record = record;
                return this;
            },

            /**
             *
             * @returns {Object}
             */
            getRecord: function(){
                return this._record;
            },

            recordLoadFailed: function(response){
                console.log('@TODO: implement recordLoadFailed');
                if (response.success === false) {
                    console.log('error: ' + response.message);
                } else {
                    console.log(response);
                }
            },

            /**
             * because enctype needs to switch when there are file
             * components at play we need to listen to our
             * own component dom to see if any file components are inside
             * and change the enctype of the form accordingly
             */
            detectFileComponents: function(){
                this.on('fileFieldComponentLoaded', function(e, options){
                    this._hasFiles = true;
                    this.setEnctype('multipart/form-data');
                    //forms nesting is illegal so we can stop here
                    e.stopPropagation();
                    return false;
                }, true);
            },

            /**
             * change the form's enctype to the specified one
             * this method gets auto called when an internal component fires
             * the `fileFieldComponentLoaded` event to set the form to "multipart/form-data"
             */
            setEnctype: function(enctype){
                var allowedEncTyped = ["multipart/form-data", "application/x-www-form-urlencoded", "text/plain"];
                if (allowedEncTyped.indexOf(enctype)){
                    throw new Error(
                        "the specified enctype ["+enctype+"] is not supported, supported typed are: "+
                        allowedEncTyped.join(', ')
                    );
                }
                this.getElement().setAttribute("enctype", enctype);
            }
        });

        return Form;
    }
);
