/**
 * Created with JetBrains PhpStorm.
 * User: janesser
 * Date: 16/05/13
 * Time: 15:58
 * To change this template use File | Settings | File Templates.
 */
define(
    [
        "Inventis/Application/Components/Panel",
        "Inventis/HTML/Element",
        "Sizzle"
    ],
    function(Panel, Element, $) {
        var BackFilterPanel = Panel.extend({

            filterEvent: null,

            filterRouting: null,

            disableWhenFalse: true,

            ids: null,

            setupFromConfig: function(setup){
                this.__super(setup);
                this.setFilterEventFromSetup(setup);
                this.setFilterRoutingFromSetup(setup);
                this.setDisableWhenFalseFromSetup(setup);
                this.setIdsFromSetup(setup);
            },

            onDomReady: function(e, options) {
                this.__super(e, options);
                this._attachEvents();
            },

            _attachEvents: function() {
                this.on(this.filterEvent, this.onFilterEvent, true);
            },

            onFilterEvent: function(event, options) {
                var ids = this.ids,
                    filterRouting = this.filterRouting,
                    newOptions = {},
                    element = null,
                    x = 0,
                    y = 0,
                    disable = false;
                newOptions.filters = [];
                for (y in filterRouting) {
                    if (filterRouting.hasOwnProperty(y)) {
                        if (options[y]) {
                            var filter = {};
                            filter.property = filterRouting[y];
                            filter.value = options[y];
                            newOptions.filters.push(filter);
                        }
                        disable = disable || !options[y];
                    }
                }
                for (x in ids) {
                    if (ids.hasOwnProperty(x)) {
                        element = new Element(document.getElementById(ids[x]));
                        element.fire('setFilters', newOptions);
                    }
                }
                if (disable && this.disableWhenFalse) {
                    this.fire('disable', [], false);
                } else {
                    this.fire('enable', [], false);
                }
            },

            setFilterEventFromSetup: function(setup) {
                if (setup.filterEvent) {
                    this.setFilterEvent(setup.filterEvent);
                }
            },

            setFilterRoutingFromSetup: function(setup) {
                if (setup.filterRouting) {
                    this.setFilterRouting(setup.filterRouting);
                }
            },

            setDisableWhenFalseFromSetup: function(setup) {
                if (setup.disableWhenFalse) {
                    this.setDisableWhenFalse(setup.disableWhenFalse);
                }
            },

            setIdsFromSetup: function(setup) {
                if (setup.ids) {
                    this.setIds(setup.ids);
                }
            },

            setFilterEvent: function(filterEvent) {
                this.filterEvent = filterEvent;
                return this;
            },

            setFilterRouting: function(filterRouting) {
                this.filterRouting = filterRouting;
                return this;
            },

            setDisableWhenFalse: function(disableWhenFalse) {
                this.disableWhenFalse = disableWhenFalse;
                return this;
            },

            setIds: function(ids) {
                this.ids = ids;
                return this;
            },

            getFilterEvent: function() {
                return this.filterEvent;
            },

            getFilterRouting: function() {
                return this.filterRouting;
            },

            getDisableWhenFalse: function() {
                return this.disableWhenFalse;
            },

            getIds: function() {
                return this.ids;
            }
        });
        return BackFilterPanel;
    }
);