/**
 * Created with JetBrains PhpStorm.
 * User: janesser
 * Date: 09/08/13
 * Time: 08:28
 * To change this template use File | Settings | File Templates.
 */
define(
    [
        "Inventis/Application/Components/Module"
    ],
    function(Module) {
        var TwoLayeredModule = Module.extend({
            /**
             * an instance of Inventis/HTML/Element
             * added when that component fires the componentInitialized event
             */
            _backPanel:null,
            _backPanelId:null,

            /**
             * an instance of the Inventis/HTML/Element
             * added when that component fires the componentInitialized event
             */
            _previewPanel:null,
            _previewPanelId:null,

            setupFromConfig: function(setup){
                this.__super(setup);
                this.setDependenciesFromSetup(setup);
            },

            setDependenciesFromSetup: function(setup){
                this.setBackPanelFromSetup(setup);
                this.setPreviewPanelFromSetup(setup);
                this.on('componentInitialized', this.onComponentInitialized, true);
            },

            setBackPanelFromSetup: function(setup) {
                if (setup.dependencies && setup.dependencies.backPanel) {
                    this._backPanelId = setup.dependencies.backPanel;
                }
            },

            setPreviewPanelFromSetup: function(setup) {
                if (setup.dependencies && setup.dependencies.previewPanel) {
                    this._previewPanelId = setup.dependencies.previewPanel;
                }
            },

            onDomReady: function(){
                this.__super();
            },

            /**
             * called when child components are initialized, this method
             * will try and assign a backpanel component and attach an event handler on it
             * @param e
             * @param options
             */
            onComponentInitialized: function(e, options){
                //if component matches dependency store reference to observable element
                switch(options.id){
                    case this._backPanelId:
                        return this.setBackPanel(options.element);
                    case this._previewPanelId:
                        return this.setPreviewPanel(options.element);
                }
            },

            /**
             * sets the to use back panel component for back panel show event firing
             * @param {Inventis/HTML/Element} panel
             * @returns {*}
             */
            setBackPanel: function(panel) {
                this._backPanel = panel;
                return this;
            },

            getBackPanel: function(){
                if (typeof this._backPanel !== "object") {
                    throw new Error('No backpanel component is set yet, but one is requested');
                }
                return this._backPanel;
            },

            /**
             * sets the to use preview panel component for preview panel load event firing
             * @param {Inventis/HTML/Element} panel
             * @returns {*}
             */
            setPreviewPanel: function(panel) {
                this._previewPanel = panel;
                return this;
            },

            getPreviewPanel: function(){
                if (typeof this._previewPanel !== "object") {
                    throw new Error('No previewPanel component is set yet, but one is requested');
                }
                return this._previewPanel;
            }
        });
        return TwoLayeredModule;
    }
);