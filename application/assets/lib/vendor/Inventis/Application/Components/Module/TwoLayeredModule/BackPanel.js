/**
 *  file
 *
 * Javascript
 *
 * @category
 * @package    Inventis
 * @subpackage Bricks
 * @author     Inventis Web Architects <info@inventis.be>
 * @license    Copyright © Inventis BVBA  - All rights reserved
 * @link       https://github.com/Inventis/Bricks
 */
/**
 * The backpanel component is a plain panel component that listens to a specified event for opening
 */
define(
    [
        "Inventis/Application/Components/Panel",
        "Inventis/Translator",
        "Sizzle"
    ],
    function (Panel, _, $) {
        var BackPanel = Panel.extend({
            dirties: [],

            setupFromConfig: function(setup) {
                this.__super(setup);
            },

            onDomReady: function(){
                this.attachEventListeners();
                this.__super();
            },

            /**
             * called on domready this method is responsible for attaching all events
             */
            attachEventListeners: function() {
                this.on('backPanelOpen', this.onBackPanelOpenRequest, true);
                this.on('backPanelClose', this.onBackPanelCloseRequest, true);

                var closeButton = $('#' + this.getId() + ' .heading .window-button-close').shift();
                closeButton.onclick = function(evt) {
                    this.checkDirtyAndClose();
                }.bind(this);
                this.on('dirtyChange', this.onDirtyChange, true);
            },

            checkDirtyAndClose: function() {
                var close = true;
                if (this.dirties.length !== 0) {
                    this.askConfirmation();
                } else {
                    this.hide();
                }
            },

            askConfirmation: function() {
                var window = new Ext.window.MessageBox();
                window.confirm(
                    _('INVENTIS.APPLICATION.COMPONENTS.FROM.BACK_TO_OVERVIEW', 'Terug naar overzicht'),
                    _(
                        'INVENTIS.APPLICATION.COMPONENTS.FORM.BACK_TO_OVERVIEW.QUESTION',
                        'Het blijkt dat u één of meerdere tekstvelden veranderd hebt.' +
                        'Veranderingen zullen niet opgeslagen worden. ' +
                        'Weet u zeker dat u terug naar het overzicht wilt gaan?'
                    )
                    ,
                    function(btn){
                        if (btn === 'yes') {
                            this.hide();
                            this.dirties = [];
                        }
                    }.bind(this)
                );
            },

            onDirtyChange: function(event, options) {
                if (options.isDirty) {
                    if (this.dirties.indexOf(options.id) === -1) {
                        this.dirties.push(options.id);
                    }
                } else {
                    this.dirties.splice(this.dirties.indexOf(options.id), 1);
                }
            },

            /**
             * handler for backPanelOpen events. these type of events will
             * not be bubbled further up the chain
             * @param e
             * @param options
             */
            onBackPanelOpenRequest: function(e, options){
                e.stopPropagation();// just to make sure, in case events are fires as bubble
                this.dirties = [];
                this.show();
            },

            /**
             * handler for backPanelClose events. these type of events will
             * not be bubbled further up the chain
             * @param e
             * @param options
             */
            onBackPanelCloseRequest: function(e, options){
                e.stopPropagation();// just to make sure, in case events are fires as bubble
                this.checkDirtyAndClose();
            }

        });
        return BackPanel;
    }
);