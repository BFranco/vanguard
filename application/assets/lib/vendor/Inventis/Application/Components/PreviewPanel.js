/**
 *  file
 *
 * Javascript
 *
 * @category
 * @package    Inventis
 * @subpackage Bricks
 * @author     Inventis Web Architects <info@inventis.be>
 * @license    Copyright © Inventis BVBA  - All rights reserved
 * @link       https://github.com/Inventis/Bricks
 */
define(
    [
        "Inventis/Application/Components/Component",
        "Inventis/Application/Components/Component/ApiAwareMixin",
        "Inventis/Mixins/Maskable",
        "Sizzle"
    ],
    function (Component, Api, Maskable, $) {
        var PreviewPanel = Component.extend({
            use: [Api, Maskable],

            /**
             *
             * @param setup
             */
            setupFromConfig: function(setup){
                this.__super(setup);
                this.setApiFromConfig(setup);
            },

            onDomReady: function(){
                this.attachEventListeners();
                this.__super();
            },

            attachEventListeners: function(){
                this.on('loadPreview', this.onLoadPreview, true);
            },

            onLoadPreview: function(e, options){
                if (options.ids && options.ids.length) {
                    this.mask();
                    this.apiRequest('get', {ids: options.ids}, this.onPreviewDataLoaded);
                } else if (options.id) {
                    this.mask();
                    this.apiRequest('get', {ids: [options.id]}, this.onPreviewDataLoaded);
                } else {
                    this.clear();
                }
            },

            clear: function(){
                this.mask();
                var el = $('.data-container', this.getElement()).shift();
                el.innerHTML = '';
            },

            onPreviewDataLoaded: function(response){
                var el = $('.data-container', this.getElement()).shift();
                if (response.success === true) {
                    el.innerHTML = response.result;
                } else {
                    el.innerHTML = response.message || response;
                }
                this.unmask();
            }
        });
        return PreviewPanel;
    }
);