/**
 * Created with JetBrains PhpStorm.
 * User: janesser
 * Date: 16/05/13
 * Time: 15:58
 * To change this template use File | Settings | File Templates.
 */
define(
    [
        "Inventis/Application/Components/Component",
        "Inventis/HTML/Element",
        "Inventis/Button",
        "Sizzle"
    ],
    function(Component, Element, Button, $) {
        var TabPanel = Component.extend({
            _tabs: [],
            _activeTab:0,
            _hiddenTabClass: 'hidden',
            _tabButtons: [],

            onDomReady: function(){
                this.__super();

                this._attachTabEventHandling();
                this.showActiveTab();
            },

            setupFromConfig: function(setup){
                this.__super(setup);
                this.setTabsFromSetup(setup);
                this.setActiveTabFromSetup(setup);
            },

            setActiveTabFromSetup: function(setup){
                if (setup.activeTab) {
                    this.setActiveTab(setup.activeTab);
                }
            },

            /**
             * shows the panel that is at the activeTab index
             */
            showActiveTab: function(){
                this.showPanel(this.getTabs()[this.getActiveTab()]);
                this.activatePanel();
            },

            /**
             * sets the index of the currently active tab
             * this method does not switch to that tab, use showActiveTab
             * for that or showPanel if you know the panelId
             * @param index
             * @returns {*}
             */
            setActiveTab: function(index){
                this._activeTab = parseInt(index);
                return this;
            },

            getActiveTab: function(){
                return this._activeTab;
            },

            /**
             * reads out all top level sub components and puts their ids
             * in a list
             * @param setup
             */
            setTabsFromSetup: function(setup){
                if (setup.components) {
                    var i, tabs=[];
                    for (i=0;i<setup.components.length;i++) {
                        tabs.push(setup.components[i].setup.id);
                    }
                    this.setTabs(tabs);
                }
            },

            setTabs: function(tabs){
                this._tabs = tabs;
                return this;
            },

            getTabs: function(){
                return this._tabs;
            },

            hasError: function(panelId) {
                return function (event, options) {
                    // 'this' is the panel that has the error
                    var x;
                    for (x in this.getTabs()) {
                        if (this.getTabs()[x] === panelId) {
                            var element = this._tabButtons[x]._element,
                                classes = element.className.split(' '),
                                __errorClass = options.errorClass;
                            if (classes.indexOf(__errorClass) === -1) {
                                classes.push(__errorClass);
                                element.className = classes.join(' ');
                            }
                        }
                    }
                }
            },

            hasNoError: function(panelId) {
                return function (event, options) {
                    // 'this' is the panel that has the error
                    var x;
                    for (x in this.getTabs()) {
                        if (this.getTabs()[x] === panelId) {
                            var element = this._tabButtons[x]._element,
                                classes = element.className.split(' '),
                                __errorClass = options.errorClass,
                                index = classes.indexOf(__errorClass);
                            if (index !== -1) {
                                classes.splice(index, 1);
                                element.className = classes.join(' ');
                            }
                        }
                    }
                }
            },

            onDisableAllTabs: function(event, options) {
                if (options.id === this.getId()) {
                    for (var i in this._tabButtons){
                        if (this._tabButtons.hasOwnProperty(i)) {
                            this._tabButtons[i].disable();
                        }
                    }
                }
            },

            _attachTabEventHandling: function(){
                var tabButtons = $("> .tabs li", this.getElement()),
                    i, x, tab, tabElement;
                this.on('disableAllTabs', this.onDisableAllTabs);
                for (i=0; i<tabButtons.length; i++) {
                    tabButtons[i] = new Button(
                        tabButtons[i],
                        {for: tabButtons[i].getAttribute('data-for'), tabPanel: this.getId()}
                    );
                    tabButtons[i].on("buttonClicked", this.onTabButtonClick.bind(this), true);
                }

                for (x in this.getTabs()) {
                    tab = document.getElementById(this.getTabs()[x]);
                    tabElement = new Element(tab);
                    tabElement.on("hasError", this.hasError(tabElement._element.id).bind(this), true);
                    tabElement.on("noError", this.hasNoError(tabElement._element.id).bind(this), true);
                }

                //this.on("buttonClicked", this.onTabButtonClick, true);
                this.on("componentDisabled", function(e, options){
                    var index = this.getTabs().indexOf(options.id);
                    if (index != -1) {
                        (this.hasNoError(options.id).bind(this))({}, {errorClass: 'error'});
                        tabButtons[index].disable();
                        tabElement = new Element();
                        //change the active tab is needed
                        if (index === this.getActiveTab()) {
                            for (var i=0;i<tabButtons.length;i++){
                                if (!tabButtons[i].disabled()) {
                                    this.setActiveTab(i).showActiveTab();
                                }
                            }
                            if (index === this.getActiveTab()) {
                                this.setActiveTab(0).showActiveTab();
                            }
                        }
                        return false;
                    }
                }, true);
                this.on("componentEnabled", function(e, options){
                    var index = this.getTabs().indexOf(options.id);
                    if (index != -1) {
                        tabButtons[index].enable();
                    }
                    // if active tab is disabled, enable this tab that's enabled for sure
                    if (tabButtons[this.getActiveTab()].disabled()) {
                        this.setActiveTab(index).showActiveTab();
                    }
                }, true);
                this._tabButtons = tabButtons;
            },

            activatePanel: function() {
                for (x in this._tabButtons) {
                    var classes = String(this._tabButtons[x]._element.className).split(" "),
                        index;
                    if (parseInt(x) === this._activeTab) {
                        index = classes.indexOf("active");
                        if (index == -1) {
                            classes.unshift("active");
                            this._tabButtons[x]._element.className = classes.join(" ");
                        }
                    } else {
                        index = classes.indexOf("active");
                        if (index != -1) {
                            classes.splice(index, 1);
                            this._tabButtons[x]._element.className = classes.join(" ");
                        }
                    }
                }
            },

            onTabButtonClick: function(e, options) {
                if (options.tabPanel === this.getId()) {
                    this.showPanel(options.for);
                    this.activatePanel();
                    return false;
                }
            },

            showPanel: function(panelId){
                var tabs = this.getTabs(),
                    i, panel, activePanel, panelClasses, index, panelElement;

                for(i=0;i<tabs.length;i++) {
                    activePanel = tabs[i] === panelId;
                    panel = $("#"+tabs[i]).shift();
                    panelClasses = panel.className.split(' ');
                    index = panelClasses.indexOf(this._hiddenTabClass);
                    if (activePanel) {
                        if (index !== -1) {
                            panelClasses.splice(index, 1);
                        }
                        this.setActiveTab(i);
                        panelElement = new Element(panel);
                    } else {
                        if (index === -1) {
                            panelClasses.unshift(this._hiddenTabClass)
                        }
                    }
                    panel.className = panelClasses.join(' ');
                }
                panelElement.fire('tabActivated', {}, true);
            }
        });
        return TabPanel;
    }
);