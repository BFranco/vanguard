
define(
    [
        "Inventis/Application/Components/Component",
        "Inventis/HTML/Element",
        "Inventis/Mixins/GoogleMapMixin",
        "Sizzle"
    ],
    function(Component, Element, GM, $) {
        var markerName = 'marker';
        var GoogleMaps = Component.extend({
            use: [GM],

            _managerComponentId: null,

            _managerComponent: null,

            _entityComponentId: null,

            _entityComponent: null,

            _mapsElement: null,

            _defaultCoords: '',

            setupFromConfig: function(setup){
                this.__super(setup);
                this.dependenciesFromConfig(setup);
                this.defaultCoordsFromConfig(setup);
                this.on("componentInitialized", this.onComponentInitialized, true);
                this.on('selectCoords', this.onSelect, true);
                this.getManagerComponent().on('windowOpened', this.onManagerShow.bind(this), true);
            },

            defaultCoordsFromConfig: function(setup) {
                if (setup.defaultCoords !== undefined) {
                    this._defaultCoords = setup.defaultCoords;
                }
            },

            onSelect: function(event, options) {
                var googlePos = this.markers[markerName].getPosition(),
                    lat = googlePos.lat(),
                    lng = googlePos.lng();
                this.getEntityComponent()._element.value = lat + ',' + lng;
                this.getManagerComponent().fire('hide', {}, false);
                return false;
            },

            onManagerShow: function(event, options) {
                var value = this.getEntityComponent()._element.value ?
                        this.getEntityComponent()._element.value :
                        this._defaultCoords,
                    index = value.indexOf(','),
                    lat = value.substring(0, index),
                    lng = value.substring(index + 1),
                    coords = this.toGoogleCoords({lng: lng, lat: lat});
                this.getGMNamespace().event.trigger(this.maps[this._mapsElement], 'resize');
                this.addMarker(markerName, {
                    map: this.maps[this._mapsElement],
                    position: coords,
                    draggable: true,
                    crossOnDrag: true,
                    clickable: false
                });
                this.maps[this._mapsElement].setZoom(8);
                this.maps[this._mapsElement].setCenter(coords);
            },

            init: function(setup) {
                this.__super(setup);
                this._mapsElement = $('#' + this._managerComponentId + ' > div.data-container').shift();
                // set Google maps component
                this.onMapsLoad(function() {
                    var map = this.initGM(
                        this._mapsElement,
                        {
                            center: this.toGoogleCoords({lat: 51.02294, lng: 5.46489}),
                            zoom: 8,
                            mapTypeId: this.getGMNamespace().MapTypeId.ROADMAP
                        }
                    );
                    window.google.maps.event.addListener(map, 'click', function(event) {
                        this.setMarkerPosition(markerName, event.latLng);
                    }.bind(this));
                }.bind(this));
            },

            dependenciesFromConfig: function(setup) {
                if (setup.dependencies === undefined) {
                    return;
                }
                this.managerComponentIdFromDependencies(setup.dependencies);
                this.entityComponentIdFromDependencies(setup.dependencies);
            },

            managerComponentIdFromDependencies: function(dependencies) {
                if (dependencies.managerComponent === undefined) {
                    return;
                }
                this._managerComponentId = dependencies.managerComponent;
            },

            entityComponentIdFromDependencies: function(dependencies) {
                if (dependencies.entityComponent === undefined) {
                    return;
                }
                this._entityComponentId = dependencies.entityComponent;
            },

            getEntityComponent: function() {
                if (!this._entityComponent) {
                    this._entityComponent = new Element(document.getElementById(this._entityComponentId));
                }
                return this._entityComponent;
            },

            getManagerComponent: function() {
                if (!this._managerComponent) {
                    this._managerComponent = new Element(document.getElementById(this._managerComponentId));
                }
                return this._managerComponent;
            },

            onComponentInitialized: function(event, options) {
                switch (options.id) {
                    case this._managerComponentId:
                        this._managerComponent = options.element;
                        break;
                    case this._entityComponentId:
                        this._entityComponent = options.element;
                        break;
                }
            }
        });
        return GoogleMaps;
    }
);