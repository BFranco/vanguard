/**
 * Window.js file
 *
 * Javascript
 *
 * @category
 * @package    Inventis
 * @subpackage Bricks
 * @author     Inventis Web Architects <info@inventis.be>
 * @license    Copyright © Inventis BVBA  - All rights reserved
 * @link       https://github.com/Inventis/Bricks
 */
define(
    [
        "Inventis/Application/Components/Panel",
        "Inventis/Button",
        "Inventis/HTML/Element",
        "Inventis/Application/Components/Button",
        "Inventis/Mixins/Draggable",
        "Sizzle"
    ],
    function (Panel, Button, Element, ComponentButton, Draggable, $) {
        //static scope

        //object definition
        var Window = Panel.extend({
            use: [Draggable],
            titlebarClass: "title",
            closeButtonClass: "window-button-close",

            /**
             * @var Button
             */
            _closeButton: null,

            /**
             * @var Draggable
             */
            _draggable: null,

            /**
             * An array of buttons at the bottom of the window
             * @var Array
             */
            _buttons: [],

            _parent: null,

            /**
             * constructor
             */
            __construct: function(draggable, closeButton, setup){
                this.setCloseButton(closeButton);
            },

            /**
             * Sets the properties from the given configuration
             * @param setup the setup received from the php components
             */
            setupFromConfig: function(setup) {
                this.__super(setup);
                this.setButtonsFromSetup(setup);
                this.setOverlayFromSetup(setup);
                this.setDraggableFromSetup(setup);
                this.on('windowHide', this.onWindowHide, true);
            },

            setDraggableFromSetup: function(setup) {
                if (setup.draggable === undefined) {
                    return;
                }
                this.setDraggable(setup.draggable);
            },

            onDomReady: function(){
                this.__super();
                // A button in the window is clicked
                this.on('buttonClicked', this.onButtonClicked, true);
            },

            getParent: function() {
                if (this._parent === null) {
                    var parent = this.getElement().parentNode,
                        parentElement = new Element(parent);
                    this._parent = parentElement;
                }
                return this._parent;
            },

            /**
             * Sets the overlay bool from config
             * @param setup
             */
            setOverlayFromSetup: function(setup) {
                if (setup.overlay === undefined) {
                    return;
                }
                this.setOverlay(setup.overlay);
            },

            /**
             * Sets the buttons from config
             * @param setup
             */
            setButtonsFromSetup: function(setup) {
                if (setup.buttons !== undefined) {
                    var x;
                    for (x in setup.buttons) {
                        if (setup.buttons.hasOwnProperty(x)) {
                            var button = new ComponentButton();
                            button.setupFromConfig(setup.buttons[x].setup);
                            button.onDomReady();
                            this._buttons.push(button);
                        }
                    }
                }
            },

            /**
             * ataches to the init to set up the close button
             * @param setup
             */
            init: function(setup){
                this.__super(setup);
                this.initCloseButton();
                if (this._draggable) {
                    this.setDraggableContainers($(this.getRenderTo()));
                    this.initDraggable();
                }
            },

            initCloseButton: function(){
                var closeButton = this.getCloseButton();

                if(closeButton === undefined || closeButton === null) {
                    var eventData = {};
                    eventData.callee = this.getId();
                    eventData.action = 'close';
                    this.setCloseButton(
                        new Button(
                            $("." + this.closeButtonClass, this.getElement()).shift(),
                            eventData
                        )
                    );
                }
            },

            /**
             * centers the window based on its dimensions and the screen
             */
            center: function(){
                var el = this.getElement();

                el.style.left = (window.innerWidth  / 2 - el.offsetWidth / 2) + 'px';
                el.style.top  = (window.innerHeight / 2 - el.offsetHeight / 2) + 'px';
            },

            /**
             * sets the draggable object to the provided object
             * @param draggable
             * @return Panel
             */
            setDraggable: function(draggable){
                this._draggable = draggable;
                return this;
            },

            /**
             * returns the set draggable object
             * @returns {Draggable}
             */
            getDraggable: function(){
                return this._draggable;
            },

            setOverlay: function(overlay) {
                this._overlay = overlay;
            },

            getOverlay: function() {
                return this._overlay;
            },

            /**
             * sets the close button to the provided object
             * @param closeButton
             * @return Panel
             */
            setCloseButton: function(closeButton){
                this._closeButton = closeButton;
                return this;
            },

            /**
             * returns the set close button
             * @returns {Button}
             */
            getCloseButton: function(){
                return this._closeButton;
            },

            /**
             * called when the close button is pressed
             */
            onButtonClicked: function(event, options) {
                if (options.action === 'close') {
                    this.hide();
                }
            },
            /**
             * Is called when the show event is called
             * @param event event data
             * @param options other data
             */
            onShow: function(event, options) {
                this.__super(event, options);
                this.center();
            },

            showOverlayParent: function() {
                if (!this.getOverlay()) {
                    return;
                }
                this.getParent().mask();
            },

            removeOverlayParent: function() {
                if (!this.getOverlay()) {
                    return;
                }
                this.getParent().unmask();
            },

            /**
             * override render to move the window to the render to location
             */
            render: function(){
                this.center();
                this.__super();
            },

            onWindowHide: function(event, options) {
                event.stopPropagation();
                this.hide();
            },

            show: function() {
                this.showOverlayParent();
                this.__super();
                this.fire('windowOpened');
            },

            hide: function() {
                this.removeOverlayParent();
                this.__super();
                this.fire('windowClosed', {}, true);
            },

            getDraggableElement: function(){
                return $("." + this.titlebarClass, this.getElement()).shift()
            }
        });
        return Window;
    }
);