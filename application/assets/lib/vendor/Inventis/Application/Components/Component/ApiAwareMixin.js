/**
 * Created with JetBrains PhpStorm.
 * User: janesser
 * Date: 22/04/13
 * Time: 11:30
 * To change this template use File | Settings | File Templates.
 */
define(
    [
        "Inventis/Mixin",
        "Inventis/Application/Webadmin"
    ],
    function (Mixin, WA) {
        return Mixin({
            _api:{},

            /**
             * handles setting the API from a setup provided
             * @param setup
             */
            setApiFromConfig: function(setup){
                if (typeof setup.api === "object") {
                    this.setApi(setup.api);
                }
            },

            /**
             * set the API
             * @param {Object} api
             */
            setApi: function(api){
                if (typeof api !== "object") {
                    throw new TypeError('Api must be an instance of an object');
                }
                this._api = api;
            },

            /**
             * get the API set
             * @returns {Object}|null
             */
            getApi: function(){
                return this._api;
            },

            /**
             * this is the default API request handler
             * which will form a default request object for you
             * that matches the provided action and will ensure the data gets passed
             * as part of the request, callback will only be called when the ajax request completed
             * (e.g. a valid response was returns)
             * @param action
             * @param data
             * @param callback
             * @param [binaryData] optional binary data to add to the request
             */
            apiRequest: function(action, data, callback, binaryData){
                var serverData = this.buildRequestData(action, data),
                    request, name;

                if (binaryData) {
                    if (!WA.browser.support.XMLHttpRequestLevel2) {
                        throw new Error(
                            'you can only use binaryData with XMLHttpRequest Level2 compatible browsers'
                        );
                    }
                    request = new FormData();
                    for (name in serverData) {
                        request.append(name, serverData[name]);
                    }
                    for (name in binaryData) {
                        for (var i=0; i<binaryData[name].length;i++) {
                            request.append(name, binaryData[name][i]);
                        }
                    }
                    serverData = request;
                }
                WA.ajax(this.getActionUrl(action), serverData, callback, this);
            },

            /**
             * builds API compliant request params that should be used to send to the server
             * @param action
             * @param data
             * @returns {{request: *}}
             */
            buildRequestData: function(action, data){
                if (data.componentAction !== undefined) {
                    throw new Error(
                        'Your request holds and action property, this property is reserved for the component request'
                    );
                }
                if (data.componentId !== undefined) {
                    throw new Error(
                        'Your request holds and componentId property, this property is reserved for the component request'
                    );
                }
                return WA.merge(data, {
                    componentId: this.getId(),
                    componentAction: action
                });
            },

            /**
             * returns the full url for use in your api call
             * @param action
             * @returns {*}
             */
            getActionUrl: function(action){
                var api = this.getApi();
                if (api[action] === undefined) {
                    throw new TypeError(
                        "Action ["+action+"] is not a valid api request, either correct the action or "+
                        "make sure that the api knows what to do with that action by defining a route for it "+
                        "in the component config"
                    );
                }
                return (api.baseUrl || this._getLocationBaseUrl()) + api[action];
            },

            /**
             * returns the current locations base url
             * @returns {string}
             * @private
             */
            _getLocationBaseUrl: function(){
                var l = window.location;
                return l.protocol + '//' + l.hostname + l.pathname;
            }

        });
    }
)