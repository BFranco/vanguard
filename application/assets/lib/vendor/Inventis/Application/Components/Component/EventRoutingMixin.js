/**
 * Created with JetBrains PhpStorm.
 * User: janesser
 * Date: 22/04/13
 * Time: 11:30
 * To change this template use File | Settings | File Templates.
 */
define(
    [
        "Inventis/Mixin",
        "Inventis/HTML/Element",
        "Inventis/Application/Webadmin"
    ],
    function (Mixin, Element, WA) {
        return Mixin({
            _eventRouting:{},

            /**
             * handles setting the API from a setup provided
             * @param setup
             */
            setEventRoutingFromConfig: function(setup){
                if (setup.eventRouting !== undefined &&
                    typeof setup.eventRouting === "object" &&
                    setup.eventRouting.length !== 0) {
                    this.setEventRouting(setup.eventRouting);
                }
            },

            /**
             * set the eventRouting object
             * @param {Object} eventRouting
             */
            setEventRouting: function(eventRouting){
                if (typeof eventRouting !== "object") {
                    throw new TypeError('Event routing must be an instance of an object');
                }
                this._eventRouting = eventRouting;
                this.initEventRouting();
            },

            /**
             * get the eventRouting object
             * @returns {Object}|null
             */
            getEventRouting: function(){
                return this._eventRouting;
            },

            /**
             * Initialize the routing of the events received by the config
             */
            initEventRouting: function() {
                var eventRouting = this.getEventRouting();
                for (x in eventRouting) {
                    if (eventRouting.hasOwnProperty(x)) {
                        var eventRoute = eventRouting[x];
                        this.addEventRoute(eventRoute);
                    }
                }
            },

            /**
             * Translates an eventRoute object to an actual mechanism (this.on(event, callback)) to route the
             * events that are specified to the right object
             * @param eventRoute an eventRoute object that contains events, destinations and an absorb flag
             */
            addEventRoute: function(eventRoute) {
                var events = eventRoute.events,
                    destinations = eventRoute.destinations,
                    absorb = eventRoute.absorb;
                for (listenEvent in events) {
                    if (events.hasOwnProperty(listenEvent)) {
                        /*
                         * for every event, create an listener that listens on listenEvent
                         */
                        this.on(listenEvent, this.route(destinations, absorb, events[listenEvent]), true);
                    }
                }
            },

            /**
             * The actual routing mechanism that fires an event to a destination and chooses to cancel the bubbling of
             * the event
             * @param destinations the ids of the elements that receives this event
             * @param absorb whether or not to cancel the bubbling of the event
             * @param fireEvent the event to be fired on the destinations
             * @returns {Function} the callback that has to be executed when a certain event must be routed
             */
            route: function(destinations, absorb, fireEvent) {
                return function(event, options) {
                    // for every destination (id of the element to be fired on)
                    for (x in destinations) {
                        if (destinations.hasOwnProperty(x) && destinations[x] !== false) {
                            // Search element that has the current destination id
                            var destinationElement = document.getElementById(destinations[x]);
                            if (destinationElement === undefined) {
                                throw new Exception('Element with id ' + destinations[x] + 'isn\'t found');
                            }
                            destinationElement = new Element(destinationElement);
                            // fire the new event fireEvent with the same objects on the found element
                            destinationElement.fire(fireEvent, options, false);
                        }
                    }
                    /*
                     * When absorb is true (the event must prevent the event from bubbling further),
                     * false is returned, preventing the event from bubbling to the ancestors of the eventRouter
                     */
                    return !absorb;
                };
            }
        });
    }
)