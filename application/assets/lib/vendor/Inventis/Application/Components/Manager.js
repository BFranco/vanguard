/**
 *  file
 *
 * Javasript
 *
 * @category
 * @package    Inventis
 * @subpackage Bricks
 * @author     Inventis Web Architects <info@inventis.be>
 * @license    Copyright © Inventis BVBA  - All rights reserved
 * @link       https://github.com/Inventis/Bricks
 */
define(
    [
        "Inventis/Class",
        "Inventis/Mixins/Observable"
    ],
    function (Class, Observable) {
        var instance;
        var Manager = Class.extend({
            use: [Observable],

            __construct: function() {
                this.attachListeners();
            },

            /**
             * creates listeners for the different components
             */
            attachListeners: function(){
                this.on('createComponents', function(e, options) {
                    this.createComponents(options.components);
                });
            },

            /**
             * creates components from a list of component configurations
             * @param components
             */
            createComponents: function(components){
                if(components.length === undefined) {
                    throw new Error('Failed to create components, components list must be an array.');
                }
                /*
                 * add components asynchronous, the order in which they are loaded
                 * is not important
                 */
                for (var i=0; i<components.length; i++) {
                    if (components[i].component === false) {//component doesn't have JS component
                        //see if there are sub-component configurations available and create those
                        if(components[i].setup && components[i].setup.components) {
                            this.createComponents(components[i].setup.components);
                        }
                    } else {
                        this.createComponent(components[i]);
                    }
                }
            },

            /**
             * creates a component after asynchronous call and passes it as an argument
             * to the callback function once the component has been created
             * @param {Object} config
             * @param {Function} [callback]
             * @param {Object} [scope]
             */
            createComponent: function(config, callback, scope){
                if (!config.component) {
                    throw new TypeError('Did not find a valid component setting in component configuration');
                }
                var me = this;//reference this inside callback
                // do async call and add component after initialization
                require(
                    [config.component],
                    function(Component){
                        if (Component === undefined) {
                            throw new TypeError(config.component + " could not be loaded. Ensure namespace is correct.");
                        }
                        var comp = new Component();
                        comp.init(config.setup);
                        if (callback !== undefined) {
                            callback.call(scope, comp);
                        }
                        me.fire('componentCreated', {componentType:config.component});
                    }
                );
            }
        });

        //create singleton
        return (instance = (instance || new Manager()));
    }
);