/**
 * Created with JetBrains PhpStorm.
 * User: janesser
 * Date: 16/05/13
 * Time: 15:58
 * To change this template use File | Settings | File Templates.
 */
define(
    [
        "Inventis/Application/Components/Panel",
        "Inventis/HTML/Element",
        "Sizzle"
    ],
    function(Panel, Element, $) {
        var UploadManager = Panel.extend({

            _managerComponentId: null,

            _managerComponent: null,

            _gridFormId: null,

            _gridForm: null,
            _currentRelatedId: null,

            _allowedColumn: null,

            setupFromConfig: function(setup){
                this.__super(setup);
                this.dependenciesFromConfig(setup);
                this.getAllowedColumnFromConfig(setup);
                this.observeManagerComponent();
                this.on("editRecord", this.onEditRecord, true);
                this.on('filterChange', this.onFilterChange, true);
            },

            getAllowedColumnFromConfig: function(setup) {
                this._allowedColumn = setup.allowedColumn;
            },

            onFilterChange: function(events, options) {
                var relatedIdFound = false;
                for (x in options.filters) {
                    if (options.filters.hasOwnProperty(x)) {
                        var filter = options.filters[x];
                        if (filter.property === 'related_id') {
                            relatedIdFound = true;
                            break;
                        }
                    }
                }
                if (!relatedIdFound) {
                    this.sendRelatedId();
                }
            },

            onEditRecord: function(event, options) {
                if (!options.id || (options.record && options.record[this._allowedColumn] === false)) {
                    this.disable();
                } else {
                    this.enable();
                    this.getManagerComponent().fire('changeRelatedId', {related_id : options.id}, false);
                    this._currentRelatedId = options.id;
                    this.sendRelatedId();
                }
            },

            observeManagerComponent: function() {
                this.getManagerComponent().on("uploadComplete", this.onUploadComplete.bind(this), true);
            },

            onUploadComplete: function(event, options) {
                this.fire("recordSaved", options, false);
            },

            dependenciesFromConfig: function(setup) {
                if (setup.dependencies === undefined) {
                    return;
                }
                this.addDependencyFromConfig(setup.dependencies, 'managerComponent', this.setManagerComponentId.bind(this));
                this.addDependencyFromConfig(setup.dependencies, 'entityComponent', this.setGridFormId.bind(this));
            },

            addDependencyFromConfig: function(dependencies, dependencyName, setter) {
                if (dependencies[dependencyName] === undefined) {
                    return;
                }
                setter(dependencies[dependencyName]);
            },

            setManagerComponentId: function(managerComponentId) {
                this._managerComponentId = managerComponentId;
            },

            setManagerComponent: function(managerComponent) {
                this._managerComponent = managerComponent;
            },

            setGridFormId: function(gridFormId) {
                this._gridFormId = gridFormId;
            },

            setGridForm: function(gridForm) {
                this._gridForm = gridForm;
            },

            getManagerComponentId: function() {
                return this._managerComponentId;
            },

            getManagerComponent: function() {
                if (this._managerComponent === null) {
                    var managerElement = document.getElementById(this.getManagerComponentId());
                    this.setManagerComponent(new Element(managerElement));
                }
                return this._managerComponent;
            },

            getGridFormId: function() {
                return this._gridFormId;
            },

            getGridForm: function() {
                if (this._gridForm === null) {
                    var gridForm = document.getElementById(this.getGridFormId());
                    this.setGridForm(new Element(gridForm));
                }
                return this._gridForm;
            },

            sendRelatedId: function() {
                this.getGridForm().fire('gridFilter', {filters : [{property: "related_id", value: this._currentRelatedId}]}, false);
            }
        });
        return UploadManager;
    }
);