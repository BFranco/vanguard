/**
 * Webadmin.js file
 *
 * Javasript
 *
 * @category
 * @package    Inventis
 * @subpackage Bricks
 * @author     Inventis Web Architects <info@inventis.be>
 * @license    Copyright © Inventis BVBA  - All rights reserved
 * @link       https://github.com/Inventis/Bricks
 */

/**
 * the webadmin is a singleton component that functions as the adapter to framework specific code
 * it is there to allow us to quickly switch frameworks without the need to refactor all other components
 * or at least keep this refactoring to a bare minimum
 * we will wrap framework functions that can than be used by all components using the webadmin
 *
 * the webadmin implement a singleton pattern as it will also ensure heartbeat functionality and other
 * `implement once` features that are needed
 */
define(
    [
        "Inventis/Console",
        "Inventis/Class",
        //load the framework adapter of choice
        "Inventis/Mixins/Adapters/ExtJSAdapter"
    ],
    function (console, Class, Adapter) {
        //reference to the singleton
        var instance,

        Webadmin = Class.extend({
            use: [Adapter],

            __construct: function(){

            },

            /**
             * we overwrite the default mixin handler to implement the feature as desired
             * @param response
             * @param options
             */
            ajaxFailureHandler: function(response, options){
                console.log('@TODO handle failures!', response, options);
            },

            ajax: function(url, data, callback, scope, headers) {
                this.fire('beforeAjaxRequest', {data:data});
                var failHandler = function(response, options){
                    this.ajaxFailureHandler(response, options);
                    this.fire('afterAjaxRequest', {response: false});
                };
                var successHandler = function(response, options){
                    try{
                        //try decode the response or return as in on failure
                        var decodedResponse;
                        decodedResponse = this.JSON.decode(response.responseText);
                        response = decodedResponse;
                    }
                    catch(e){
                        response = response.responseText;
                    }
                    finally {
                        callback.call(scope, response, options);
                        this.fire('afterAjaxRequest', {response: response});
                    }
                };
                if (window.FormData && data instanceof FormData) {
                    var xhr = new XMLHttpRequest(),
                        me = this;
                    xhr.open('POST', url);
                    xhr.onload = function(response){
                        if (response.target.status === 200) {
                            successHandler.call(me, response.target);
                        } else {
                            failHandler.call(me, response.target);
                        }
                    }
                    xhr.send(data);
                } else {
                    Adapter.ajax(url, data, callback, this, headers, successHandler, failHandler);
                }
            }

        });

        return (instance = (instance || new Webadmin()));
    }
);